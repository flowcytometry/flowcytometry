# Process Scheduler Design

This design for the processing system should describe how we're going to schedule the doctor's processing tasks. 

Things to take into consideration:

* If a doctor uploads 5 files at the same time
* What if we only have 1 compute node? Should his 5 get processed before anyone elses?
* (Add more if you think of them...)

_this is not a design for the actual processing system(SANJAY)._ This is just the design for how we will schedule jobs across the compute nodes. I'll probably be making some assumptions.


## Design


Acronym | ... | Description
--------|-----|------------
RFP     | Request for processing | This is what the scheduler will receive from the web api when it wan't to schedule a processing time for a new .fcs file

This system will be sort of a layered system. One layer will consist of 

Assumptions: The .fcs data has already been saved into the DB

Basic Idea:

* There are two main parts:
    * [1] HTTP Load Balancing Server
    * [2] SANJAY HTTP Notifier
* Our main web api will send an RFP to [1]
* [1] Will check the load(how many jobs they have queued up) of each of the compute nodes
* Once a node is idle, and ready for a job, [1] will send an RFP to it containing:
    * the mongodb file id
    * the http address to send a notification to once it's finished
* Once the node finishes processing, it will send a notification to the given http address
* Once [1] gets a notification from a node, it notifies the web api and that will send the 


We could either create a queue for each compute node, or just hold an overall queue, and assign tasks to idle nodes (I kinda think 2 could work better)
