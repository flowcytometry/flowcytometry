# How to test

## Web API

You should be able to run the API unit tests using: `npm test` or `yarn test` if you're using yarn.

The testing framework that is being used for the api, is a combination of the [Mocha](https://mochajs.org/) testing framework and the [Chai](http://chaijs.com/) assertion library. Using these libraries, we can create tests of the form:

```javascript
describe('An element that is being tested', function() {
    it('should do this thing', done) {
        // In here is where we would test some kind of functionality.
    }

    it('should do this other thing as well', done) {
        // Another assersion
    }

    xit('this test will be skipped, hence the x at the beginning') {
        // None of this will happen
    }
});
```

Using Chai, we can send requests, and test the responses we get from them.

```javascript
describe('/api/patients', function() {
    it('should return a list of patients on GET', done) {
        chai.request(server) // We're going to send a request to this server
            .get('/api/patients')   // Send a GET request to the following endpoint
            .end(function(err, res) {   // When we receive a response, perform this function
                res.should.have.status(200);    // Testing that we get the correct HTTP status code
                res.should.be.json;             // Testing that we're receiving a JSON response.
                res.body.should.be.a('array');  // Testing that the body is a list
            });
    }
})
```

This is using the BDD style, but if you prefer asserts, tests can be written in that [style](http://chaijs.com/guide/styles/#assert) using Chai as well.

More complete examples can be found in the top level `/test` directory. This is where all API tests should be located.

For many tests, we'll want to make some assumptions, such as having a patient in our database before we try to query for them. To do this, mocha gives us the `before` and `beforeEach` functions that can be placed inside of a `describe` block to specify that some code should be run before all, or before each of the tests, respectively. To demonstrate the given example of adding a patient to the database before running a test:

```javascript
const Patient = require('../models/patient');

describe('getting a doctor', () => {
    before(function(done) {
        const patient = new Patient({firstname: 'Rick', lastname: 'Morty'});
        patient.save((err) => {
            done(); // Make sure to call done so that the rest of the test suite runs.
        });
    });

    // Do some stuff with the patient...
    it('should do some stuff...');
    
    // Clear the database
    after(function(done) {
        Patient.collection.drop();
    });
});
```

Note that we can also run code after the tests with `after`, and `afterEach`, as shown above.

### FLAGS

While running tests, we want to be using a slightly different setup of our application so that the runs are consistent. For this, you'll want to set the following flags at the top of each test file, _before_ you require the server.js file:

```javascript
process.env.ENV = 'test'; // To make sure that we're in a testing environment
process.env.LOG_LEVEL = 'error'; // To lessen the amount of log messages that show up in between the test results

// Since these settings are read in the server.js file, they need to be set before pulling that in.
const server = require('../server');
```

You can also accomplish this in the command line as such: `ENV=test LOG_LEVEL=error npm test`

### Notes

* __Make sure that the db is running before running the tests, or you'll end up with failures.__ We could also setup a hook to automatically start it up before the test in the package.json.

### Links

* [Testing NodeJS with Mocha and Chai](http://mherman.org/blog/2015/09/10/testing-node-js-with-mocha-and-chai/#.WBDM7FmVsXd)
* [Chai HTTP](https://github.com/chaijs/chai-http)