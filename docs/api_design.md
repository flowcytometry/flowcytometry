# Web API Design

- [Web API Design](#web-api-design)
    - [General Requests](#general-requests)
    - [Endpoints](#endpoints)
        - [Doctors (Access: Admins)](#doctors-access-admins)
            - [Endpoint: /doctors](#endpoint-doctors)
            - [Endpoint: /doctors/:id (Doctor also has access?)](#endpoint-doctorsid-doctor-also-has-access)
        - [Patients (Access: Doctors)](#patients-access-doctors)
            - [Endpoint: /patients](#endpoint-patients)
            - [Endpoint : /patients/:id](#endpoint-patientsid)
        - [Processed Data (Access: Doctors)](#processed-data-access-doctors)
            - [Endpoint: /processed](#endpoint-processed)
            - [Endpoint: /processed/:id](#endpoint-processedid)
        - [Unprocessed Data (Access: Doctors)](#unprocessed-data-access-doctors)
            - [Endpoint: /unprocessed](#endpoint-unprocessed)
            - [Endpoint: /unprocessed/:id](#endpoint-unprocessedid)
    - [JWT](#jwt)
    - [Data Layouts](#data-layouts)
        - [Patient Data](#patient-data)
        - [Processed Data](#processed-data)
        - [Unprocessed Data](#unprocessed-data)
        - [Data Lists](#data-lists)

This design for the web api should allow for simple and intuitive access to necessary data. It will use the [RESTful](https://en.wikipedia.org/wiki/Representational_state_transfer) architecture.

TODO:

* Finalize data layouts

## General Requests

When a request comes in, no matter to what endpoint, check for a [JWT](#JWT) token.

* If token is present
    * Check privledges (admin/doctor, etc.)
    * Check if user is authorized to access the page endpoint (eg. a doctor trying to access some /admin endpoint)
        * If not, give an [HTTP 403](http://en.wikipedia.org/wiki/HTTP_403) error code
        * Else, continue processing the request
* If token is not present
    * Return an [HTTP 401](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#4xx_Client_Error)
    * Client should be redirected to login page

Note: __All requests will check for authentication before processing the request.__
Note: __All query parameters only apply to GET requests__

## Endpoints

These are all of the data endpoints that the client will use to interact with data on the server.
If an HTTP method for an endpoint is not listed, it should not be supported.

### Doctors (Access: Admins)

#### Endpoint: /doctors

Method | Action  | Response
-------|---------|---------
GET    | Get the full list of doctors | List of basic doctor data
POST   | Create a new doctor's account | Success or failure

#### Endpoint: /doctors/:id (Doctor also has access?)

Method | Action  | Response
-------|---------|---------
GET    | Get information on a single doctor | Basic doctor information
PUT    | Update's doctor's information | The updated doctor profile
DELETE | Delete a doctor's profile | Success or failure

_Note_: We should figure out exactly what a doctor can change on their account, vs what the admin can change

### Patients (Access: Doctors)

#### Endpoint: /patients

Method | Action  | Response
-------|---------|---------
GET    | List of patients belonging to the logged in doctor's id as specified by the [JWT](#JWT) | Array of patients
POST   | Add a [new patient](#Patient-Data) profile to the doctor's list | Id of newly created patient

__Query Parameters__

Parameter | Function
----------|---------
first\_name | Search patients by first name
last\_name | Search patients by last name

#### Endpoint : /patients/:id

Method | Action | Response
-------|--------|---------
GET    | Get Patient information | [patient information](#Patient-Data)
PUT    | Update Patient information | [patient information](#Patient-Data)
DELETE | Remove Patient information, along with all related data (processed and unprocessed) | success or failure

### Processed Data (Access: Doctors)

_Note:_ Each of these can also be accessed via `/patients/:id/{processed|unprocessed}` to find data on a specific patient. These can probably just use redirects.

#### Endpoint: /processed (/patients/:id/processed)

Method  | Action  | Response
------- | --------| ---------
GET     | Get a list of all of the processed data files that the doctor has | [processed data list](#Data-Lists)

__Query Parameters__

Parameter | Function
----------|---------
patient_id | Return only the list of processed data belonging to a certain patient
date | Return list of data processed after this date

#### Endpoint: /processed/:id (/patients/:id/processed/:id)

Make sure to verify that the doctor attempting to access the data at this endpoint is the owner of it.
Something along the lines of : `if( data.doctorId == requester.id ) { continue... }`

Method  | Action  | Response
------- | --------| ---------
GET     | Get/download the processed data file | [processed data](#Processed-Data)
DELETE  | Remove the processed data file from the database | success or failure

### Unprocessed Data (Access: Doctors)

#### Endpoint: /unprocessed (/patients/:id/unprocessed)

Method  | Action  | Response
------- | --------| ---------
GET     | Get a list of basic metadata on all of a doctor's uploaded data files | [unprocessed data list](#Data-Lists)
POST    | Upload a new data file | id of the newly uploaded data file

__Query Parameters__

Parameter | Function
----------|---------
patient_id | Return only the list of .fcs files belonging to a certain patient
date | Return list of data uploaded after this date

#### Endpoint: /unprocessed/:id (/patients/:id/unprocessed/:id)

Method  | Action  | Response
------- | --------| ---------
GET     | Get/download the unprocessed data file | [unprocessed data](#Unprocessed-Data)
DELETE  | Delete an uploaded data file | success or failure
PUT     | Update basic information of an uploaded data file | id? success/failure?

## JWT

A [JWT](http://www.jwt.io)(JSON Web Token) is a good way of authenticating and keeping basic information on logged in users. It's basically a web token that encodes a certain payload (some data that we want to know about the user), along with a signature that ensures that the payload hasn't been tampered with. In our case, the JWT should contain at least the following information:

```json
{
  "sub": "authentication",
  "id": "58a9f352c524304237ebbe8c",
  "roles" : "admin|doctor",
  "iat" : "1487534224685"
}
```

This is how the client will hold it's state, so no [client state information](https://en.wikipedia.org/wiki/Representational_state_transfer#Stateless) will be held on the server.
If the token does not already exist, it is created __on the server__ when the user logs in, and passes the token to the client which will store it in a cookie (or some local storage). This token is then passed from the client to the server on every request to verify authentication and stay logged in.

__Basically:__

* If the token exists, the user is already logged in
* If the token does not exist, the user needs to log in, and should be redirected to the login page (possibly by the client?)

Language | JWT Libraries
---------|--------------
NodeJS   | [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken)
GO       | [jose](https://github.com/SermoDigital/jose)


## Data Layouts

These are going to be the formats that the user of the api can expect.

### Patient Data

```json
{
  id : "58045f0a632dce25861c4469"
  firstname : "Jon",
  lastname : "Doe",
  doctorId : "580446e2632dce25861c4464"
}
```

### Processed Data

```json
{
  dataPoints : [
    [1,85,200],
    [90,23,534],
    etc.
  ],
  metadata : {
    accuracy : 0.98
    patientId : "58045f0a632dce25861c4469"
    sourceId : "23abc50184d4bb93a1e593daf6f7401d"
  }
}
```

### Unprocessed Data

Just the .fcs data file...

### Data Lists

This will just hold the basic metadata of the uploaded files, not any of the actual data files.

__Processed Data List__

```json
[
  {
    id : int,
    patientId : int,
    patientName : string,
    dateUploaded : date
  },
  etc...
]
```

__Unprocessed Data__

```json
[
  {
    id : int,
    patientId : int,
    patientName : string,
    dateUploaded : date
  },
  etc...
]
```
