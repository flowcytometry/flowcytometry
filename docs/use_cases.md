# Use Cases

## Admin Use Cases

### Adding A Doctor

* Click "New Doctor" button
* Fill out the doctor's information:
    * Name
    * Email
* Press "Create"
* Send new doctor request /doctors
    * Create's new doctor in database
        * Set `doctor.activated = false`
    * Return new doctor's id
    
### Managing Doctors

* Click "Manage" button
* (Optional) Enter a search query
* (Optional) Select a filter and/or order 
* Select a row from the list of doctors that appear

### Deleting a doctor

* Click "Delete Doctor" button
* System should notify that the deletion will delete all of the doctor's data
* Verify that they want to delete
* Send delete request to /doctors/:id
    * Delete all GridFS files and chunks associated with the doctor's id
    * Delete all patients associated with the doctor's id
    * Delete the doctor's account

## Doctor Use Cases

### Adding a patient

* Click "Add Patient" button
* Fill out patient's information:
    * Name
* Press "Create"
    * Send new patient request to /patients
    * Create a new patient account in database
    * Return new patient's id


### Deleting a patient

* Click "Delete Patient" button
* Verify
* Send delete request to /patient/:id
    * Delete all GridFS files and chunks associated with patient's id
    * Delete patient

### Adding a .fcs file

* Click "Add .fcs"
* Choose Patient
* Upload to /patients/:id/unprocessed
    * Web App will receive this file
    * Store the file in MongoDB
    * Get the Id of the newly stored file
    * Enqueue a processing request for that file

### Starting a processing job

Still contemplating this one...

### Downloading .fcs file

* Navigate to unprocessed file list
* Click "Download"
* Send request to /unprocessed/:id
    * Retreive file from mongo
    * Send to client
* Download file to client

### Opening a visualization

* Navigate to processed file list
* Click "Visualize"
* Send request to /processed/:id
    * Retreive file from mongo
    * Send to client
* Download file to client
* Start visualization