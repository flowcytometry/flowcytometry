# Database Design

- [Database Design](#database-design)
    - [Design Choices Made](#design-choices-made)
    - [Top Level Structures](#top-level-structures)
        - [Admin](#admin)
        - [Doctor](#doctor)
        - [Patient](#patient)
    - [GridFS](#gridfs)
        - [Unprocessed File Metadata](#unprocessed-file-metadata)
        - [Processed File Metadata](#processed-file-metadata)
    - [Notes](#notes)
    - [Docs](#docs)

This design for the database schema and setup should allow for easy and intuitive data access. It should have sensical relationships and enough decoupling such that there is not too much replicated information in any part of the schema.
*Refer to the notes at the bottom for some additional information and links.*

## Design Choices Made

One major design choice made here is the choice to use additional documents to hold metadata corrisponding to the (un)processed data files. This was done so that when querying for general information on the files, we won't get back large amounts of data that we may not actually want. If we need to access the BLOB data directly, then we can query for it's metadata document and get the `id` of the file. Using that, we can access and retreive the actual large data files.

## Top Level Structures

### Admin

Field | Type | Function
------|------|---------
id    | long | Unique Identifier
name  | string | The admin's actual name
username | string | The username that the admin can use to login with
email | string | The admin's email that can be used for login and password resets
password | hashed string | A hashed and possibly salted password

### Doctor

Field | Type | Function
------|------|---------
id    | long | Unique Identifier
firstname | string | Doctor's first name
lastname | string | Doctor's last name
username | string | Doctor's Username, can be used for login?
email | string | The email that the doctor can use to login with and request password resets
password | hashed string | A hashed and possibly salted password
activated | bool | Whether or not a doctor has activated the account an admin made for them

### Patient

Field | Type | Function
------|------|---------
id    | long | Unique Identifier
firstname | string | Patient's First Name
lastname | string | Patient's Last Name

## GridFS

### Unprocessed File Metadata

Field | Type | Function
------|------|---------
id    | long | Unique Identifier
filename | string | Unique filename, this is what will be used to retrieve the data from GridFS
doctor\_id | long | Links an .fcs file to the doctor who uploaded it
patient\_id | long | Links an .fcs file to the patient whom the data was taken from
upload\_date | ISODate | The date of upload
blob\_id | long | The location of the actual blob data

### Processed File Metadata

Field | Type | Function
------|------|---------
id    | long | Unique Identifier
filename | string | Unique filename, this is what will be used to retrieve the data from GridFS
doctor\_id | long | Links an .fcs file to the doctor who uploaded it
patient\_id | long | Links an .fcs file to the patient whom the data was taken from
upload\_date | ISODate | The date of upload
source\_id | long | Links a processed file to the source .fcs file from which it was created
accuracy | float(0 - 1.0?) | Represents the processing accuracy requested by the doctor. (In percentage form? I'm not entirely sure how they quantified it TODO: Figure this out)
blob\_id | long | The location of the actual blob data

## Notes

* MongoDB automatically creates an \_id value for all documents created. This is usually in the form of an ObjectId(58045f0a632dce25861c4469). So don't take the `long` as a rule...
* MongoDB's BLOB storage system, GridFS splits up it's blob files into multiple smaller documents that will be recompiled when queried for. (TODO: Better wording?)
* MongoDB actually seems to do a lot of the metadata stuff [already](https://docs.mongodb.com/manual/core/gridfs/), we'd just have to add stuff to the files.metadata field
* All passwords will need to be hashed, a good utility for this could be bcrypt, there should be implementations of it for every major language. [LINKS HERE] 

## Docs

* [MongoDB Manual](https://docs.mongodb.com/manual/)
* [NodeJS Mongo Quick Start](http://mongodb.github.io/node-mongodb-native/2.2/quick-start/?_ga=1.60791214.241561046.1476299602)

GridFS Stuff: 

* [Core](https://docs.mongodb.com/manual/core/gridfs/)
* [Use Cases](https://www.mongodb.com/blog/post/building-mongodb-applications-binary-files-using-gridfs-part-1)
* [Simple Example](https://www.mongodb.com/blog/post/building-mongodb-applications-binary-files-using-gridfs-part-2)
* [Video Explaination](https://www.youtube.com/watch?v=EVIGIcm7o2w)