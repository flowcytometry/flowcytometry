## FLAGS

* process.env.ENV: This is the environment that the application is running in. Possible values are: 
  * `test` for running in a testing environment
  * `dev` for running in development mode, also the default as shown at the top of server.js
  * `prod` for running in production mode, I guess for when we finally 'ship' this?

* process.env.LOG_LEVEL: This is used mainly for clearing up the console while the app is running, the various log levels are noted [here](https://github.com/winstonjs/winston#logging-levels). We're currently using winston for our logging.
