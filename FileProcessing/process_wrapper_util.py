
import json
import tarfile
import fnmatch
import os
from subprocess import call
from bson import ObjectId
import time

import pika
import pymongo
import gridfs

with open('settings.json') as f:
    SETTINGS = json.load(f)

def find_points_txt_and_log_dir(pfromd_folder):
    """ Finds the points file inside of the log directory """
    fil = fnmatch.filter(os.listdir(pfromd_folder), r"log_*")
    for log_dir in fil:
        for file in os.listdir(log_dir):
            if file == "points.txt":
                return (os.path.join(log_dir, file), log_dir)

def move_files(dest='.'):
    """ Extracts all of the needed processing files into a given dir """
    config = SETTINGS['pfromd']
    with tarfile.open(config['tarfile'], 'r') as archive:
        archive.extractall(path=dest)

def setup_rabbit():
    """
    Connects to the  rabbitmq instance from the SETTINGS file, and returns the
    connection, and channels for use with incomming and outgoing messages
    return (conn, incomming, outgoing)
    """
    config = SETTINGS['rabbit']
    username = config['credentials']['username']
    password = config['credentials']['password']
    host = config['host']

    creds = pika.PlainCredentials(username, password)
    # heartbeat interval is turned off because the process takes so long, that the connection will drop
    params = pika.ConnectionParameters(host=host, credentials=creds, heartbeat_interval=0)
    conn = pika.BlockingConnection(params)
    incomming = conn.channel()
    outgoing = conn.channel()
    return (conn, incomming, outgoing)

def setup_mongo():
    """
    Returns handles to the uploaded files and processed files as a tuple
    return (client, uploaded_files, processed_files)
    """
    config = SETTINGS['mongo']
    host = config['host']
    pre_bucket_name = config['buckets']['pre']
    post_bucket_name = config['buckets']['post']
    mongo_client = pymongo.MongoClient(host).get_database('dev')

    # Create buckets for uploaded and processed files
    uploaded_files = gridfs.GridFSBucket(mongo_client, bucket_name=pre_bucket_name)
    processed_files = gridfs.GridFSBucket(mongo_client, bucket_name=post_bucket_name)

    return (mongo_client, uploaded_files, processed_files)


def start_job(filename):
    """ Starts the run_pfromd script """
    config = SETTINGS['pfromd']
    jobs = config['jobs']
    max_points = config['max_points']

    start_time = time.time()
    print('Starting job...')
    call([
        './run_pfromd.sh',
        '-j{}'.format(jobs),
        '-n{}'.format(max_points),
        filename
    ])
    print("Job Complete")
    total_time = (time.time() - start_time) * 1000
    print('total time: {}'.format(total_time))
    return total_time

def pull_fcs_file(file_id, grid: gridfs.GridFSBucket, folder="./"):
    """ Pulls the necessarry file from the mongodb gridfs instance """
    filename = os.path.join(folder, 'fcs_file.fcs')
    with open(filename, 'wb') as fcs_file:
        grid.download_to_stream(ObjectId(file_id), fcs_file)
    return filename

def error_msg(msg, message_body):
    return {"error": msg, "request_data": message_body.decode()}
