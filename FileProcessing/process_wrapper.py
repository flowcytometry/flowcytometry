#!/usr/bin/env python3
"""
This will receive job requests from the web api and begin the processing job,
and store the appropriate files back in the mongodb instance
"""

import json
import os
import uuid
import shutil
from threading import Thread
from time import time
from subprocess import call

from bson import ObjectId
import gridfs
import pika
import util

import process_wrapper_util as procutil
import clustering


class ConsumerThread(Thread):
    """
    ConsumerThread pulls processing requests from the requests queue in our rabbitMQ instance,
    and starts the process, creates the metadata needed, and once the process is complete, saves
    the data inside of our mongodb gridfs instance, and notifies the rest of the system of it's completion.

    This is implemented as a child of the threading.Thread class, so that if needed, we can run multiple
    instances of this worker.
    """
    def __init__(self, ident=uuid.uuid1()):
        """
        This just initializes everything needed for the consumer to run
        This creates connections to mongo and rabbitMQ, creates the folder that the process will be completed
        in, and extracts the pfromd folder into it.
        """
        super().__init__()
        self.thread_id = ident
        # TODO(xavier): Add the full worker name
        worker_name = 'worker_'
        # The folder that all of the operations will be performed inside of
        if not os.path.exists(worker_name):
            os.mkdir(worker_name)

        self.folder = os.path.join('.', worker_name)
        procutil.move_files(self.folder)
        os.chdir(os.path.join(self.folder, 'sd_pfromd_source'))

        # Make sure to build the sanjay program
        print('Building driver.c')
        call(['make'])

        # Set up connections to rabbitMQ and mongo
        conn, incoming, outgoing = procutil.setup_rabbit()
        client, uploaded, processed = procutil.setup_mongo()

        self.rabbit_conn = conn
        self.incoming_chan = incoming
        self.outgoing_chan = outgoing
        self.uploaded_files = uploaded
        self.processed_files = processed
        self.mongo_client = client

    def send_message(self, queue, message):
        """ Publish a message over the created channel """
        msg = json.dumps(message)
        self.outgoing_chan.basic_publish(
            exchange='',
            routing_key=queue,
            body=msg
        )

    def callback(self, chan: pika.channel.Channel, method, props, body):
        """
        Run through the full process
        Receives the needed information in the body param.
        """
        queues = procutil.SETTINGS['rabbit']['queues']
        log_dir = None
        try:
            # Step(2): retreive the message from the queue
            # Decode the message into a dictionary
            decoded_msg = json.loads(body.decode(), object_hook=dict)
            if 'id' not in decoded_msg:
                error = procutil.error_msg('id required', body)
                self.send_message(queues['error'], error)
                return

            # Extract the ids from the decoded message
            source_id = decoded_msg['id']
            doctor_id = decoded_msg['doctor']
            patient_id = decoded_msg['patient']
            metadata_id = decoded_msg['metadata']

            # Update the status of the file to "Processing"
            util.update_metadata(self.mongo_client, metadata_id, {'status': 1})

            start_msg = {
                'doctor': str(doctor_id),
                'patient': str(patient_id)
            }

            self.send_message(queues['start'], start_msg)

            print('pulling id: {}'.format(source_id))

            # Step(3): Pull the uploaded .fcs file from the mongodb instance
            filename = procutil.pull_fcs_file(decoded_msg['id'], self.uploaded_files)

            # Step(4): Start the processing job and make points.txt file
            start_time = time()
            total_time = procutil.start_job(filename)
            end_time = time()

            # Step(5): Find the points.txt file
            points_txt, log_dir = procutil.find_points_txt_and_log_dir('.')
            print(points_txt, log_dir)

            # Step(6): Convert the points.txt file into json, also get the number of points for the metadata
            # It should end up looking like an array of: {'x': '21432425', 'y': '2349732897', 'z': '234087234'}
            # Convert the points into a list of python dictionaries, so that they can be passed
            # converted to json, and so that they can be clustered
            points = util.convert_to_dict_list(points_txt)
            num_points = len(points)

            points_json = json.dumps(points)

            # Add the kmeans cluster
            kmeans_cluster = clustering.kmeans(points, 3)
            clusters = [kmeans_cluster]

            # Step(7): Send the json data to the DB, along with the created metadata
            processed_id = util.sends_to_db(self.mongo_client, points_json, metadata={
                'doctor': str(doctor_id),
                'patient': str(patient_id),
                'metadata': str(metadata_id)
            })

            # Step(8): Update the metadata relating to the processed file
            metadata = {
                'processed_file': str(processed_id),
                'processing_time': total_time,
                'process_start': start_time,
                'process_end': end_time,
                'num_points': num_points,
                'clusters': clusters,
                'status': 2  # Complete
            }

            metadata_id = util.update_metadata(self.mongo_client, metadata_id, metadata)

            # Step(9): Remove the directory after the processing has completed
            shutil.rmtree(log_dir)

            msg = metadata.copy()
            del msg['clusters']
            msg['doctor'] = str(doctor_id)
            msg['patient'] = str(patient_id)

            # Step(10): Send the completion message to the appropriate queue
            self.send_message(queues['complete'], msg)
        except gridfs.errors.NoFile as err:
            error = procutil.error_msg("There is no file in the GridStore with this ID {}".format(source_id), body)
            self.send_message(queues['error'], error)
        except ValueError as err:
            error = procutil.error_msg("Could not decode json", body)
            self.send_message(queues['error'], error)
            print("Json Err: {}".format(err))
            print("Malformed Body: {}".format(body))
        except TypeError as err:
            print(err)
            error = procutil.error_msg("Process request was malformed", body)
            self.send_message(queues['error'], error)
        finally:
            # TODO(xavier): We may not want to ack on the error
            # Also, this might be where we want to remove the log folder.
            if log_dir is not None and os.path.exists(log_dir):
                shutil.rmtree(log_dir)

            chan.basic_ack(delivery_tag=method.delivery_tag)
            print(" Complete ".center(30, '='))

    def run(self):

        # Step(1): Make sure to initialize all of the queues that we'll be using.
        # queue_declare makes a queue, if it hasn't already been made. It's safest to assume that
        # this is running before any other process that might be using these queues, so we should
        # make sure to declare it, even if it's already been done by another application.
        queues = procutil.SETTINGS['rabbit']['queues']
        for _, queue in queues.items():
            self.incoming_chan.queue_declare(queue, durable=True)

        rabbit_server = procutil.SETTINGS['rabbit']['host']
        print("Consumer thread: {} is waiting for messages".format(self.thread_id))
        print("Listening on [server: {}, queue: {}]".format(rabbit_server, queues['requests']))

        # Set the prefetch count so that the queue is only pulled from once the
        # previous request has been completed.
        self.incoming_chan.basic_qos(prefetch_count=1)
        self.incoming_chan.basic_consume(self.callback, queue=queues['requests'])
        self.incoming_chan.start_consuming()


def main():
    """Main function"""
    thread = ConsumerThread()
    thread.start()
    thread.join()


if __name__ == '__main__':
    main()
