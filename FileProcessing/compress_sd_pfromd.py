#!/usr/bin/env python3
""" This is used from recompressing the sd_pfromd folder into either a tar or zip """

import fnmatch
import tarfile
import zipfile
import argparse
import os
from itertools import filterfalse

FOLDER_NAME = 'sd_pfromd_source' # os.path.expanduser('~/sd_pfromd') on the real machine
OUTPUT_NAME = 'sd_pfromd_lite'
EXCLUDE = [
    '*.git*',
    '*.fcs'
]

def in_exclude(filename):
    """ Determines which files we're excluding """
    return any(fnmatch.fnmatch(filename, pattern) for pattern in EXCLUDE)

def tar_compress(output):
    """ Compresses into a tar.gz archive """
    print('Compressing as tar.gz')

    if not output.endswith('.tar.gz'):
        output = output + '.tar.gz'

    with tarfile.open(output, 'w|gz') as archive:
        archive.add(FOLDER_NAME, exclude=in_exclude)


def zip_compress(output):
    """ Compresses into a zip archive """
    print('Compressing as .zip')

    if not output.endswith('.zip'):
        output = output + '.zip'

    with zipfile.ZipFile(output, 'w', compression=zipfile.ZIP_DEFLATED) as archive:
        for root, _, files in os.walk(FOLDER_NAME):
            files[:] = filterfalse(in_exclude, files)
            if in_exclude(root):
                continue
            for file in files:
                archive.write(os.path.join(root, file))

def main():
    """ Program entry point """
    parser = argparse.ArgumentParser()
    parser.set_defaults(archive_type='tar', output='sd_pfromd_lite')
    mutex_group = parser.add_mutually_exclusive_group()

    parser.add_argument('-o', '--output', help="The name of the compressed file")
    mutex_group.add_argument('-t', '--tar', help="compress to .tar.gz file",
                             action='store_const', const='tar', dest='archive_type')
    mutex_group.add_argument('-z', '--zip', help="compress to .zip file",
                             action='store_const', const='zip', dest='archive_type')
    args = parser.parse_args()

    if args.archive_type == 'tar':
        tar_compress(args.output)
    else:
        zip_compress(args.output)
    print('Compression Complete')


if __name__ == '__main__':
    main()
