"""
This module holds all of the clustering operations that we can do on the post processed fcs data.
"""

import os
import sys
import time

from util import convert_to_dict_list

import numpy as np
# import matplotlib.pyplot as plt
# from mpl_toolkits.mplot3d import Axes3D
from sklearn.cluster import KMeans


# def generate_unique_name(filename, ext):
#     """
#     Generates a unique filename with the given filename prefix, and extension
#     """
#     unique_name = '{}.{}'.format(filename, ext)
#     if os.path.exists(unique_name):
#         for i in range(sys.maxsize):
#             unique_name = '{}_{}.{}'.format(filename, i, ext)
#             if not os.path.exists(unique_name):
#                 break
#     return unique_name


# def generate_preview_from_points_file(points_file: str, filename='point_preview'):
#     """
#     Generates a 3D graph preview of the points from a given point file
#     Saved file is an SVG
#     """
#     points = convert_to_dict_list(points_file)
#     points = [list(point.values()) for point in points]
#     pts = np.array(points)
#     figure = plt.figure(1)
#     plot = figure.add_subplot(111, projection='3d')

#     # Scatter plot with the xyz values using blue (b) dots
#     plot.scatter(pts[:, 0], pts[:, 1], pts[:, 2], 'b')
#     plot.autoscale_view(True)

#     # Some stuff
#     plot.set_xlabel("X Axis")
#     plot.set_ylabel("Y Axis")
#     plot.set_zlabel("Z Axis")

#     # Empty tick labels for simplicity, considering this is just a preview.
#     plot.w_xaxis.set_ticklabels([])
#     plot.w_yaxis.set_ticklabels([])
#     plot.w_zaxis.set_ticklabels([])

#     # Generate a unique name by appending a number to the end, if needed
#     filename_svg = generate_unique_name(filename, 'svg')
#     figure.savefig(filename_svg)

#     return filename_svg

# def color(point, centroids):
#     """
#     This associates a point with a color based on it's closest centroid.
#     """
#     colors = ['ff0000', '00ff00', '0000ff'] # [Red, Green, Blue]
#     distances = list(map(lambda cent: np.linalg.norm(point - cent), centroids))
#     index = distances.index(min(distances))
#     return colors[index]

def kmeans(points, n_clusters):
    """
    Generate a Cluster of centroids and colors based on the KMeans clustering algorithm.
    The shape of the cluster is based on the shape in Server/models/cluster.ts
    """
    point_list = [list(point.values()) for point in points]
    pts = np.array(point_list)

    KM = KMeans()
    KM.n_clusters = n_clusters

    KM.fit(pts)
    centroids = KM.cluster_centers_.tolist()
    colors = ['#ff0000', '#00ff00', '#0000ff']

    labels = ['x', 'y', 'z']

    labeled_centroids = [zip(labels, centroid) for centroid in centroids]
    labeled_centroids = map(dict, labeled_centroids)

    shapes = [shape(centroid, color) for centroid, color in zip(labeled_centroids, colors)]

    cluster = {
        'name': 'kmeans',
        'shapes': shapes,
        'date_created': time.time()
    }

    return cluster

def shape(center_pt, color):
    """Returns a centroid shape based on a center point and color"""
    return {
        'shape_type': 'centroid',
        'center_point': center_pt,
        'dimensions': (0, 0, 0),
        'rotation': (0, 0, 0),
        'scale': 1,
        'color': color
    }
