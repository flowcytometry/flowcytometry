![Screen Shot 2017-04-04 at 6.10.14 PM.png](https://bitbucket.org/repo/Kkrqkd/images/3465481886-Screen%20Shot%202017-04-04%20at%206.10.14%20PM.png)

### Authors: 
Xavier Banks, Darian Smalley, Alberto Urbina

### Sponsors:
Dr. Jha, Dr. Pattaneik

University of Central Florida

Dr. Jeff Chang

Florida Hospital

### Problem Statement: 
Design a cyber-infrastructure for creating a virtual- reality experience for cancer pathologists interacting with high-dimensional flow cytometry big-data.

### Motivation: 
A tissue sample (such as a blood or bone marrow sample) may contain millions of cells. Equipment called the flow cytometer can measure 10-50 properties of every cell in such a sample. Thus, the pathologist has access to a lot of information about the patient – 50 measurements/cell × 10 million cells = 500 million measurements = 2 billion bytes = Gigabyte of information per test per patient. This exceeds the cognitive capacity of a pathologist or even a data scientist.

How can one create good 3-D projections of high-dimensional data sets so that clinical pathologists (and patients) can visualize it?

### Disruptive Innovation: 
The newly-implemented SANJAY algorithm is 2.74 times more accurate than competing approaches! It will take N-dimensional data and produce its 3-dimensional projection.

### Scope: 
The core component of the project will be building a cyber- infrastructure to bring the SANJAY algorithm to the market (cancer pathologists, biologists).

## How to build the project

### Server

Our project consists of a few different parts that must be built separately. Additionally, a instance of mongoDB is expected to be installed and accessible.
It also requires that there is an instance of RabbitMQ running. These can all be running on different servers, the locations of which can be specified in the .env environment variable file that's covered in the Environment Variables section.

Building the server/client:
```bash
cd Client
npm install  # Install the client dependencies
npm run build # Build the client project
cd ..
cd Server # server
npm install # Install the server dependencies
npm run build # Build the server project
open http://localhost:3001
```

We've also provided a `build_all.py` python file that will build the project all at once.

#### Environment Variables

Our project uses environment variables for much of our configuraion. We can organize the 
configuration in a .env file. Here is the full list of environment variables used by this
project.

```bash
# Server/.env

API_PORT                            # The port that the web api will run on (default: 3001)
CERTIFICATE_LOCATION                # The signed certificate for the server
PRIVATE_KEY_LOCATION                # The location of the private key for the server

# Email Configuration
EMAIL_SERVICE                       # ** The email service that is used (default:gmail) 
EMAIL_PASSWORD                      # ** The password associated with the email address that the emails will be sent from
EMAIL_USER                          # ** The username for the email

# MongoDB Configuration
MONGO_URI                           # The location of the mongodb database (default: mongodb://localhost/dev)
PREPROCESSED_COLLECTION_NAME        # The name of the uploaded file collection (default: uploaded)
PROCESSED_COLLECTION_NAME           # The name of the processed file collection (default: processed)

TOKEN_EXP_TIME                      # The expiration time for a password reset token (default: 15m)

# RabbitMQ Connection Configuration
RABBIT_SERVER                       # The server address that rabbitmq runs on
RABBIT_PORT                         # The port for that rabbitmq runs on
RABBIT_USERNAME                     # The username of the rabbitmq user
RABBIT_PASSWORD                     # The password for the rabbitmq instance
## Queue names
RABBIT_EMAIL_QUEUE                  # The name of the rabbitmq email queue (default: emails)
RABBIT_PROCESS_COMPLETE_QUEUE       # The name of the completion queue (default: process_complete)
RABBIT_PROCESS_ERROR_QUEUE          # The name of the error queue (default: process_error)
RABBIT_PROCESS_REQUEST_QUEUE        # The name of the request queue (default: process_requests)
RABBIT_PROCESS_START_QUEUE          # The name of the process start queue (default: process_start)

# Client/.env

REACT_APP_API_URI                   # The location of the web api, the client application needs to be rebuilt after changing this (default: 3001)
```

__Note__: The environment variables do not have to be set using this method, if you have a preferred method of setting environment variables, you can do that instead.


### SANJAY Interface

The files of interest here are all of the files in the FileProcessing folder. This is the folder that you can simply copy onto the cluster, or you could just check out the repository and run the files from that folder. The settings for the interface are defined in the settings.json file.

The sanjay interface is made to be constantly running on a compute cluster since it waits for messages from the web api, it may need to be modified to run on the hilbert cluster, since I'm not sure whether or not we can just run the program as we regularly could with simulate. 

It runs using python, so a few python modules will need to be installed. We've included a requirements.txt file, which holds the names of all of the python modules that are needed. These can all be installed at once using the following command:

`pip install -r requirements.txt`

To start the actual SANJAY interface, you have to start the `process_wrapper.py` file. You can run it one of the following ways:

```bash
python ./process_wrapper.py

# or

chmod +x ./process_wrapper.py
./process_wrapper.py
```

## Data Flow Overview
![Client Data Flow Diagram.png](https://bitbucket.org/repo/Kkrqkd/images/1968531717-Client%20Data%20Flow%20Diagram.png)![SANJAY Data Flow.png](https://bitbucket.org/repo/Kkrqkd/images/1482389020-SANJAY%20Data%20Flow.png)