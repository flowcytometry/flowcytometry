#!/usr/bin/env python

""" A build script that builds the entire project, we should add to this as needed
    Each function builds a specific part of the project.
"""

from subprocess import call
import os
import tarfile
import fnmatch

ROOT = os.path.abspath(".")
LOG_FILE = "build_all.log"
ERR_LOG_FILE = "build_all_err.log"


def build_server(log_file, err_log):
    """ Builds the API """
    print(" Backend ".center(20, "="))
    server_path = os.path.join(ROOT, "Server")
    os.chdir(server_path)
    print("Moved into: {}".format(server_path))

    print("Installing Dependencies...")
    call(["npm", "install"], stdout=log_file, stderr=err_log)

    print("Building API")
    call(["npm", "run", "build"], stdout=log_file, stderr=err_log)

def build_client(log_file, err_log):
    """ Builds the frontend """
    print(" Frontend ".center(20, "="))

    client_path = os.path.join(ROOT, "Client")
    os.chdir(client_path)

    print("Moved into: {}".format(client_path))

    print("Installing Dependencies...")
    call(["npm", "install"], stdout=log_file, stderr=err_log)

    print("Building Frontend")
    call(["npm", "run", "build"], stdout=log_file, stderr=err_log)

def zip_processing_files():
    """ Compresses the FileProcessing folder so we can put it on the cluster """
    no_git = lambda name: fnmatch.fnmatch(name, "*.git*")
    os.chdir(ROOT)

    print(" Compressing The FileProcessing Folder ".center(30, "="))
    with tarfile.open("package.tar.gz", "w|gz") as archive:
        archive.add('./FileProcessing/', exclude=no_git)

def main():
    """ Builds everything """
    with open(LOG_FILE, "w") as log, open(ERR_LOG_FILE, "w") as err_log:
        zip_processing_files()
        build_server(log, err_log)
        build_client(log, err_log)

    print(" Done ".center(20, "="))
    print("log files written to: {0} and {1}".format(LOG_FILE, ERR_LOG_FILE))


if __name__ == '__main__':
    main()
