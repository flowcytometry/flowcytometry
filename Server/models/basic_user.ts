'use strict';

import * as mongoose from 'mongoose'
import * as Plugins from './plugins/Plugins'
import * as User from './user';

const Schema = mongoose.Schema;

const UserSchema = new Schema({});

UserSchema.plugin(Plugins.DefaultUserSchema);
UserSchema.plugin(Plugins.PasswordHasher);
UserSchema.plugin(Plugins.DuplicateKeyHandler);

export default mongoose.model<User.IBasicUserModel>('User', UserSchema, 'users');
