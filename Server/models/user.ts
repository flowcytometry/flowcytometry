'use strict';

import { Document } from 'mongoose'

export const validRoles = [
    'admin',
    'doctor'
];

export interface IBasicUserModel extends Document {
    firstname: string;
    lastname: string;
    email: string;
    password: string;
    activated: boolean;
    settings: ISettings;
    organization: string;
    last_login: Date;
    phone: string;
    roles: [string];
    resetPasswordToken: string;
    resetPasswordExpires: Date;
}

export interface IRoleBasedModel extends Document {
    roles: [string];
}

export interface IRoleBasedUser extends IRoleBasedModel, IBasicUserModel {};

export interface ISettings extends Document {};
