//model/patient.js
'use strict';

import * as mongoose from 'mongoose';
import { isValidNumber } from 'libphonenumber-js';

const Schema = mongoose.Schema;

interface IPatient extends mongoose.Document {
  firstname: string;
  lastname: string;
  doctor: string;
  email: string;
  phone: string;
  num_tests: number;
}

//create new instance of the mongoose.schema. the schema takes an object that shows
//the shape of your database entries.
const PatientSchema = new Schema({
  firstname:  { type: String, required: true },
  lastname:   { type: String, required: true },
  doctor:     { type: Schema.Types.ObjectId, required: true },
  email:      { type: String, lowercase: true },
  phone:      { type: String, lowercase: true, validate: (phone) => isValidNumber(phone, 'US') },
  num_tests:  { type: Number, default: 0, min: 0 }
});

//export our module to use in server.js
const Patient = mongoose.model<IPatient>('Patient', PatientSchema);
export default Patient;
