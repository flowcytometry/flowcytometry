//model/admin.js
'use strict';

import * as mongoose from 'mongoose';
import * as Plugins from './plugins/Plugins';
import * as User from './user';

const Schema = mongoose.Schema;

export interface IAdminModel extends User.IBasicUserModel, User.IRoleBasedModel {
    settings: IAdminSettings;
}

export interface IAdminNotifications extends mongoose.Document {
    ticket_submission: boolean;
}

// TODO(xavier): This can probably be abstracted out with the doctor settings as well.
export interface IAdminSettings extends User.ISettings, mongoose.Document {
    notifications: IAdminNotifications;
}

const AdminNotifSettingsSchema = new Schema({
    ticket_submission: { type: Boolean, default: false }
});

const AdminSettingsSchema = new Schema({
    notifications: { type: AdminNotifSettingsSchema, default: AdminNotifSettingsSchema }
});

const AdminSchema = new Schema({
    settings: {type: AdminSettingsSchema, default: AdminSettingsSchema }
});

AdminSchema.plugin(Plugins.DefaultUserSchema);
AdminSchema.plugin(Plugins.RoleBasedUserWithDefault('admin'));
AdminSchema.plugin(Plugins.PasswordHasher);
// AdminSchema.plugin(Plugins.DuplicateKeyHandler);

//export our module to use in server.js
export default mongoose.model<IAdminModel>('Admin', AdminSchema, 'users');
