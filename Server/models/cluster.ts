'use strict';

import * as mongoose from 'mongoose';
const Schema = mongoose.Schema;

interface IVector3 extends mongoose.Document {
    x: number;
    y: number;
    z: number;
}

interface IShape extends mongoose.Document {
    type:           string; // "rect", "sphere", etc...
    center_point:   IVector3;
    dimensions:     Object;
    scale:          IVector3;
    rotation:       IVector3;
}

interface ICluster extends mongoose.Document {
    name:           string;
    point_src:      string;
    date_created:   Date;
    shapes:         [IShape];
}

export interface IClusterModel extends mongoose.Document {
    label: string;
    point_src: string;
    date_created: Date;
    color: String;
    center_point: IVector3;
    radius: number;
}

// Validate the colors based on a color hex regex or a common color name. (hex value is preferred)
export function validateColor(color: string) {
    let COLORS = ['red', 'blue', 'green', 'yellow', 'gray', 'purple', 'black'];
    let hexRegex = RegExp("^#?([A-Fa-f0-9]{3}|[A-Fa-f0-9]{6})$");

    return hexRegex.test(color) || COLORS.indexOf(color) > -1;
}

const Vector3Schema = new Schema({
    x: { type: Number, required: true, default: 0 },
    y: { type: Number, required: true, default: 0 },
    z: { type: Number, required: true, default: 0 }
});

const ShapeSchema = new Schema({
    shape_type:     { type: String, required: true },
    // Possible shape types: Sphere, Cube, Centroid
    position:       { type: Vector3Schema, required: true },
    scale:          { type: Vector3Schema, required: true },
    color:          { type: String, required: true, validate: [validateColor, "Color must be a hex value"] }
});

export const ClusterSchema = new Schema({
    name:           { type: String, required: true },
    date_created:   { type: Schema.Types.Date, required: true, default: Date.now() },
    shapes:         { type: [ShapeSchema] }
});

const ClusterModel = mongoose.model<IClusterModel>('Cluster', ClusterSchema, 'clusters');
export default ClusterModel;
