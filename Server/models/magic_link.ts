import { Schema, Document, model } from 'mongoose';

interface IMagicLink extends Document {
    _id: Schema.Types.ObjectId;
    user: Schema.Types.ObjectId;
    createdAt: Date;
}

// NOTE: If we change this, we will need to drop the index from the mongodb table! or the change
// will have no affect!
// NOTE: command: db.magic_links.dropIndex('createdAt_1')
const expirationTime = process.env.MAGIC_LINK_EXP_TIME || '2h';

const MagicLinkSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'user', required: true },
    createdAt: { type: Date, default: Date.now, expires: expirationTime, required: true }
});

export default model<IMagicLink>('MagicLink', MagicLinkSchema, 'magic_links');
