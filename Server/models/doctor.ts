//model/doctor.js
'use strict';

import * as mongoose from 'mongoose';
import * as Plugins from './plugins/Plugins';
import * as User from './user';

const Schema = mongoose.Schema;

export interface IDoctorModel extends User.IBasicUserModel, User.IRoleBasedModel {
    settings: IDoctorSettings;
}

export interface IDoctorNotifications extends mongoose.Document {
    processing_start: boolean;
    processing_end: boolean;
    ticket_resolution: boolean;
}

export interface IDoctorSettings extends User.ISettings {
    notifications: IDoctorNotifications;
}

// TODO(xavier): Should any of these be set to true by default?
const DoctorNotifSettingsSchema = new Schema({
    processing_start:   { type: Boolean, default: false },
    processing_end:     { type: Boolean, default: true },
    ticket_resolution:  { type: Boolean, default: false }
});

// Doctor schema holds the settings
const DoctorSettingsSchema = new Schema({
    notifications: { type: DoctorNotifSettingsSchema, default: DoctorNotifSettingsSchema }
});

const DoctorSchema = new Schema({
    settings: { type: DoctorSettingsSchema, default: DoctorSettingsSchema }
});

DoctorSchema.plugin(Plugins.DefaultUserSchema);
DoctorSchema.plugin(Plugins.PasswordHasher);

// DoctorSchema.plugin(Plugins.RoleBasedUserWithDefault('doctor'));
DoctorSchema.plugin(Plugins.DuplicateKeyHandler);

//export our module to use in server.js
export default mongoose.model<IDoctorModel>('Doctor', DoctorSchema, 'users');
