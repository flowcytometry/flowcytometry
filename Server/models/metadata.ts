import * as mongoose from 'mongoose';
import { ClusterSchema } from './cluster';

interface IMetadata extends mongoose.Document {
    processed_file: string;
    uploaded_file: string;

    upload_date: Date;
    process_end: Date;
    process_start: Date;
    status: string;
    clusters: [Object];
    num_points: number;

    patient: string;
    doctor: string;
}

// NOTE: If we ever decide that the unique keys are not needed here, we will have to remove
// the collection entirely to get rid of the constraint.
const MetadataSchema = new mongoose.Schema({
    patient:        { type: mongoose.Schema.Types.ObjectId, required: true },
    uploaded_file:  { type: mongoose.Schema.Types.ObjectId, required: true }, // unique?
    processed_file: { type: mongoose.Schema.Types.ObjectId }, // unique?
    upload_date:    { type: Date, required: true, default: Date.now },
    process_start:  { type: Date },
    process_end:    { type: Date },
    status:         { type: Number, default: 0, min: 0, max: 2 },
    // 0 => Waiting, 1 => Processing, 2 => Complete,
    clusters:       { type: [ClusterSchema] },
    num_points:     { type: Number, min: 0 }
});

const Metadata = mongoose.model<IMetadata>('Metadata', MetadataSchema, 'metadata');
export default Metadata;
