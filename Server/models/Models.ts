import admin from './admin';
import user from './basic_user';
import cluster from './cluster';
import doctor from './doctor';
import magic_link from './magic_link';
import metadata from './metadata';
import patient from './patient';

export const Admin = admin;
export const Doctor = doctor;
export const Patient = patient;
export const Cluster = cluster;
export const User = user;
export const MagicLink = magic_link;
export const Metadata = metadata;