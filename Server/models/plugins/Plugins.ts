export * from './role-based';
import duplicateKeyHandler from './handle-dup-key';
import passwordHasher from './secure-passwords';

export const DuplicateKeyHandler = duplicateKeyHandler;
export const PasswordHasher = passwordHasher;
