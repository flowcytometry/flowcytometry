import logger from '../../util/logger';

// http://thecodebarbarian.com/mongoose-error-handling.html
function handleDuplicateKey(err, user, next) {

  // Duplicate key error regarding either the username or email
    if ( err.code === 11000 ) {
        logger.debug('There was a duplicate key error');
        next(new Error('There was a duplicate key error.'));
    } else {
        next();
    }
}

export default function DuplicateKeyHandler(schema, options) {
  // These functions can throw a duplicate key error
  schema.post('save', handleDuplicateKey);
  schema.post('update', handleDuplicateKey);
  schema.post('findOneAndUpdate', handleDuplicateKey);
  schema.post('insertMany', handleDuplicateKey);
}
