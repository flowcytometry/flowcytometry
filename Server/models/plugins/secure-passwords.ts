import * as bcrypt from 'bcryptjs';
import logger from '../../util/logger';

const saltRounds = 10;

function securePassword(next) {
  const user = this;

  // We only want to hash it if it's been modified, otherwise we'd just
  // keep rehashing the previous hash
  if(!user.isModified('password')) {
    logger.debug('password was not modified');
    return next();
  }

  // If there is no password set, then the account is inactive
  if(user.password === undefined) {
    logger.debug('password is undefined, user account is deactivated');
    user.activated = false;
    return next();
  }

  // Hash pasword and activate doctor
  bcrypt.hash(user.password, saltRounds, (err, hash) => {
    if(err) {
      return next(err);
    }
    
    user.password = hash;
    user.activated = true;
    return next();
  });
}

export default function PasswordHasher(schema, options) {
  schema.pre('save', securePassword);
}
