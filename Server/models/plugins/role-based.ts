import { Document, Schema, SchemaOptions } from 'mongoose';
import { validRoles } from '../user';
import logger from '../../util/logger';
import { isValidNumber } from 'libphonenumber-js';

export function DefaultUserSchema(schema: Schema, options) {
    schema.add({
        firstname:  { type: String, required: true },
        lastname:   { type: String, required: true },
        email:      { type: String, required: true, lowercase: true, unique: true, index: true },
        password:   { type: String, default: undefined },
        activated:  { type: Boolean, default: false },
        organization: { type: String, required: true },
        phone:      { type: String, required: true, validate: (phone) => isValidNumber(phone, 'US') },
        last_login: { type: Date },
        roles: { type: [String], validate: (roles: string[]) => { return roles.every(role => validRoles.indexOf(role) >= 0); } },
        settings: {
            notifications: {
                processing_start:   { type: Boolean, default: false },
                processing_end:     { type: Boolean, default: true  },
                ticket_resolution:  { type: Boolean, default: false }
            }
        },
        resetPasswordToken: String,
        resetPasswordExpires: Date
    });
};

export function RoleBasedUser(schema: Schema, options: SchemaOptions) {
    function validateRoles(roles: string[]) {
        return roles.every(role => validRoles.indexOf(role) >= 0);
    }

    schema.add({
        roles: { type: [String], validate: validateRoles }
    });

    schema.methods.addRole = function(role) {
        if(!this.roles.includes(role)) {
            this.roles.push(role);
        }
    };
}

export const RoleBasedUserWithDefault = (role: string) => (schema: Schema, options: SchemaOptions) => {
    RoleBasedUser(schema, options);

    schema.pre('save', function(next) {
        logger.debug('adding this role: ' + role);
        this.addRole(role);
        next();
    });
};
