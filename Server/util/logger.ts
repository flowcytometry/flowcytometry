import winston = require('winston');
const loglevel = process.env.LOG_LEVEL || 'debug';
winston.level = loglevel;

export default winston;
export function PrefixLogger(prefix: string) {
    const bracketed = '[' + prefix + '] ';
    const info  = (s: string) => winston.info(bracketed + s);
    const debug = (s: string) => winston.debug(bracketed + s);  
    const error = (s: string) => winston.error(bracketed + s);
    
    return {
        info: info,
        debug: debug,
        error: error
    };
}
