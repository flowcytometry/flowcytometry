const mongoose = require('mongoose');
const readline = require('readline-sync');

const doctor = require('../models/doctor');
const logger = require('../util/logger');

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/dev')
    .then(() => logger.debug('connected'), () => logger.debug('connection failed'))
    .catch((err) => console.error.bind(console, 'db connection error: ' + err));

var doc = {};

doc.firstname = readline.question('firstname: ');
doc.lastname = readline.question('lastname: ');
doc.username = readline.question('username: ');
doc.email = readline.questionEMail('email: ');
doc.password = readline.question('password: ');
doc.activated = true;

new doctor(doc).save((err, d) => {
    if(err) {
        logger.debug('something went wrong: ' + err);
        process.exit(1);
    } else {
        logger.debug('added doctor: ' + d._id);
        process.exit(1);
    }
});