'use strict';

const mongoose = require('mongoose');
const Doctor = require('../models/doctor');
const Patient = require('../models/patient');
const Admin = require('../models/admin');
const logger = require('./logger');

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/dev')
    .then(() => logger.debug('connected'), () => logger.debug('connection failed'))
    .catch((err) => console.error.bind(console, 'db connection error: ' + err));


function create_user(firstname, lastname, password) {
	const uname = firstname[0] + lastname;
	const email = uname + '@mail.com';
	const user = {
		firstname: firstname,
		lastname: lastname,
		username: uname,
		password: password,
		email: email
	};

	return user;
};

const new_doc = (fname, lname, passwd) => new Doctor(create_user(fname, lname, passwd));
const new_admin = (fname, lname, passwd) => new Admin(create_user(fname, lname, passwd));

function create_patient(firstname, lastname) {
	const patient = {
		firstname: firstname,
		lastname: lastname
	}


	Doctor.findOne()
		.then((doc) => {
			patient.doctor = doc._id;
			return new Patient(patient);
		})
		.catch((err) => {
			logger.debug('could not get a doctor from the db to make a new patient');
			return null;
		});
};

function populate() {
	// Create 5 doctors
	const doctors = [];

	doctors.push(new_doc('bob', 'jones', '0000'));
	doctors.push(new_doc('john', 'dough', '1234'));
	doctors.push(new_doc('streetlamp', 'LeMoose', 'bestever'));
	doctors.push(new_doc('bat', 'man', 'albert'));
	doctors.push(new_doc('Doctor', 'McDoc', 'password1'));

	doctors.forEach((doc) => {
		doc.save()
			.then((usr) => logger.log(`added doctor: ${usr.username}`))
			.catch((err) => logger.log(`could not save doctor ${usr.username}`))
	})

	// Create 5 admins
	const admins = [];

	admins.push(new_admin('x', 'banks', '0001'));
	admins.push(new_admin('d', 'smalley', '0002'));
	admins.push(new_admin('a', 'urbina', '0003'));
	admins.push(new_admin('k', 'mortimer', 'secretlybatman'));
	admins.push(new_admin('admin', 'dmin', 'password1'));

	admins.forEach((admin) => {
		admin.save()
			.then((usr) => logger.log(`added admin: ${usr.username}`))
			.catch((err) => logger.log(`could not save admin ${admin.username}`));
	});

	// Create 2 admin+doctors
	const docadmin1 = new_doc('doc', 'admin1', 'password');
	const docadmin2 = new_admin('doc', 'admin2', 'password');

	docadmin1.addRole('admin');
	docadmin2.addRole('doctor');

	docadmin1.save()
		.then((usr) => logger.info(`added docadmin: ${usr.username}`))
		.catch((err) => logger.debug('could not create docadmin1'));

	docadmin2.save()
		.then((usr) => logger.info(`added docadmin: ${usr.username}`))
		.catch((err) => logger.debug('could not create docadmin1'));

	// Create 20 new patients
	const victims = [];

	for(var i = 0; i < 20; i++) {
		victims.push(create_patient(`firstname${i}`, `lastname${i}`));
	}

	victims.forEach((p) => {
		p.save((err) => {
			logger.debug('could not insert patients');
		});
	});
}

module.exports = create_patient;
