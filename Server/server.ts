'use strict';
// TODO(xavier): Set up https.

//first we import our dependencies...
import * as express from 'express';
import * as gridfs from 'gridfs-stream';
import * as http from 'http';
import * as https from 'https';
import * as mongoose from 'mongoose';
import * as morgan from 'morgan';
import * as path from 'path';
import * as fs from 'fs';
import bodyParser = require('body-parser');
import cookieParser = require('cookie-parser');

import endpoint from './api_utils/endpoint';
import { IndividualEndpoints } from './api_utils/individual-endpoints';
import { SetupGridfs, SetupMongo } from './api_utils/setup_mongo';
import { SetupRabbitMQConnection } from './api_utils/setup_rabbitmq';
import { StartMailService } from './services/email';
import { Admin, User } from './models/Models';
import * as Routes from './routes/Routes';
import login from './routes/login';
import { CheckAuthorization as authorize } from './security/authorization';
import logger from './util/logger';

// Pull in our configuration file.
require('dotenv').config()

// TODO(xavier): replace this with a real secret key
const PRIVATE_KEY_LOCATION = process.env.PRIVATE_KEY_LOCATION;
const CERTIFICATE_LOCATION = process.env.CERTIFICATE_LOCATION;

if (!PRIVATE_KEY_LOCATION) {
    throw "No private key location defined. The PRIVATE_KEY_LOCATION environment variable must be set.";
}

// if (!CERTIFICATE_LOCATION) {
//    throw "No certification location defined. The CERTIFICATE_LOCATION environment variable must be set.";
// }

const secret = fs.readFileSync(PRIVATE_KEY_LOCATION).toString();

// set our port to either a predetermined port number if you have set it up, or 3001
const port = process.env.API_PORT || 3001;
const mode = process.env.NODE_ENV || 'dev';

// Use native Node promises
(mongoose as any).Promise = global.Promise;

// Setup the needed connections
const mongoConn = SetupMongo(mode);
const rabbitConn = SetupRabbitMQConnection();

// and create our instances
var app = express();

// ====== Middleware ======
// now we should configure the API to use bodyParser and look for JSON data in the request body
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 100000 }));
app.use(bodyParser.json({ limit: '50mb' }));
// Used the 'as any' here to suppress typescript errors (the code does work correctly)
app.use(cookieParser(secret) as any);
app.use(morgan(':remote-addr :remote-user :method :url HTTP/:http-version :status :res[content-length] - :response-time ms') as any);


//To prevent errors from Cross Origin Resource Sharing, we will set our headers to allow CORS with middleware like so:
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method');

    // and remove cacheing so we get the most recent comments
    res.setHeader('Cache-Control', 'no-cache');
    next();
});


// ====== Routes ======
app.use('/api/login', login(secret));

// Authorization done within the doctorRoute
app.use('/api/doctors', Routes.Doctor(secret));

// Authorization done at the route level
app.use('/api/patients', authorize(secret, ['doctor']), Routes.Patient(secret));

// TODO(xavier): Make this a littl less hacky?
app.route('/api/admins').get(authorize(secret, ['admin']), IndividualEndpoints(Admin).getAllByRoles(['admin']))
app.use('/api/admins', authorize(secret, ['admin']), endpoint(Admin));
app.use('/api/users', authorize(secret, ['admin']), endpoint(User));

app.use('/api/metadata', Routes.Metadata(secret));
app.use('/api/processed', Routes.PostProcessed(secret, mongoConn));
app.use('/api/preprocessed', Routes.PreProcessed(secret, mongoConn));
app.use('/api/forgot', Routes.Forgot(secret));
app.use('/reset', Routes.ResetPassword(secret));

// =========== Serving the Frontend ===============

// Serve everything that out index.html needs as static files
app.use(express.static(path.resolve(__dirname, '../../Client/build/')));

// always send index.html when a refresh action occurs
app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../Client/build/index.html'));
});

rabbitConn.then(conn => StartMailService(conn));

//starts the server and listens for requests
// Uncomment this line, as well as the httpsServer.listen line to run this using https
// let httpsOptions = {
//    key: fs.readFileSync(PRIVATE_KEY_LOCATION),
//    cert: fs.readFileSync(CERTIFICATE_LOCATION)
// };

const httpServer = http.createServer(app);
// const httpsServer = https.createServer(httpsOptions, app);

httpServer.listen(port, function () {
    logger.debug(`api running on port ${port}`);
});

// httpsServer.listen(8443, function() {
// 	logger.debug('https listening on 8443');
// });


export default app;
