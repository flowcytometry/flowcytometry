import { Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import * as gridfs from 'gridfs-stream';
import * as _ from 'lodash';
import { authCookieName, TOKEN_EXP_TIME } from '../routes/login';
import { PrefixLogger } from '../util/logger';
let logger = PrefixLogger('Auth');

// TODO(xavier): REMOVE THIS BEFORE DEPLOYMENT
const DISABLE_AUTH = process.env.DISABLE_AUTH || false;

// This will be used as middleware to authorize users.
// The client application will be responsible for sending any unauthenticated users
// To the login page.

export interface IRequestWithUserData extends Request {
    user_data: {
        id: string;
    }
}

function refreshToken(token, secret, res) {
    delete token['exp'];
    delete token['iat'];
    console.log('token', token);

    res.clearCookie(authCookieName);
    let refreshed = jwt.sign(token, secret, {expiresIn: TOKEN_EXP_TIME});
    res.cookie(authCookieName, refreshed, {signed: true, httpOnly: true});
}

// secret - the jwt token secret string
// roles - the array of roles that are authorized to access this point. e.g ['doctor', 'admin'] meaning both doctors and admins can access this.
export function CheckAuthorization(secret: string, roles: string[]) {
    return (req: IRequestWithUserData, res: Response, next) => {
        logger.debug('attempting to check authorization');

        // TODO(xavier): REMOVE THIS BEFORE DEPLOYMENT
        if(DISABLE_AUTH) {
            logger.debug('AUTHENTICATION IS DISABLED, SOME THINGS WILL NOT WORK CORRECTLY!');
            next();
        } else {

            const token = req.signedCookies[authCookieName];
            try {

                let decoded = jwt.verify(token, secret);
                // Refresh the token expiration
                refreshToken(decoded, secret, res);

                if(_.intersection(roles, decoded.roles).length === 0) {
                    logger.debug('Roles: ' + roles);
                    logger.debug('decoded.roles: ' + decoded.roles);

                    logger.debug('user does not have access');
                    res.status(403).json({required: roles});
                } else {
                    logger.debug('verified: ' + decoded.id);
                    req.user_data = decoded;
                    next();
                }
            } catch (error) {
                res.clearCookie(authCookieName);
                console.log(error);

                if(error instanceof jwt.TokenExpiredError) {
                    res.send({message: 'Token has expired'}).status(401);
                } else {
                    res.send({message: 'Token could not be verified'}).status(401);
                }

            }
        }
    };
};

// Verify that the information that the user wants to view/modify is their own.
// This is meant to be used _after_ the CheckAuthorization

export function VerifyAdminOrIdentity(req, res, next) {
    if(req.user_data) {
        if( _.includes(req.user_data.roles, 'admin') ) {
            next();
        } else {
            VerifyIdentity(req, res, next);
        }
    }
}

export function VerifyIdentity(req, res, next) {
    if( req.user_data !== undefined ) {
        if( req.user_data.id === req.params.id ) {
            next();
        } else {
            res.sendStatus(403);
        }
    } else {
        const token = req.signedCookies[authCookieName];
        const decoded = jwt.decode(token);

        if(decoded.id === req.params.id) {
            next();
        } else {
            res.sendStatus(403);
        }
    }
}


export function VerifyGridfsOwner(gridfs: gridfs.Grid) {
    return (req: IRequestWithUserData, res: Response, next, file_id) => {
        let doc_id = req.user_data.id;
        // let file_id = req.params.id;

        console.log(gridfs.files.collectionName);

        gridfs.findOne({_id: file_id}, (err, record) => {
            if(err) {
                console.log(err);
                res.status(500);
            } else if( record === null ) {
                res.send({message: 'No file found', data: {}}).status(404);
            // TODO(xavier): Fix this issue. We should be consistent in whether we pass around strings or ObjectIds
            } else if ( String(record.metadata.doctor) !== String(doc_id) ){
                res.sendStatus(403);
            } else {
                next();
            }
        });
    }
}