import * as crypto from 'crypto';

export function GenerateToken(): string {
    let buffer = crypto.randomBytes(20);
    let token = buffer.toString('hex');
    return token;
}
