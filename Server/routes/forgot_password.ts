import { Router, Request, Response  } from 'express';
import { User  } from '../models/Models';
import { SendEmail } from '../services/email';
import { GenerateToken } from '../security/utils';
import * as path from 'path';
import * as crypto from 'crypto';

const router = Router();


export default function routes(secret: string) {

    async function SetUserToken(req: Request, res: Response, next) {
        try {
            let user = await User.findOne({email: req.body.email});
            let token = GenerateToken();
            user.resetPasswordToken = token;
            user.resetPasswordExpires = new Date(Date.now() + 3600000); // One hour
            await user.save();
            
            res.locals.email = user.email;
            res.locals.token = token;

            next();
        } catch (err) {
            res.send({message: 'No user exists with this email.'}).status(400);
        }
    }

    async function SendMail(req: Request, res: Response, next) {
        let email = res.locals.email;
        let token = res.locals.token;
        let message = "You have requested to reset your password\n" +
                      "Please click this link to reset your password:\n\n" +
                      "http://" + req.headers['host'] + "/reset/" + token + "\n\n" +
                      "If you did not request this change, please ignore this message and your password will remain unchanged.";
        let subject = "Password Reset";
        
        SendEmail(email, subject, message);
        res.send({message: 'email sent'});
    }

    router.route('/')
        .post(SetUserToken, SendMail);


    return router;
}
