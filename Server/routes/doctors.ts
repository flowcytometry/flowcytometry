import { Request, Response, Router } from 'express';
import * as jwt from 'jsonwebtoken';
import { IndividualEndpoints } from '../api_utils/individual-endpoints';
import { Doctor } from '../models/Models';
import { CheckAuthorization as auth, VerifyIdentity, VerifyAdminOrIdentity } from '../security/authorization';
import { PrefixLogger } from '../util/logger';
import { GenerateToken } from '../security/utils';
import { SendEmail } from '../services/email';

const logger = PrefixLogger('DoctorRoute');
const router = Router();
const endpts = IndividualEndpoints(Doctor);

export default function routes(secret: string) {
    // More specific routes first
    router.route('/:id')
        .get(auth(secret, ['doctor', 'admin']), VerifyIdentity, endpts.getById)
        .put(auth(secret, ['doctor', 'admin']), VerifyAdminOrIdentity, endpts.put) // Doctors and admins can modify doctors
        .delete(auth(secret, ['admin']), endpts.delById);


    // A function to create a new user, if the user is not set with a password, then they
    // will be sent a confirmation email along with a link to set their new password. 
    async function CreateNewUser(req: Request, res: Response, next) {
        try {
            let user = new Doctor(req.body);
            let subject = "Your Account has been Created for DrFCS";
            let message = "Your account has been created for DrFCS\n" +
                          "You can login to your new account here:\n\n" +
                          "http://" + req.headers['host'] + "/\n\n";

            if(!user.password) {
                let token = GenerateToken();

                let now = new Date();
                let expiration = new Date(now);
                expiration.setDate(now.getDate() + 7);
                user.resetPasswordToken = token;
                user.resetPasswordExpires = expiration;

                message += "Your account has been created without a password," +
                           "please click this link to set your new password:\n\n" +
                           "http://" + req.headers['host'] + "/reset/" + token + "\n\n";
            }


            await user.save();
            SendEmail(user.email, subject, message);

            res.send({message: 'User successfully created', data: {}});
        } catch (err) {
            res.send({message: 'Could not create user', err}).status(400);
        }
    }

    // Only an admin can view the doctor list
    router.use('/', auth(secret, ['admin']));
    router.route('/')
        // TODO(xavier): Fix the getAllByRoles function
        .get(endpts.getAllByRoles(['doctor']))
        .post(CreateNewUser);

    return router;
}
