'use strict';

import * as bcrypt from 'bcryptjs';
import { Request, Response, Router } from 'express';
import { Model } from 'mongoose';
import { IRoleBasedUser } from '../models/user';
import jwt = require('jsonwebtoken');
import { Doctor } from '../models/Models';
import logger from '../util/logger';

export const TOKEN_EXP_TIME = process.env.TOKEN_EXP_TIME || '15m';

var router = Router();
// var cert = require('fs').readFileSync('private.pem');
export const authCookieName = 'auth';

export default function routes(secret: string) {
    async function Login(req: Request, res: Response, next) {
        // TODO(xavier): lookup signed cookies?
        if( req.signedCookies[authCookieName] ) {
            res.clearCookie(authCookieName);
        }

        const email = req.body.email;
        const passwd = req.body.password;

        if( email === undefined || passwd === undefined ) {
            res.status(400).json({mesage: 'email and password required', data: {}});
        }
        else {
            const user = await FindUser(email);

            if( !user ) {
                logger.debug('user does not exist');
                res.status(400).json({message: 'user does not exist', data: {}});
            } else {
                ProcessUserLogin(user, passwd, res);
            }
        }
    }

    function ProcessUserLogin(user: IRoleBasedUser, password: string, res: Response) {
        if( !user.activated ) {
            res.status(403).send({message: 'Inactive user'});
        } else {
            let sendResponse = async (err, matched: Boolean) => {
                if(matched) {
                    user.last_login = new Date;
                    await user.save();
                    let token = CreateToken(user);
                    res.cookie(authCookieName, token, {signed: true, httpOnly: true});
                    let data = {id: user._id}
                    res.json({message: 'login successful', data});
                } else if(err) {
                    logger.error(err);
                    res.sendStatus(500);
                } else {
                    logger.debug('incorrect password');
                    res.status(400).json({message: 'incorrect password'});
                }
            };
            bcrypt.compare(password, user.password, sendResponse);
        }
    }

    function CreateToken(user: IRoleBasedUser) {
        const claim = {
            sub: 'authentication',  // Subject
            id: user._id,
            roles: user.roles
        };

        return jwt.sign(claim, secret, {expiresIn: TOKEN_EXP_TIME});
    }

    async function FindUser(email: string) {
        let lowercaseEmail = email.toLowerCase();
        let neededFields   = {
            _id: true,
            password: true,
            roles: true,
            activated: true
        };

        try {
            return await Doctor.findOne({email: lowercaseEmail}, neededFields);
        }
        catch(err) {
            logger.debug('user does not exist: ' + err);
            return undefined;
        }
    }

    function Logout(req, res) {
        // This is basically the logout mechanisim.
        // We just need to clear the jwt cookie
        logger.debug('logging out');
        if(req.signedCookies[authCookieName] === undefined) {
            // Should this be a 200 code?
            logger.debug('not logged in');
            res.status(200).json({message: 'not logged in', data: {}});
        } else {
            logger.debug('clearing auth cookie');
            res.clearCookie(authCookieName);
            res.status(200).json({message: 'logout successful', data: {}});
        }
    }

    router.route('/')
        .post(Login)
        .delete(Logout);

    return router;
};
