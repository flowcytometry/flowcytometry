import { Request, Response, Router } from 'express';
import * as gridfs from 'gridfs-stream';
import * as mongoose from 'mongoose';
import * as multer from 'multer';
import * as GridFSStorage from 'multer-gridfs-storage';

import { SetupGridfs } from '../api_utils/setup_mongo';
import { ProcessRequestMessage, SendProcessRequest, SetupRabbitMQConnection } from '../api_utils/setup_rabbitmq';
import { Metadata, Patient } from '../models/Models';
import { CheckAuthorization as auth, IRequestWithUserData, VerifyGridfsOwner } from '../security/authorization';
import { PrefixLogger } from '../util/logger';

const logger = PrefixLogger('Preprocessed');
const router = Router();

const collection = process.env.PREPROCESSED_COLLECTION_NAME || 'uploaded';

// Set the metadata field in the gridfs file
function setMetadata(req: IRequestWithUserData, file, cb) {
    logger.debug('Adding metadata');

    // Probably need to add some extra data
    let metadata = {
        doctor: req.user_data.id, // Get the doctorid maybe from jwt instead
        patient: req.params.patient_id
    };
    cb(null, metadata);
}

export default function routes(secret: string, mongoConn: mongoose.Connection) {

    const rabbitConn = SetupRabbitMQConnection();
    const gridfs = SetupGridfs(mongoConn);
    gridfs.collection(collection);


    // This is what we'll use to send the uploaded files straight into the mongodb Gridfs instance
    // We could have also specified a url here instead of 'gfs', but since we have a gridfs instance
    // We've created, we might as well use it for consistency.
    const gridStorage = GridFSStorage({
        gfs: gridfs,
        root: 'uploaded',
        metadata: setMetadata,
        log: true
    });

    gridStorage.on('file', async (file) => {
        let metadata = new Metadata({
            patient: file.metadata.patient,
            uploaded_file: file._id,
            upload_date: file.uploadDate
        });

        await metadata.save();
        let message = new ProcessRequestMessage(file._id, file.metadata.doctor, file.metadata.patient, metadata._id);
        rabbitConn.then(conn => {
            SendProcessRequest(conn, message);
            logger.debug(`Sent message: ${message}`);
        });
    });

    // This is the object that specifies how to upload our files.
    // In this case, an uploaded file is stored in the gridStorage, specified above
    // And will only upload files that pass through the fileFilter
    const upload = multer({
        storage: gridStorage,
        // fileFilter: fileFilter
    });

    async function UploadFcs(req: Request, res: Response, next) {
        try {
            let patient = await Patient.findById(req.params.patient_id);
            patient.num_tests++;
            patient.save();
        } catch (err) {
            logger.error('Could not increment the patients number of tests');
        }

        res.json({ files: req.files, file: req.file }).status(200);
    }

    function DownloadFcs(req: Request, res: Response, next) {
        // Find the file in gridfs and stream it to the client through the response.
        const readstream = gridfs.createReadStream({ _id: req.params.id });
        readstream.pipe(res);
        readstream.on('error', (err) => {
            logger.error('Could not read from the Mongo Database, is it running?');
            res.sendStatus(500);
        });
    }

    // these are both done by doctor id
    async function GetAllFCSMetadata(req: IRequestWithUserData, res: Response, next) {
        // console.log(req.user_data);
        const userId = req.user_data.id;
        let filter = { 'metadata.doctor': userId };
        Object.assign(filter, req.query);

        try {
            let metadata = await gridfs.files.find(filter).toArray();
            res.json({ metadata: metadata });
        } catch (e) {
            logger.error(e);
            res.send('an error occured').status(500);
        }
    }

    function GetSingleFCSMetadata(req: Request, res: Response, next) {
        gridfs.files.findOne({ _id: req.params.id }, (err, record) => {
            if (err) {
                // TODO(xavier): find the correct status code
                res.send('Could not find metadata').status(404);
            } else {
                res.json({ metadata: record });
            }
        });
    }

    router.use(auth(secret, ['doctor']));
    router.param('id', VerifyGridfsOwner(gridfs));
    router.route('/')
        .get(GetAllFCSMetadata);

    router.route('/patient/:patient_id')
        .post(upload.single('file'), UploadFcs);

    router.route('/:id/download')
        .get(DownloadFcs);

    router.route('/:id')
        .get(GetSingleFCSMetadata);

    return router;
}
