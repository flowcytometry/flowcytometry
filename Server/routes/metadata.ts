import { Router } from 'express';
import * as jwt from 'jsonwebtoken';

import { Metadata, Cluster } from '../models/Models';
import { CheckAuthorization as auth } from '../security/authorization';

const router = Router();

export default function routes(secret: string) {
    function getMetadataForPatient(req, res) {
        Metadata.find({ patient: req.params.patient_id })
            .then((data) => res.json({ data }))
            .catch((err) => res.send({ message: 'could not find metadata for this patient', data: {} }));
    }

    function getMetadata(req, res) {
        Metadata.findById(req.params.id)
            .then((data) => res.json({ data }))
            .catch((err) => res.send({ message: 'could not find metadata with this id', data: {} }).status(400));
    }

    function getAllMetadata(req, res) {
        Metadata.find()
            .then((data) => res.json({ data }))
            .catch((err) => res.send({ message: 'something went wrong', data: {} }).status(400));
    }

    function updateMetadata(req, res) {
        Metadata.findByIdAndUpdate(req.params.id, req.body)
            .then((data) => res.json({ message: 'updated metadata successfully', data}))
            .catch((err) => res.send({ message: 'could not update metadata: ', err, data: {} }).status(400));
    }

    async function addCluster(req, res) {
        try {
            let metadata = await Metadata.findById(req.params.id);
            let cluster = new Cluster(req.body);
            metadata.clusters.push(cluster);
            await metadata.save();
            res.send({ message: 'cluster successfully added!', data: {} });
        } catch (error) {
            res.send({ message: 'could not add cluster to metadata', data: {}, error: error }).status(400);
        }
    }

    router.use(auth(secret, ['doctor']));
    // router.route('/').get(getAllMetadata);
    router.route('/patient/:patient_id')
        .get(getMetadataForPatient);

    router.route('/:id')
        .get(getMetadata)
        .put(updateMetadata);

    router.route('/:id/addcluster')
        .post(addCluster);

    return router;
}
