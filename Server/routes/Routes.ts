import doctor from './doctors';
import login from './login';
import patient from './patients';
import postprocessed from './postprocessed';
import preprocessed from './preprocessed';
import metadata from './metadata';
import reset from './reset_password';
import forgot from './forgot_password';

export const Doctor = doctor;
export const Login = login;
export const Patient = patient; 
export const PostProcessed = postprocessed;
export const PreProcessed = preprocessed;
export const Metadata = metadata;
export const ResetPassword = reset;
export const Forgot = forgot;
