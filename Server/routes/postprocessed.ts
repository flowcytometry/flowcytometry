import { NextFunction, Request, Response, Router } from 'express';
import * as gridfs from 'gridfs-stream';
import * as mongoose from 'mongoose';

import { SetupGridfs } from '../api_utils/setup_mongo';
import { Cluster, Metadata } from '../models/Models';
import { CheckAuthorization as auth, IRequestWithUserData, VerifyGridfsOwner } from '../security/authorization';
import { PrefixLogger } from '../util/logger';

const router = Router();
const logger = PrefixLogger('PostProcessed');

const collection = process.env.PROCESSED_COLLECTION_NAME || 'processed'

class RandomPoint {
    x: number;
    y: number;
    z: number;

    constructor(range: number) {
        let randomInRange = (r: number) => Math.floor(Math.random() * r);
        this.x = randomInRange(range);
        this.y = randomInRange(range);
        this.z = randomInRange(range);
    }
}

export default function routes(secret: string, conn: mongoose.Connection) {

    const gridfs = SetupGridfs(conn);
    gridfs.collection(collection);
    logger.info(gridfs.files.collectionName);

    function DownloadProcessedData(req: IRequestWithUserData, res: Response, next) {
        let doc_id = req.user_data.id;
        let file_id = req.params.id;

        // this is what should happen after everything is set up
        gridfs.files.findOne({_id: file_id}, (err, record) => {
            if(err) {
                res.sendStatus(500);
            } else {
                res.contentType('application/json');
                let stream = gridfs.createReadStream({_id: file_id});
                let data = '';

                stream.on('data', (chunk) => {
                    data += chunk;
                });

                stream.on('end', () => {
                    data = JSON.parse(data);
                    res.json({data});
                });

                stream.on('error', (err) => {
                    logger.error('Could not read from the Mongo Database, is it running?');
                    res.sendStatus(500);
                });
            }
        });
    }

    async function GetAllProcessedMetadata(req: IRequestWithUserData, res: Response, next) {
        // console.log(req.user_data);
        const userId = req.user_data.id;
        let filter = { 'metadata.doctor': userId };
        Object.assign(filter, req.query);
        try {
            let metadata = await gridfs.files.find(filter).toArray();
            res.json({metadata: metadata});
        } catch(e) {
            logger.error(e);
            res.send('an error occured').status(500);
        }
    }

    function GetSingleProcessedMetadata(req, res) {
        gridfs.findOne({_id: req.params.id}, (err, record) => {
            if(err) {
                // TODO(xavier): find the correct status code
                res.send('Could not find metadata').status(400);
            } else {
                res.json({metadata: record});
            }
        })
    }

    router.use(auth(secret, ['doctor']));
    router.param('id', VerifyGridfsOwner(gridfs));
    router.route('/')
        .get(GetAllProcessedMetadata);

    router.route('/:id')
        .get(GetSingleProcessedMetadata);

    router.route('/:id/download')
        .get(DownloadProcessedData)

    return router;
}
