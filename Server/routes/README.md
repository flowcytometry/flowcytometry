Endpoint Documentation
======================

# Login

* URL: `/api/login`
* Method: `POST`
* URL Encoded Parameters:
```
email: string
password: string
```
## Example Requests

* Using cURL
```bash
curl -X POST -d 'email=xbanks@example.com' -d 'password=0000' graphics.cs.ucf.edu:3001/api/login
```
* Using javascript

```javascript
var data = { email: 'xbanks@example.com', password: '0000' }
$.ajax({
    type: 'POST',
    url: 'graphics.cs.ucf.edu:3001/api/login', // Or however it's done in relation to the frontend
    data: data.encode()
})
// Not entirely sure which one of these will work??
fetch( 'graphics.cs.ucf.edu:3001/api/login', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: new FormData(data)
})
```
## Example Response

```json
{
  "message": "login successful",
  "id": "58962142c800b361b1d13f66"
}
```

# Doctors Route (/api/doctors)

## Getting information on a single doctor

* URL: `/api/doctors/:id`

* Method: `GET`

* URL Parameters:
```
id: The doctor's ID
```

### Example Requests

* Using cURL

```bash
curl 
```

# Upload file

```javascript
var form = new FormData();
form.append("patient", "1234");
form.append("file", "002.fcs");

var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://localhost:3001/api/preprocessed",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache",
    "postman-token": "1a6a1346-b67c-517a-230a-a7bb81946165"
  },
  "processData": false,
  "contentType": false,
  "mimeType": "multipart/form-data",
  "data": form
}

$.ajax(settings).done(function (response) {
  console.log(response);
});
```