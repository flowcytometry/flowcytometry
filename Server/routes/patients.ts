import * as express from 'express';
import * as jwt from 'jsonwebtoken';

import Patient from '../models/patient';
import { CheckAuthorization as auth } from '../security/authorization'

const router = express.Router();

export default function routes(secret) {
    function getPatients(req, res, next) {
        const doctorId = req.user_data.id;
        let filter = {doctor: doctorId};
        Object.assign(filter, req.query);

        Patient.find(filter)
            .then((data) => res.json({data}))
            .catch((err) => res.sendStatus(400));
    }

    function createPatient(req, res, next) {
        const newPatient = req.body;
        newPatient.doctor = req.user_data.id;
        let patient = new Patient(newPatient);
        patient.save()
            .then((data) => res.json({data}))
            .catch((err) => res.sendStatus(400));
    }

    function findSinglePatient(req, res, next) {
        let doc_id = req.user_data.id;
        Patient.findOne({_id: req.params.id, doctor: doc_id})
            .then((data) => res.json({data}))
            .catch((err) => res.sendStatus(400));
    }

    function removePatient(req, res, next) {
        let doc_id = req.user_data.id;
        // Make sure that patients can only be deleted by their doctor
        Patient.findOneAndRemove({_id: req.params.id, doctor: doc_id})
            .then((data) => res.json({data: data._id}))
            .catch((err) => res.sendStatus(400));
    }

    function updatePatient(req, res, next) {
        let doc_id = req.user_data.id;
        Patient.findOneAndUpdate({_id: req.params.id, doctor: doc_id}, req.body)
            .then((data) => res.json({data: data._id}))
            .catch((err) => res.sendStatus(400));
    }

    router.use(auth(secret, ['doctor']))
    router.route('/')
        .get(getPatients)
        .post(createPatient);

    router.route('/:id')
        .get(findSinglePatient)
        .put(updatePatient)
        .delete(removePatient);

    return router;
}
