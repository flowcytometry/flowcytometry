import { Request, Response, Router } from 'express';
import * as path from 'path';
import { User } from '../models/Models';
import { SendEmail } from '../services/email';

const router = Router();

export default function(secret: string) {

    async function SendResetPage(req: Request, res: Response, next) {
        try {
            let user = await User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } });
            res.sendFile(path.resolve(__dirname, '../../../Client/build/reset.html'));
        } catch (err) {
            res.send({message: 'Token is invalid, or has expired'}).status(400);
        }
    }

    async function ResetPassword(req: Request, res: Response, next) {
        try {
            let user = await User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } });
            user.password = req.body.password;
            user.resetPasswordExpires = undefined;
            user.resetPasswordToken = undefined;
            
            await user.save();

            let subject = "Confirmation of password change";
            let message = "Hello,\n\n" +
                          "This is a confirmation of the password change for " + user.email + ".\n\n";

            SendEmail(user.email, subject, message);

            res.redirect('/');
        } catch (err) {
            res.send({message: 'Token is invalid, or has expired'}).status(400);
        }
    }

    router.route('/:token')
        .get(SendResetPage)
        .post(ResetPassword);

    return router;
}
