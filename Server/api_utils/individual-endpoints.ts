'use strict';

/* This is used as a more modular version of endpoints.js
 * Use this for slightly more fine grained control over methods in a path
 * If you need even more control, you'll probably have to create your own method endpoint.
*/

export function IndividualEndpoints(model) {
    function post(req, res, next) {
        model.create(req.body, (err, modelInstance) => {
            if (err) {
                // Here I need to send an appropriate response.
                // Something that means: "Duplicate/Incorrect Data?"
                console.log(req.body);
                console.error("doc post", err);
                res.sendStatus(400);
            } else {
                res.json({message: model.modelName + ' successfully added!', data: modelInstance._id});
            }
        });
    }

    const getAllByRoles = (roles) => function(req, res, next) {
        let filter = {roles: { $in: roles}};
        Object.assign(filter, req.query);
        model.find(filter, { password: false }, function(err, data) {
            if(err) {
                return next(err);
            } else {
                res.json({data});
            }
        });
    };

    function getAll(req, res, next) {
        // We don't want to expose passwords, even if they're hashed
        model.find(req.query, { password: false }, function(err, data) {
            if(err) {
                return next(err);
            } else {
                res.json({data});
            }
        });
    }

    function getById(req, res, next) {
        // We don't want to expose passwords, even if they're hashed
        model.findById(req.params.id, { password: false }, function(err, data) {
            if(err) {
                return next(err);
            } else {
                res.json({ message: model.modelName + ' successfully found!', data});
            }
        });
    }

    function put(req, res, next) {
        model.findByIdAndUpdate(req.params.id, req.body, function(err, data) {
            if (err) {
                return next(err);
            } else {
                res.json({data});
            }
        });
    }

    function delById(req, res, next) {
        model.findByIdAndRemove(req.params.id, { password: false, _id: true }/*req.query*/, function(err, data) {
            if (err) {
                return next(err);
            } else {
                res.json({data});
            }
        });
    }

    return {
        getAll: getAll,
        getAllByRoles: getAllByRoles,
        post: post,
        getById: getById,
        put: put,
        delById: delById
    };
};
