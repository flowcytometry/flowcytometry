'use strict';

import {Request, Response, Router} from 'express';

export default function endpoints(model) {
    var router = Router();
    router.route('/')
        .get((req, res, next) => {
            model.find(req.query, { password: false }, function(err, data) {
                if(err) {
                    return next(err);
                }

                res.json({data: data});
            });
        })
        .post((req, res, next) => {
            model.create(req.body, (err, modelInstance) => {
                if (err) {
                    // Here I need to send an appropriate response.
                    // Something that means: "Duplicate/Incorrect Data?"
                    res.sendStatus(400);
                } else {
                    res.json({message: model.modelName + ' successfully added!', data: modelInstance._id});
                }
            });
        });

    router.route('/:id')
        .get((req, res, next) => {
            model.findById(req.params.id, { password: false }, function(err, data) {
                if(err) {
                    return next(err);
                }

                if(data === null) {
                    res.json({ message: model.modelName + ' could not be found.', data})
                } else {
                    res.json({ message: model.modelName + ' successfully found!', data});
                }
            });
        })
        .put(function(req, res, next) {
            model.findById(req.params.id, function(err, data) {
                if(err) {
                    return next(err);
                }

                data.set(req.body);
                data.save();
                
                res.json({data: data._id});
            });
        })
        .delete(function(req, res, next) {
            model.findByIdAndRemove(req.params.id, req.query, function(err, post) {
                if (err) {
                    return next(err);
                }

                res.json({data: post});
            });
        });

    return router;
};
