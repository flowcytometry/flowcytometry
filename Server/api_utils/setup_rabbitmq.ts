import * as amqp from 'amqplib';
import { PrefixLogger } from '../util/logger';

const rabbitServer   = process.env.RABBIT_SERVER        || 'graphics.cs.ucf.edu'; // will need to be localhost if we're using the guest login
const rabbitPort     = process.env.RABBIT_PORT          || 5672;
const rabbitUsername = process.env.RABBIT_USERNAME      || 'test'; // guest can only connect to localhost sessions
const rabbitPassword = process.env.RABBIT_PASSWORD      || 'test1234';

const process_request_queue = process.env.RABBIT_PROCESS_REQUEST_QUEUE || 'process_requests';
const process_start_queue = process.env.RABBIT_PROCESS_START_QUEUE || 'process_start';
const process_complete_queue = process.env.RABBIT_PROCESS_COMPLETE_QUEUE || 'process_complete';
const process_error_queue = process.env.RABBIT_PROCESS_ERROR_QUEUE || 'process_error';
export const email_queue = process.env.RABBIT_EMAIL_QUEUE || 'emails';

const url = `amqp://${rabbitUsername}:${rabbitPassword}@${rabbitServer}:${rabbitPort}/`;

const logger = PrefixLogger('RabbitMQ');

export const queue_options: amqp.Options.AssertQueue = {
    durable: true
};

export const publish_options: amqp.Options.Publish = {};

export class ProcessRequestMessage {
    constructor(public id: string,
                public doctor: string,
                public patient: string,
                public metadata: string) {}
}

export function SetupRabbitMQConnection() {
    let conn = amqp.connect(url);
    conn.then(conn => {
        logger.info('Connection successful: ' + url);
        conn.on('close', () => {
            logger.info('Connection closed');
        });

        // make sure all of the channels we're using are initialized
        conn.createChannel().then(chan => {
            chan.assertQueue(email_queue, queue_options);
            chan.assertQueue(process_error_queue, queue_options);
            chan.assertQueue(process_complete_queue, queue_options);
            chan.assertQueue(process_request_queue, queue_options);
            logger.info('Created the channels');
        });

    }).catch(err => {
        logger.error(`Could not make a connection to the RabbitMQ instance at ( ${rabbitServer}:${rabbitPort} ). Is it running?`);
        logger.error(err);
    });
    return conn;
}

export function SendProcessRequest(conn: amqp.Connection, msg: ProcessRequestMessage) {
    const json_msg = JSON.stringify(msg);
    const msg_buffer = Buffer.from(json_msg);

    conn.createChannel().then(chan => {
        chan.assertQueue(process_request_queue, queue_options);
        chan.sendToQueue(process_request_queue, msg_buffer, publish_options);
        logger.debug(`Sent message: ${json_msg}`);
    });

}

export function ConsumeCompletedJobs(conn: amqp.Connection, fn: (msg: Object) => void) {
    const comp_logger = PrefixLogger('Complete Consumer');
    conn.createChannel().then(chan => {
        chan.assertQueue(process_complete_queue, queue_options);
        chan.consume(process_complete_queue, (msg) => {
            let str = msg.content.toString();
            let decoded = JSON.parse(str);
            comp_logger.debug('Received message: ' + msg.content);
            comp_logger.debug('Decoded to: ' + decoded);
            fn(decoded);
            chan.ack(msg);
        });
    });
}

export function ConsumeErrors(conn: amqp.Connection, fn: (msg) => void ) {
    const err_logger = PrefixLogger('Error Consumer');
    conn.createChannel().then(chan => {
        chan.assertQueue(process_error_queue, queue_options);
        chan.consume(process_error_queue, (msg) => {
            let str = msg.content.toString();
            let message = JSON.parse(str);
            err_logger.debug('received message' + msg.content);
            err_logger.debug('decoded to ' + message);
            fn(message);
            chan.ack(msg);
        });
    });
}

export function ConsumeStarts(conn: amqp.Connection, fn: (msg) => void) {
    const start_logger = PrefixLogger('Start Consumer');
    conn.createChannel().then(chan => {
        chan.assertQueue(process_start_queue, queue_options);
        chan.consume(process_start_queue, (msg) => {
            let str = msg.content.toString();
            let decoded = JSON.parse(str);
            start_logger.debug('Received message: ' + msg.content);
            start_logger.debug('Decoded to: ' + decoded);
            fn(decoded);
            chan.ack(msg);
        });
    });
}