import * as gridfs from 'gridfs-stream';
import * as mongoose from 'mongoose';
import { PrefixLogger } from '../util/logger';
const logger = PrefixLogger('Mongo');

(mongoose as any).Promise = global.Promise;

const mongoURI = {
    prod: 'mongodb://localhost/prod',
    dev:  'mongodb://localhost/dev',
    test: 'mongodb://localhost/testing',
    graphics: 'mongodb://graphics.cs.ucf.edu/dev'
};


export function SetupMongo(mode: string) {
    const dbname = process.env.MONGO_URI || mongoURI[mode];

    // Creates a default connection that the models will use to initialize themselves

    let db = mongoose.connection;

    db.on('connecting', () => {
        logger.info('Connecting to mongo: ' + dbname);
    });

    db.on('connected', () => {
        logger.info('default connection successful.')
    });

    db.on('error', (err) => {
        logger.error('There was an error!');
        logger.error(err);
    });

    db.on('disconnected', () => {
        logger.error('Disconnected, is the mongo instance still running?');
    });

    db.on('reconnected', () => {
        logger.info('Connection was reestablished!');
    })

    // Connect, and keep trying to reconnect if unsuccessful.
    mongoose.connect(dbname,  { server: { reconnectTries: Number.MAX_VALUE } }).catch(err => logger.error('Could not make a default connection to mongo.'));
    return db;
}

export function SetupGridfs(conn: mongoose.Connection) {
    // Maybe i could use a global thing? global.GridFS = gridfs(conn.db, mongoose.mongo);
    conn.on('error', (err) => {
        logger.error('[GridFS] Could not make gridfs connection');
    });
    conn.on('open', () => {
        logger.info('[GridFS] New GridFS connection has been made.');
    })
    const gfs = gridfs(conn.db, mongoose.mongo);
    return gfs;
}
