process.env.NODE_ENV = 'test';
// process.env.LOG_LEVEL = 'debug';

// import * as chai from 'chai';
let chai = require('chai')
import Doctor from '../models/doctor';
import Patient from '../models/patient';
import server from '../server';
import logger from '../util/logger';

let chaihttp = require('chai-http');
const should = chai.should();
const URI = '/api/patients';
const LOGIN_URI = '/api/login';

chai.use(chaihttp);

describe('/api/patients', function() {

    var testDoctor = {
        id: 0,
        firstname: 'Test',
        lastname: 'Doctor',
        username: 'tesddoc',
        password: 'pa$$word',
        email: 'test@email.com',
        roles: ['doctor']
    };

    var loginAndDo = (agent, f) => agent.post(LOGIN_URI).send({
        email: testDoctor.email,
        password: testDoctor.password
    }).then(f);

    // Make sure that there is a doctor that we can use as a login
    before(function(done) {
        var doc = new Doctor(testDoctor);
        var addPatients = (id) => {
            var p1 = new Patient({
                firstname: 'patient1',
                lastname: 'last1',
                doctor: id
            });
            var p2 =new Patient({
                firstname: 'patient2',
                lastname: 'last2',
                doctor: id
            });

            p1.save();
            p2.save();
        };

        doc.save((err, doc) => {
            if(err) {
                logger.debug('could not add test doctor at the beginning of test');
                done();
            } else {
                testDoctor.id = doc._id;
                addPatients(testDoctor.id);
                done();
            }
        });
    });

    // Remove the doctor from the test database
    after(function(done) {
        Doctor.remove({_id: testDoctor.id}, (err) => {
            logger.debug('doctor removed.');
        });

        Patient.remove({doctor: testDoctor.id}, (err) => {
            logger.debug('patients removed');
            done();
        });
    });

    describe('GET', function() {
        it('should throw a 403 without login', function(done) {
            chai.request(server)
                .get(URI)
                .end(function(err, res) {
                    res.should.have.status(401);
                    done();
                });
        });

        it('should retrieve patients on get', function(done) {
            var agent = chai.request.agent(server);
            loginAndDo(agent, function(res) {
                res.should.have.status(200);

                agent.get(URI).end(function(err, res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(2);
                    done();
                });
            });
        });
    });

    describe('POST', function() {
        it('should reject without login', function(done) {
            chai.request(server)
                .post(URI)
                .send({})
                .end(function(err, res) {
                    res.should.have.status(401);
                    done();
                });
        });

        it('should successfully add a patient to the db', function(done) {
            var agent = chai.request.agent(server);
            loginAndDo(agent, function(res) {
                res.should.have.status(200);

                agent.post(URI).send({
                    firstname: 'postedtest',
                    lastname: 'postedlastname'
                }).end(function(err, res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a('object');
                    res.body.should.have.property('message');
                    res.body.should.have.property('data');
                    res.body.data.should.have.property('firstname').eql('postedtest');
                    res.body.data.should.have.property('lastname').eql('postedlastname');
                    res.body.data.should.have.property('doctor').eql(`${testDoctor.id}`);
                    done();
                });
            });
        });
    });

    describe('GET :/id', function() {
        it('should return 401 without login');
        it('should not show patients to doctors they are not assigned to');
        it('should return a single patient');
    });

    describe('PUT /:id', function() {
        it('should not allow a doctor to update another doctors patient');
        it('should update a patient');
    });

    describe('DELETE /:id', function() {
        it('should remove a patient');
        it('should only allow the correct doctor to remove a patient');
    });
});
