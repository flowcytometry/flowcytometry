import { assert, expect, should } from 'chai';
import { Admin, Doctor } from '../models/Models';

process.env.NODE_ENV = 'test';

import '../server';
import logger from '../util/logger';

describe('User Roles', () => {

    afterEach(async () => {
        await Admin.collection.drop();
    });

    it('should allow only roles defines in RoleTypes', function(done) {

        let doc = new Doctor({
            firstname: 'name',
            lastname: 'asd',
            username: 'asd',
            email: 'asd@email.com',
            roles: ['admins']
        });

        expect(doc.errors).to.be.undefined;

        let doc2 = new Doctor({
            firstname: 'name',
            lastname: 'asd',
            username: 'something',
            email: 'asd@email.com',
            roles: ['notright', 'othernotright']
        });

        let errors = doc2.validateSync('roles');
        expect(doc2.errors).to.not.be.undefined;
        done();
    });

    it('should automatically assign roles', async () => {

        let doc = new Doctor({
            firstname: 'name',
            lastname: 'asd',
            username: 'asd',
            email: 'asf@email.com'
        });

        let admin = new Admin({
            firstname: 'name',
            lastname: 'asd',
            username: 'asd',
            email: 'asd@email.com'
        });

        await doc.save();
        expect(doc.roles).to.contain('doctor');

        await admin.save();
        expect(admin.roles).to.contain('admin');
    });

    // TODO(xavier): maybe they should hash passwords even before they're entered into the db?
    it('should hash passwords on save', async () => {

        let admin = new Admin({
            firstname: 'admin',
            lastname: 'mcadmin',
            username: 'adminguy',
            email: 'admin@example.com',
            password: '0000'
        });

        let doctor = new Doctor({
            firstname: 'doctor',
            lastname: 'mcdoctor',
            username: 'doctorguy',
            email: 'doctor@example.com',
            password: '0000'
        });

        try {
            await admin.save();
            await doctor.save();

            assert(admin.password !== '0000');
            assert(doctor.password !== '0000');
            assert(admin.password !== doctor.password);
        } catch (e) {
            assert(false, 'Something Failed' + e);
        }
    })

});
