process.env.NODE_ENV = 'test';
// process.env.LOG_LEVEL = 'error';


const chai = require('chai');
const should = chai.should;
import Doctor from '../models/doctor';
import server from '../server';
import { PrefixLogger } from '../util/logger';


const logger = PrefixLogger('LoginTest');
const URI = '/api/login';

describe('/api/login', function() {

    // user for login testing
    var testUser = {
        id: 0,
        firstname: 'Login',
        lastname: 'Dude',
        username: 'Logan',
        email: 'Logan@login.com',
        password: 'password1'
    };

    const inactiveUser = {
        firstname: 'test',
        lastname: 'user',
        username: 'inactive',
        email: 'inactive@example.com',
        password: 'password1'
    }

    before(function(done) {
        const doc = new Doctor(testUser);
        const inactive = new Doctor(inactiveUser);

        doc.save()
            .then((document) => {
                testUser.id = document._id
            })
            .catch((err) => logger.error('doctor could not be saved'));

        inactive.save()
            .then((document) => inactive._id = document._id)
            .then(() => Doctor.findByIdAndUpdate(inactive._id, {activated: false}))
            .then((document) => {
                logger.info('Inactive: ' + document.activated);
                done();
            })
            .catch((err) => logger.error('could not save deactivated doctor'));
    });

    after(function(done) {
        // Remove the test user
        Doctor.remove({_id: testUser.id}, (err) => {
            done();
        });
    });

    it('should not require authentication send requests', function(done) {
        chai.request(server)
            .post(URI)
            .send({})
            .end(function(err, res) {
                res.should.not.have.status(401);
                res.should.not.have.status(403);
                done();
            });
    });

    it('should reject logins with incorrect emails', function(done) {
        chai.request(server)
            .post(URI)
            .send({
                email: 'not-the-right-email@gmail.com',
                password: 'does not matter'
            })
            .end(function(err, res) {
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property('message');
                res.body.message.should.equal('user does not exist');

                done();
            });
    });

    it('should reject logins with incorrect passwords', function(done) {
        chai.request(server)
            .post(URI)
            .send({
                email: testUser.email,
                password: 'not the right password'
            })
            .end(function(err, res) {
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property('message');
                res.body.message.should.equal('incorrect password');

                done();
            });
    });

    it('should respond with id and success message on successful login', function(done) {
        var agent = chai.request.agent(server);
        agent.post(URI)
            .send({
                email: testUser.email,
                password: testUser.password
            })
            .then(function(res) {
                res.should.have.status(200);
                res.should.have.cookie('auth');
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('message');
                res.body.should.have.property('id');
                res.body.message.should.equal('login successful');
                res.body.id.should.equal(`${testUser.id}`); // Since it returns a string

                agent.del(URI).end(function(err, res) {
                    res.should.not.have.cookie('auth');
                    done();
                });
            });
    });

    it('should allow for logout on DELETE', function(done) {
        chai.request(server)
            .del(URI)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                done();
            });
    });

    it('should not allow login for an inactive user', function(done) {
        chai.request(server)
            .post(URI)
            .send({
                email: inactiveUser.email,
                password: inactiveUser.password
            })
            .end(function(err, res) {
                res.should.have.status(403);
                res.body.message.should.equal('Inactive user');
                done();
            });
    });
});