process.env.NODE_ENV = 'test';
process.env.LOG_LEVEL = 'error';

import {assert, expect, should} from 'chai';
import { Cluster } from '../models/Models';
import { validateColor } from '../models/cluster';
import server from '../server';

describe('The Cluster Model', async () => {

    it('the color validator should work', async () => {
        assert(validateColor('randomstring') == false, 'random strings should not be valid');
        expect(validateColor('#0f0f0f'), '7 letter hex values should be valid').to.be.true;
        expect(validateColor('#fff'), '7 letter hex values should be valid').to.be.true;
        expect(validateColor('1234567'), 'numbers as strings should not be valid').to.be.false;

        let cluster = new Cluster({
            label: 'somelabel',
            point_src: "5856c61e072fdd4b30215f04",
            color: '#fff'
        });

        let badcluster = new Cluster({
            label: 'somelabel',
            point_src: "5856c61e072fdd4b30215f04",
            color: '#fffsometouhasdf'
        });

        try {
            await cluster.validate();
            await badcluster.validate();
        } catch (e) {
            assert(cluster.errors == undefined, 'First cluster shuold have no errors');
            assert(badcluster.errors != undefined, 'Second cluster should have errors');
        }
    });

});
