// We're in a testing environment
// This should be set before requiring the server
process.env.NODE_ENV = 'test';
// process.env.LOG_LEVEL = 'error';

const chai = require('chai');
import { assert, expect, should } from 'chai';
let chaihttp = require('chai-http');

import { Doctor } from '../models/Models';
import server from '../server';

chai.use(chaihttp);

describe('/api/doctors', function() {

    before(function(done) {
        // create admin, and login
        done();
    });

    // Maybe we should add more doctors
    beforeEach(async function() {
        var doc = new Doctor({
            firstname: 'Test',
            lastname: 'McTesty',
            username: 'mctest',
            email: 'testy@example.com',
            password: 'password1'
        });

        try {
            await doc.save();
        } catch (e) {
            assert(false, 'Doctor could not be saved in the beforeEach');
        }
    });

    afterEach( async function() {
        await Doctor.collection.drop();
    });

    // TODO(xavier): I can't use the same email in any of the test cases below, as the one in the before each hook
    describe('Doctor Schema', async function() {
        it('should have defaults for the settings', async () => {

            const doctor = new Doctor({
                firstname: 'Another',
                lastname: 'Doctor',
                username: 'ADoc',
                email: 'test@example.com',
                password: 'passwd'
            });

            await doctor.save();

            console.log('Settings: ' + doctor.settings);
            assert(doctor.settings);
            expect(doctor.settings.notifications.processing_start).to.be.false;
            expect(doctor.settings.notifications.processing_end).to.be.false;
            expect(doctor.settings.notifications.ticket_resolution).to.be.false;

        });

        it('shouldnt allow for duplicate emails', async () => {
            const doctor = new Doctor({
                firstname: 'Another',
                lastname: 'Doctor',
                username: 'ADoc',
                email: 'test@example.com',
                password: 'passwd'
            });

            const doctor2 = new Doctor({
                firstname: 'Another',
                lastname: 'Doctor',
                username: 'ADoc',
                email: 'testy@example.com',
                password: 'passwd'
            });

            try {
                await doctor.save();
                await doctor2.save();
            } catch (e) {
                assert(e, 'We shouldnt be able to save 2 users with the same email');
            }
        });

        it('should set activated to false if password isnt given', async () => {
            const doctor = new Doctor({
                firstname: 'Another',
                lastname: 'Doctor',
                username: 'ADoc',
                email: 'test@example.com'
                // password: ''
            });

            try {
                await doctor.save();
                assert(doctor.activated === false);
            } catch (e) {
                assert(false, 'Something went wrong with the saving');
            }
        });

        it('should require a firstname', async () => {
            const doctor = new Doctor({
                lastname: 'Doctor',
                username: 'ADoc',
                email: 'test@example.com'
            });

            try {
                await doctor.save();
            } catch (e) {
                assert(e, 'The doctor should not have been saved successfully');
            }
        });

        it('should require a lastname', async () => {
            const doctor = new Doctor({
                firstname: 'Doctor',
                username: 'ADoc',
                email: 'test@example.com'
            });

            try {
                await doctor.save();
            } catch (e) {
                assert(e, 'The doctor should not have been saved successfully');
            }
        });

        it('should require a username', (done) => {
            const doctor = new Doctor({
                firstname: 'Doctor',
                lastname: 'Person',
                email: 'test@example.com'
            });

            doctor.save((err) => {
                assert(err);
                done();
            });
        });

        it('should require an email', (done) => {
            const doctor = new Doctor({
                firstname: 'Doctor',
                lastname: 'Person',
                username: 'uname'
            });

            doctor.save((err) => {
                assert(err);
                done();
            });
        });
    });

    describe('GET', function() {
        it('should only be accessible by admins?');
        it('should return a list of all doctors');
    });

    describe('POST', function() {
        it('should only be accessible by admins');

        xit('should return the id of the new doctor on POST', function(done) {
            chai.request(server)
                .post('/api/doctors')
                .send({
                    'firstname': 'joe',
                    'lastname': 'shmoe',
                    'username': 'joeshmoe',
                    'email': 'joe@shmoe.com'
                })
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a('object');
                    done();
                });
        });

        it('should reject the addition of a malformed doctor, eg. no password, no first name, etc.');
    });

    // () => {} is just a placeholder for now until the actual tests are added.
    describe('GET /:id', () => {
        it('should return information in a single doctor, and should not include the doctors password');
    });

    describe('PUT /:id', () => {
        it('should only be accessible by that doctor, and an admin?');
    });

    describe('DELETE /:id', () => {
        it('should only be accessible by that doctor, and an admin?');
        it('should return the id of a deleted doctor on DELETE');
    });
});
