process.env.NODE_ENV = 'test';
process.env.LOG_LEVEL = 'error';

import * as chai from 'chai';
import { assert, expect, should } from 'chai';
let chaihttp = require('chai-http');
chai.use(chaihttp);

import { Admin } from '../models/Models';
import server from '../server';

describe('The Admin Model', function() {

    it('should have defaults for settings', async () => {
        let admin = new Admin({
            firstname: 'admin',
            lastname: 'ladmin',
            username: 'adminadmin',
            email: 'admin@example.com',
            password: '0000'
        });

        assert(admin.settings != undefined);
        expect(admin.settings.notifications.ticket_submission).to.be.false;
    });
});


describe('The Admin Routes', function() {

});
