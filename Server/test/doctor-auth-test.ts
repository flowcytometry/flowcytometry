// We're in a testing environment
// This should be set before requiring the server
process.env.NODE_ENV = 'test';
process.env.LOG_LEVEL = 'error';

const chai = require('chai');
import server from '../server';
const URI = '/api/doctors';

describe('/api/doctors, when not logged in', function() {
    it('should return 401 on GET', function(done) {
        chai.request(server)
            .get(URI)
            .end(function(err, res) {
                res.should.have.status(401);
                done();
            });
    });

    it('should return 401 on POST', function(done) {
        chai.request(server)
            .post(URI)
            .send({})
            .end(function(err, res) {
                res.should.have.status(401);
                done();
            });
    });
});

describe('/api/doctors/:id, when not logged in', function() {
    it('should return 401 on GET', function(done) {
        chai.request(server)
            .get(`${URI}/random`)
            .end(function(err, res) {
                res.should.have.status(401);
                done();
            });
    });

    it('should return 401 on PUT', function(done) {
        chai.request(server)
            .put(`${URI}/random`)
            .send({})
            .end(function(err, res) {
                res.should.have.status(401);
                done();
            });
    });

    it('should return 401 on DELETE', function(done) {
        chai.request(server)
            .delete(`${URI}/random`)
            .end(function(err, res) {
                res.should.have.status(401);
                done();
            });
    });
});