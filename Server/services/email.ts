import * as amqp from 'amqplib';
import * as nodemailer from 'nodemailer';
import * as moment from 'moment';
import * as mustache from 'mustache';
import * as fs from 'fs';

import { ConsumeCompletedJobs, ConsumeErrors, ConsumeStarts } from '../api_utils/setup_rabbitmq';
import { PrefixLogger } from '../util/logger';
import { Doctor, Patient } from '../models/Models';
const logger = PrefixLogger('Email');


const email_service  = process.env.EMAIL_SERVICE  || 'gmail';
const email_user     = process.env.EMAIL_USER
const email_password = process.env.EMAIL_PASSWORD

// TODO(xavier): Eventually set up OAuth2 for whatever email we're using: https://nodemailer.com/usage/using-gmail/
// TODO(xavier): Also make sure the email is sent through TLS.
const emailTransporter = nodemailer.createTransport({
    service: email_service,
    auth: {
        user: email_user,
        pass: email_password
    }
});

// TODO(xavier): If we want to send text messages, we need to ask for their phone number along with their carrier.
async function CreateMessage(msg: CompletionMessage) {
    let doc_id = msg['doctor'];
    let patient_id = msg['patient'];
    let processed_id = msg['id'];
    let time = msg['time'];

    try {
        let patient = await Patient.findById(patient_id);
        let doctor = await Doctor.findById(doc_id);
        let email = doctor.email;

        let name = `${doctor.firstname} ${doctor.lastname}`;
        let patient_name = `${patient.firstname} ${patient.lastname}`
        let templ = {
            doctor: name,
            patient: patient_name,
            start_date: moment.unix(msg.process_start),
            duration: moment.duration(msg.processing_time).humanize(),
            mailer_name: 'DrFCS',
            num_points: msg.num_points
            // url: 'www.drfcs.io'
        }

        // let template = fs.readFileSync('./templates/completion_template.mst');
        let message = mustache.render(completionEmailTemplate, templ);

        let mailOptions = {
            from: '"DrFCS" <drfcs.noreply@gmail.com>',
            to: email,
            subject: 'Your processing job has completed!',
            text: message,
            // html: message.replace(/\n/g, '<br/>')
        };

        return mailOptions;
    } catch (e) {
        logger.error(`Could not find a doctor with this id: ${doc_id}`)
        logger.error(e.message);
        return undefined;
    }
}

export function SendEmail(email: string, subject: string, message: string) {
    let mailOptions = {
        from: '"DrFCS" <drfcs.noreply@gmail.com>',
        to: email,
        subject: subject,
        text: message
        // html: message.replace(/\n/g, '<br/>')
    };

    emailTransporter.sendMail(mailOptions, (err, info) => {
        if(err) {
            logger.error(err.message);
        } else {
            logger.info(`Message: ${info.messageId} sent ${info.response}`);
        }
    });

}

function SendCompletionEmails(rabbit_connection: amqp.Connection) {
    ConsumeCompletedJobs(rabbit_connection, async (msg: CompletionMessage) => {
        logger.debug('received: ' + JSON.stringify(msg));

        let doc_id = msg['doctor'];
        let doctor = await Doctor.findById(doc_id);

        if(!doctor.settings.notifications.processing_end) {
            logger.info(`Doctor: ${doc_id} ${doctor.firstname} did not want an email sent, so they will not be getting one...`);
            return;
        } else {
            let mailOpt = await CreateMessage(msg);
            if(mailOpt === undefined) {
                logger.error('Could not send email');
            } 
            else {
                // TODO(xavier): Should I return something here to decide whether or not to ack the incomming message?
                // Maybe if something goes wrong, we just don't ack the message, and try again next time. Or else,
                // we could just have the StartConsumption function just ack all of the messages that come in...
                emailTransporter.sendMail(mailOpt, (err, info) => {
                    if(err) {
                        logger.error(err.message);
                    } else {
                        logger.info(`Message: ${info.messageId} sent ${info.response}`);
                    }
                });
            }
        }
    });
}

function SendProcessStartEmails(rabbit_connection: amqp.Connection) {
    ConsumeStarts(rabbit_connection, async (msg: StartMessage) => {
        logger.debug('received: ' + JSON.stringify(msg));
        let doc_id = msg['doctor'];
        let patient_id = msg['patient'];

        try {
            let doctor = await Doctor.findById(doc_id);
            let patient = await Patient.findById(patient_id);

            let templ = {
                doctor: `${doctor.firstname} ${doctor.lastname}`,
                patient: `${patient.firstname} ${patient.lastname}`,
                mailer_name: 'DrFCS'
            };

            let message = mustache.render(processingStartEmail, templ);

            let mailOptions = {
                from: '"DrFCS" <drfcs.noreply@gmail.com>',
                to: doctor.email,
                subject: 'Your processing job has begun!',
                text: message,
                // html: message.replace(/\n/g, '<br/>')
            };

            emailTransporter.sendMail(mailOptions, (err, info) => {
                if(err) {
                    logger.error(err.message);
                } else {
                    logger.info(`Message: ${info.messageId} sent ${info.response}`)
                }
            })
        } catch (err) {
            logger.error('could not send a start email');
        }

    });
}

function SendErrorEmails(rabbit_connection: amqp.Connection) {
    ConsumeErrors(rabbit_connection, (msg: CompletionMessage) => {
        logger.debug('received: ' + JSON.stringify(msg));
        let message = `Something went terribly wrong!

        Error: ${msg['error']}
        Message Data: ${msg['request_data']}`

        let mailOpt = {
            from: '"Processing Errors" <perrors@example.com>',
            to: 'errors@example.com',
            subject: 'Processing something went wrong',
            text: message,
            // html: message.replace(/\n/g, '<br/>')
        }

        emailTransporter.sendMail(mailOpt, (err, info) => {
            if(err) {
                logger.error(err.message);
            } else {
                logger.info(`Message: ${info.messageId} sent ${info.response}`)
            }
        })
    })
}

export function StartMailService(rabbit_connection: amqp.Connection) {
    SendProcessStartEmails(rabbit_connection)
    SendCompletionEmails(rabbit_connection);
    SendErrorEmails(rabbit_connection);
    logger.debug('Started Email Service');
}

interface StartMessage {
    doctor: string;
    patient: string;
}

interface CompletionMessage {
    doctor: string;
    patient: string;
    processed_file: string;
    processing_time: number;
    process_start: number;
    process_end: number;
    num_points: number;
    status: number;
}

const completionEmailTemplate = `Hello {{ doctor }},

Your processing job for {{patient}} started on {{ start_date }} has completed. It took {{ duration }}, and there were {{ num_points }} points generated!

{{#url}}

Navigate to {{ url }} to view it.
{{/url}}
Have a great day!

{{#mailer_name}}
Regards,
{{ mailer_name }}
{{/mailer_name}}
`

const processingStartEmail = `Hello {{ doctor }},

Your processing job for {{patient}} has started. We will notify you of its completion when necessary.

Have a great day!

{{#mailer_name}}
Regards,
{{ mailer_name }}
{{/mailer_name}}
`

const patientTransferEmail = `Hello {{ doctor }},

This is a confirmation email informing you that you have been assigned the following patient:

{{patient}}

Have a great day!

{{#mailer_name}}
Regards,
{{ mailer_name }}
{{/mailer_name}}
`