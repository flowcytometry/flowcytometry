(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
! function(e) {
	function r(o) {
		if (t[o]) return t[o].exports;
		var i = t[o] = {
			exports: {},
			id: o,
			loaded: !1
		};
		return e[o].call(i.exports, i, i.exports, r), i.loaded = !0, i.exports
	}
	var t = {};
	return r.m = e, r.c = t, r.p = "", r(0)
}([function(e, r) {
	if ("undefined" == typeof AFRAME) throw new Error("Component attempted to register before AFRAME was available.");
	AFRAME.registerComponent("gridhelper", {
		schema: {
			size: {
				default: 2.75
			},
			divisions: {
				default: 10
			},
			colorCenterLine: {
				default: "white"
			},
			colorGrid: {
				default: "white"
			}
		},
		init: function() {
			var e = this.el.object3D,
				r = this.data,
				t = r.size,
				o = r.divisions,
				i = r.colorCenterLine,
				n = r.colorGrid,
				d = new THREE.GridHelper(t, o, i, n);


			d.name = "gridHelper", e.add(d)
		},
		remove: function() {
			var e = this.el.object3D;
			e.remove(e.getObjectByName("gridHelper"))
		}
	})
}]);

},{}],2:[function(require,module,exports){
const defaultColor = "#90a4ae";

function render(point, color = defaultColor) {
    let entity = document.createElement("a-entity");
    entity.setAttribute("class", "point");
    entity.setAttribute("geometry", "primitive: circle; radius: 0.025");
    entity.setAttribute('look-at', '#orbital-cam');
    entity.setAttribute("position", point.x + " " + point.y + " " + point.z);
    entity.setAttribute("material", "color", color);
    scene.appendChild(entity);
};

function inBox(point, box) {
    var scale_var = box.scale;
    var position_var = box.position;

    var pos_x_check = position_var.x + (scale_var.x / 2);
    var neg_x_check = position_var.x - (scale_var.x / 2);

    var pos_y_check = position_var.y + (scale_var.y / 2);
    var neg_y_check = position_var.y - (scale_var.y / 2);

    var pos_z_check = position_var.z + (scale_var.z / 2);
    var neg_z_check = position_var.z - (scale_var.z / 2);

    return point.x <= pos_x_check && point.x >= neg_x_check &&
            point.y <= pos_y_check && point.y >= neg_y_check &&
            point.z <= pos_z_check && point.z >= neg_z_check;
}

function inSphere(point, sphere) {
    const x_displacement = point.x - sphere.position.x;
    const y_displacement = point.y - sphere.position.y;
    const z_displacement = point.z - sphere.position.z;

    const radius = sphere.scale.x;
    const distance = Math.sqrt((x_displacement*x_displacement)+(y_displacement*y_displacement)+(z_displacement*z_displacement));

    return distance <= radius;
}

function getColor(point, shapes) {
    for(var i = 0; i < shapes.length; i++) {
        var shape = shapes[i];
        if( shape.shape_type === "box" && inBox(point, shape) ) {
            return shape.color;
        } else if( shape.shape_type === "sphere" && inSphere(point, shape) ) {
            return shape.color;
        }
    }

    return defaultColor;
}

function distance(point) {
    return Math.sqrt((point.x * point.x)+(point.y*point.y)+(point.z*point.z));
}

module.exports = (points, clusterSet) => { 
    console.log(clusterSet);   
    // points.forEach((point, index) => {
    //     console.log(index, point);
    //     // if( point.x < 0 || point.y < 0 || point.z < 0) {
    //     //     console.log(point);
    //     // }
            
    // });
    const minDist = points.reduce((acc, val) => {
        return {
            x: Math.min(acc.x, val.x),
            y: Math.min(acc.y, val.y),
            z: Math.min(acc.z, val.z)
        };
    }, {x:Infinity, y:Infinity, z:Infinity});

    const maxDist = points.reduce((acc, val) => {
        return {
            x: Math.max(acc.x, val.x),
            y: Math.max(acc.y, val.y),
            z: Math.max(acc.z, val.z)
        };
    }, {x:0, y:0, z:0});

    var closestPoint = points.reduce((acc, val) => {
        // console.log(acc);
        return distance(val) < distance(acc) ? Object.assign({}, val) : Object.assign({}, acc);
    }, {x:Number.MAX_VALUE, y:Number.MAX_VALUE, z:Number.MAX_VALUE});

    const scale = {x: maxDist.x - minDist.x, y: maxDist.y - minDist.y, z: maxDist.z - minDist.z};

    // console.log("percentage scaling: ", minDist, maxDist, scale);
    console.log("clostest Point", closestPoint);
    
    let scene = document.getElementById("scene");
    points.forEach((point, i) => {
        point.x = 5 * (point.x - minDist.x)/scale.x;
        point.y = 5 * (point.y - minDist.y)/scale.y;
        point.z = 5 * (point.z - minDist.z)/scale.z;
        // console.log("% based", point);
        // point.x = (point.x - closestPoint.x);
        // point.y = (point.y - closestPoint.y);
        // point.z = (point.z - closestPoint.z);
        // console.log("clostest Piont based", point);

        var color = defaultColor;
        if(clusterSet && clusterSet.shapes) {
            var shapes = clusterSet.shapes;
            shapes.reverse();
            color = getColor(point, shapes);
        }
        render(point, color);
    });
};

// window.addEventListener('load', () => {
//     var fcsState = localStorage["reduxPersist:fcs"];
//     fcsState = JSON.parse(fcsState);
//     console.log(fcsState);
//     addPoints(fcsState.points, fcsState.selectedClusterSet);
// });
},{}],3:[function(require,module,exports){
require('./aframe-gridhelper-component.min');
var addPoints = require('./loadPoints');

window.addEventListener('load', () => {
    var fcsState = localStorage["reduxPersist:fcs"];
    fcsState = JSON.parse(fcsState);
    console.log(fcsState);
    addPoints(fcsState.points, fcsState.selectedClusterSet);
});
},{"./aframe-gridhelper-component.min":1,"./loadPoints":2}]},{},[3]);
