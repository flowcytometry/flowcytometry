const defaultColor = "#90a4ae";

function render(point, color = defaultColor) {
    let entity = document.createElement("a-entity");
    entity.setAttribute("class", "point");
    entity.setAttribute("geometry", "primitive: circle; radius: 0.025");
    entity.setAttribute('look-at', '#orbital-cam');
    entity.setAttribute("position", point.x + " " + point.y + " " + point.z);
    entity.setAttribute("material", "color", color);
    scene.appendChild(entity);
};

function inBox(point, box) {
    var scale_var = box.scale;
    var position_var = box.position;

    var pos_x_check = position_var.x + (scale_var.x / 2);
    var neg_x_check = position_var.x - (scale_var.x / 2);

    var pos_y_check = position_var.y + (scale_var.y / 2);
    var neg_y_check = position_var.y - (scale_var.y / 2);

    var pos_z_check = position_var.z + (scale_var.z / 2);
    var neg_z_check = position_var.z - (scale_var.z / 2);

    return point.x <= pos_x_check && point.x >= neg_x_check &&
            point.y <= pos_y_check && point.y >= neg_y_check &&
            point.z <= pos_z_check && point.z >= neg_z_check;
}

function inSphere(point, sphere) {
    const x_displacement = point.x - sphere.position.x;
    const y_displacement = point.y - sphere.position.y;
    const z_displacement = point.z - sphere.position.z;

    const radius = sphere.scale.x;
    const distance = Math.sqrt((x_displacement*x_displacement)+(y_displacement*y_displacement)+(z_displacement*z_displacement));

    return distance <= radius;
}

function getColor(point, shapes) {
    for(var i = 0; i < shapes.length; i++) {
        var shape = shapes[i];
        if( shape.shape_type === "box" && inBox(point, shape) ) {
            return shape.color;
        } else if( shape.shape_type === "sphere" && inSphere(point, shape) ) {
            return shape.color;
        }
    }

    return defaultColor;
}

function distance(point, target) {
    return Math.sqrt((
        (point.x - target.x) * (point.x - target.x)) +
        (point.y - target.y) * (point.y - target.y) +
        (point.z - target.z) * (point.z - target.z)
    );
}

function getColorKMeans(point, shapes) {
    const closestCentroid = shapes.reduce((acc, shape) => {
        return distance(point, acc.center_point) < distance(point, shape.center_point) ? acc : shape;
    });

    return closestCentroid.color;
}

const addPoints = (points, clusterSet) => { 

    const minDist = points.reduce((acc, val) => {
        return {
            x: Math.min(acc.x, val.x),
            y: Math.min(acc.y, val.y),
            z: Math.min(acc.z, val.z)
        };
    }, {x:Infinity, y:Infinity, z:Infinity});

    const maxDist = points.reduce((acc, val) => {
        return {
            x: Math.max(acc.x, val.x),
            y: Math.max(acc.y, val.y),
            z: Math.max(acc.z, val.z)
        };
    }, {x:0, y:0, z:0});

    const scale = {x: maxDist.x - minDist.x, y: maxDist.y - minDist.y, z: maxDist.z - minDist.z};

    // console.log("percentage scaling: ", minDist, maxDist, scale);
    
    let scene = document.getElementById("scene");
    points.forEach((point, i) => {
        point.x = 5 * (point.x - minDist.x)/scale.x;
        point.y = 5 * (point.y - minDist.y)/scale.y;
        point.z = 5 * (point.z - minDist.z)/scale.z;

        var color = defaultColor;
        if(clusterSet && clusterSet.shapes) {
            var shapes = clusterSet.shapes;
            shapes.reverse();
            
            if ( clusterSet.name === "kmeans" ) {
                color = getColorKMeans(point, shapes);
            } else {
                color = getColor(point, shapes);
            }
        }
        render(point, color);
    });
};

window.addEventListener('load', () => {
    var fcsState = localStorage["reduxPersist:fcs"];
    fcsState = JSON.parse(fcsState);
    console.log(fcsState);
    addPoints(fcsState.points, fcsState.selectedClusterSet);
});