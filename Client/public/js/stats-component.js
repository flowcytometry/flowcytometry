var fcsState = localStorage["reduxPersist:fcs"];
fcsState = JSON.parse(fcsState);

var CS = null;
var height = -.1;

AFRAME.registerComponent('graph-stats', {
    schema: {
        points: {
            default: fcsState.points.length
        },
        clusters: {
            default: 0
        },
        pointsInCluster: {
            default: 0
        },
        action: {
            default: 0
        }
    },
    init: function() {
        var e = this.el.object3D,
            r = this.data;

        if (fcsState.selectedClusterSet && fcsState.selectedClusterSet.shapes != null)
            this.data.clusters = fcsState.selectedClusterSet.shapes.length;
        else
            this.data.clusters = 0;
        this.new_clusters = 0;
        this.num_points = 0;
        this.initial_text = 'Total Points: ' + this.data.points + '\n' + 'Initial Clusters: ' + this.data.clusters + '\n';
        this.new_clusters_text = 'Created Clusters: ' + this.new_clusters + '\n';
        this.updated_text = "";
        console.log(fcsState);

        this.el.setAttribute('text', 'value', this.initial_text + this.new_clusters_text);
    },
    update: function(oldData) {
        console.log(this.data, oldData);
        // oldData.num_points = this.data.num_points;
        // this.data = oldData;
        // this.data = Object.assign({}, oldData, { num_points: this.data.num_points });
        // console.log(this.data, oldData);
        // this.el.attributes.text.value = "value: Points: " + points + '\n';
        // this.el.object3D.visible = false;


        if (CS !== null && CS.clusters && CS.clusters.length > 0 && this.data.action > oldData.action) {
            this.updated_text = "new cluster " + this.new_clusters + ": " + this.num_points + " points" + '\n';
            var el = document.createElement("a-entity");
            el.setAttribute('id', 'cluster' + this.new_clusters);
            el.setAttribute('position', "0 " + (height - (this.new_clusters) * .05) + " 0");
            el.setAttribute('text', 'value', this.updated_text);
            el.setAttribute('text', 'color', CS.clusters[CS.clusters.length - 1].color);
            this.el.appendChild(el);
            this.new_clusters++;
        }

        this.new_clusters_text = 'Created Clusters: ' + this.new_clusters + '\n';
        this.el.removeAttribute('text');
        this.el.setAttribute('text', 'value', this.initial_text + this.new_clusters_text);
    }
});