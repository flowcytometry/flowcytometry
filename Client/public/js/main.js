require('./aframe-gridhelper-component.min');
var addPoints = require('./loadPoints');

window.addEventListener('load', () => {
    var fcsState = localStorage["reduxPersist:fcs"];
    fcsState = JSON.parse(fcsState);
    console.log(fcsState);
    addPoints(fcsState.points, fcsState.selectedClusterSet);
});