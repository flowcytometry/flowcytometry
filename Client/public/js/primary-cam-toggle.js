"use strict";

// Grab Scene Reference
const scene = document.querySelector("#scene");
// ================== Graph Vars ================== //
var graphcontrol = document.querySelector('#graph');

var apiRoot = localStorage["reduxPersist:api"];
apiRoot = JSON.parse(apiRoot);

// ================== camera vars ================== //
var vr_controller = document.querySelector("#vr_controller");
vr_controller.setAttribute("gamepad_controls");
vr_controller.setAttribute("gamepad_controls", "enabled", "false");
vr_controller.setAttribute("gamepad_controls", "movementEnabled", "false");
vr_controller.setAttribute("gamepad_controls", "lookEnabled", "false");
vr_controller.setAttribute("gamepad_controls", "flyEnabled", "false");

var nvr_controller = document.querySelector('#stnd_controller');
nvr_controller.setAttribute("keyboard-controls");
nvr_controller.setAttribute("keyboard-controls", "enabled", "false");

var camcontrol = document.querySelector('#camcontrol');
var camera1El = document.querySelector("#orbital-cam");
var camera2El = document.querySelector("#standard-cam");

// ================== tool vars ================== //
var label1El = document.querySelector("#kb-help");
var label2El = document.querySelector("#gp-help");
var std_help_label = document.querySelector("#oc_help_label");
// var vr_help_label = document.querySelector("#vr_help_label");
var tool = document.querySelector('#tool');
tool.removeAttribute('gamepad-controls');

var pts = document.getElementsByClassName("point");
var save_message = document.querySelector('#ob-save');
var vr_save_message = document.querySelector('#vr-save');
var scale_on = document.querySelector('#scale-on');
var scale_off = document.querySelector('#scale-off');

var old_position;


var toolindex = 0; // tool cycle index
var scale = false,
    sphere_scale_tool = {x: 1, y: 1, z: 1},
    box_scale_tool = {x: 1, y: 1, z: 1};

    console.log(tool.getAttribute('scale'), box_scale_tool);
var def_box = tool.getAttribute('scale');
var help_toggle = false; // menu position
var save_toggle = false;

// ================== misc vars ================== //
var cam_toggle = true; // start true -> orbit cam start
var pscam_isActive = false; // point-select flag
var modal_isActive = false; // menu flag
var tool_isActive = false; // tool flag
var cp;
var colors = ["#ff1a75", "#ffff00", "#0066ff", "#9933ff", "#00ff00", "#ff8533", "#ff0000"];
var clr_selected = 0;
var statText = document.querySelector('#oc_graph_stats');

add_standard_controls();

// ================== cluster data ================== //
function ClusterSet() {
    this.date_created = Date();
    this.name = "Cluster: " + "" + Date();
    this.clusters = [];
    this.addCluster = function(Cluster) {
        this.clusters.push(Cluster);
    };
}

function Cluster(st, pos, dim, clr) {
    this.shape_type = st;
    this.position = pos;
    this.scale = dim;
    this.color = clr;
}

CS = new ClusterSet();



function add_vr_controls(){

    vr_controller.setAttribute('gamepad-controls', 'enabled', 'true');
    vr_controller.addEventListener('gamepadbuttondown:1', reset);

    nvr_controller.removeAttribute('keyboard-controls');

    tool.removeAttribute('keyboard-controls');
    console.log(tool);

    tool.setAttribute("gamepad-tool", "enabled", "true");
    tool.setAttribute("gamepad-tool", "movementEnabled", "true");
    tool.setAttribute("gamepad-tool", "lookEnabled", "false");
    tool.setAttribute("gamepad-tool", "flyEnabled", "false");


    // ================== TOOL CONTROLS ================== //
    tool.addEventListener('gamepadbuttondown:3', tool_toggle);
    tool.addEventListener('gamepadbuttondown:14', vr_x_scale_plus);
    tool.addEventListener('gamepadbuttondown:15', vr_x_scale_minus);
    tool.addEventListener('gamepadbuttondown:12', vr_y_scale_plus);
    tool.addEventListener('gamepadbuttondown:13', vr_y_scale_minus);
    tool.addEventListener('gamepadbuttondown:10', vr_z_scale_plus);
    tool.addEventListener('gamepadbuttondown:11', vr_z_scale_minus);

    // // ================== DEBUG ================== //
    // tool.addEventListener('gamepadbuttondown', function (e) {
    // console.log('Button "%d" has been pressed.', e.detail.index);
    // });

    // ================== SELECT CLUSTER ================== //

    tool.addEventListener('gamepadbuttondown:0', () => {

        if (save_toggle)
            return;

        if (help_toggle)
            toggle_help();


        switch (toolindex) {
            case 1:
                box_select();
                break;
            case 2:
                sphere_select();
                break;
            default:
        }
    });
    //        gamepad
    vr_controller.addEventListener('gamepadbuttonup:5', changeClstrClr);
    vr_controller.addEventListener('gamepadbuttonup:4', undoCluster);

    vr_controller.addEventListener('gamepadbuttonup:9', save);

    vr_controller.addEventListener('gamepadbuttonup:8', toggle_help);

    console.log(tool);

}

function add_standard_controls(){

    vr_controller.setAttribute('gamepad-controls', 'enabled', 'false');

    nvr_controller.setAttribute('keyboard-controls');
    nvr_controller.setAttribute('keyboard-controls', 'enabled', 'false');

    tool.setAttribute('gamepad-tool', 'enabled', 'false');
    tool.setAttribute('keyboard-controls');
    tool.setAttribute('keyboard-controls', 'enabled', 'false');

    // ================== BOX TOOL TOGGLE ================== //
    // keyboard
    tool.addEventListener('keyup:Space', tool_toggle);

    // ================== Additional TOOL Movement ================== //
    // up //
    tool.addEventListener('keydown:KeyQ', q_engine);
    // down //
    tool.addEventListener('keydown:KeyE', e_engine);

    // ================== SCALING ================== //
    //        keyboard
    tool.addEventListener('keyup:KeyA', a_engine);
    tool.addEventListener('keyup:KeyD', d_engine);
    tool.addEventListener('keyup:KeyZ', z_engine);
    tool.addEventListener('keyup:KeyC', c_engine);
    tool.addEventListener('keyup:Digit3', toggle_scaling);

    // ================== DATA LOG ================== //
    //        keyboard
    tool.addEventListener('keyup:Digit1', () => {

        if (save_toggle)
            return;

        if (help_toggle)
            toggle_help();


        switch (toolindex) {
            case 1:
                box_select();
                break;
            case 2:
                sphere_select();
                break;
            default:
        }
    });

    // ================== COLORS ================== //
    //        keyboard
    tool.addEventListener('keydown:Digit2', changeClstrClr);
    tool.addEventListener('keyup:Backspace', undoCluster);

    nvr_controller.addEventListener('keyup:Escape', reset);
    tool.addEventListener('keyup:Enter', save);

    nvr_controller.addEventListener('keyup:KeyH', toggle_help);
    nvr_controller.addEventListener('keydown:KeyW', help_cleaner);
    nvr_controller.addEventListener('keydown:KeyS', help_cleaner);

}

// ================== BEWARE ================== //

function vr_y_up() {

    cp = tool.getAttribute('position');
    cp.y += .1;

    tool.setAttribute('position', cp);

}

function vr_y_down() {
    cp = tool.getAttribute('position');
    cp.y -= .1;

    tool.setAttribute('position', cp);
}

function help_cleaner() {

    if (save_toggle)
        return;

    if (help_toggle) {
        help_toggle = false;
        label1El.setAttribute('visible', 'false');
    } else
        return;

}

function tool_toggle() {

    old_position = {x: 0, y: 0, z: 0};

    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    scale = false;
    var position = tool.getAttribute('position');
    if (toolindex == 0) {
        tool.setAttribute('geometry', 'primitive', 'box');
        tool.setAttribute('visible', true);
        tool.setAttribute('wasd-controls', 'enabled', true);

        toolindex++;
    } else if (toolindex == 1) {
        tool.setAttribute('scale', sphere_scale_tool);
        tool.setAttribute('geometry', 'primitive', 'sphere');
        toolindex++;
    } else {
        tool.setAttribute('wasd-controls', 'enabled', false);
        tool.setAttribute('visible', 'false');
        tool.setAttribute('scale', box_scale_tool);
        toolindex = 0;
    }
}

function vr_x_scale_plus() {

    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    switch (toolindex) {
        case 1:
            box_scale_tool.x += .5;
            tool.setAttribute('scale', box_scale_tool);
            break;
        case 2:
            sphere_scale_tool.x += .15;
            sphere_scale_tool.y += .15;
            sphere_scale_tool.z += .15;
            tool.setAttribute('scale', sphere_scale_tool);
            break;
        default:
    }
}

function vr_x_scale_minus() {

    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    switch (toolindex) {
        case 1:
            box_scale_tool.x -= .5;
            tool.setAttribute('scale', box_scale_tool);
            break;
        case 2:
            sphere_scale_tool.x -= .15;
            sphere_scale_tool.y -= .15;
            sphere_scale_tool.z -= .15;
            tool.setAttribute('scale', sphere_scale_tool);
            break;
        default:
    }
}

function vr_y_scale_plus() {
   

    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    

    switch (toolindex) {
        case 1:
         console.log("vr y plus: CASE 1");
            console.log(tool.getAttribute('scale'), box_scale_tool);
            box_scale_tool.y += .5;
            tool.setAttribute('scale', box_scale_tool);
            console.log(tool.getAttribute('scale'), box_scale_tool);
            break;
        case 2:
         
            sphere_scale_tool.x += .15;
            sphere_scale_tool.y += .15;
            sphere_scale_tool.z += .15;
            tool.setAttribute('scale', sphere_scale_tool);
            break;
        default:
            console.log("DEFAULTED");
    }
}

function vr_y_scale_minus() {

    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    switch (toolindex) {
        case 1:
            box_scale_tool.y -= .5;
            tool.setAttribute('scale', box_scale_tool);
            break;
        case 2:
            sphere_scale_tool.x -= .15;
            sphere_scale_tool.y -= .15;
            sphere_scale_tool.z -= .15;
            tool.setAttribute('scale', sphere_scale_tool);
            break;
        default:
    }
}

function vr_z_scale_plus() {

    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    switch (toolindex) {
        case 1:
            box_scale_tool.z += .5;
            tool.setAttribute('scale', box_scale_tool);
            break;
        case 2:
            sphere_scale_tool.x += .15;
            sphere_scale_tool.y += .15;
            sphere_scale_tool.z += .15;
            tool.setAttribute('scale', sphere_scale_tool);
            break;
        default:
    }
}

function vr_z_scale_minus() {

    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    switch (toolindex) {
        case 1:
            box_scale_tool.z -= .5;
            tool.setAttribute('scale', box_scale_tool);
            break;
        case 2:
            sphere_scale_tool.x -= .15;
            sphere_scale_tool.y -= .15;
            sphere_scale_tool.z -= .15;
            tool.setAttribute('scale', sphere_scale_tool);
            break;
        default:
    }
}

function q_engine() {

    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    console.log(box_scale_tool);

    if (!scale) {

        cp = tool.getAttribute('position');
        cp.y -= .1;

        tool.setAttribute('position', cp);

    } else {
        switch (toolindex) {
            case 1:

                box_scale_tool.x -= .5;
                tool.setAttribute('scale', box_scale_tool);
                break;
            case 2:
                sphere_scale_tool.x -= .15;
                sphere_scale_tool.y -= .15;
                sphere_scale_tool.z -= .15;
                tool.setAttribute('scale', sphere_scale_tool);
                break;
            default:
        }
    }
}

function e_engine() {

    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    if (!scale) {
        cp = tool.getAttribute('position');
        cp.y += .1;
        tool.setAttribute('position', cp);
    } else {
        switch (toolindex) {
            case 1:
                box_scale_tool.x += .5;
                tool.setAttribute('scale', box_scale_tool);
                break;
            case 2:
                sphere_scale_tool.x += .15;
                sphere_scale_tool.y += .15;
                sphere_scale_tool.z += .15;
                tool.setAttribute('scale', sphere_scale_tool);
                break;
            default:
        }
    }
}

function a_engine() {

    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    if (scale) {
        switch (toolindex) {
            case 1:
                box_scale_tool.y -= .5;
                tool.setAttribute('scale', box_scale_tool);
                break;
            case 2:
                break;
            default:
        }
    }
}

function d_engine() {

    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    if (scale) {
        switch (toolindex) {
            case 1:
                box_scale_tool.y += .5;
                tool.setAttribute('scale', box_scale_tool);
                break;
            case 2:
                break;
            default:
        }
    }
}

function z_engine() {

    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    if (scale) {
        switch (toolindex) {
            case 1:
                box_scale_tool.z -= .5;
                tool.setAttribute('scale', box_scale_tool);
                break;
            case 2:
                break;
            default:
        }
    }
}

function c_engine() {

    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    if (scale) {
        switch (toolindex) {
            case 1:
                box_scale_tool.z += .5;
                tool.setAttribute('scale', box_scale_tool);
                break;
            case 2:
                break;
            default:
        }
    }
}


function toggle_scaling() {
    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    if (!scale) {
        tool.setAttribute('wasd-controls', 'enabled', false);
        scale_on.emit('scale-on');
        //   spheretool.setAttribute('wasd-controls', 'enabled', false);
    } else {
        tool.setAttribute('wasd-controls', 'enabled', true);
        scale_off.emit('scale-off');
        //   spheretool.setAttribute('wasd-controls', 'enabled', true);
    }

    scale = !scale;
}

function box_select() {
    var position_var = tool.getAttribute('position');

    if(_.isEqual(old_position, position_var)){
        return;
    }else{
        old_position = position_var;
    }

    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    clr_selected = 0;
    var points_in_cluster = 0;
    var scale_var = box_scale_tool;
    var geometry_var = tool.getAttribute('geometry');
    var exists = false;

    var pos_x_check = position_var.x + (geometry_var.width * scale_var.x / 2);
    var neg_x_check = position_var.x - (geometry_var.width * scale_var.x / 2);

    var pos_y_check = position_var.y + (geometry_var.height * scale_var.y / 2);
    var neg_y_check = position_var.y - (geometry_var.height * scale_var.y / 2);

    var pos_z_check = position_var.z + (geometry_var.depth * scale_var.z / 2);
    var neg_z_check = position_var.z - (geometry_var.depth * scale_var.z / 2);

    // todo: save all check vars into array to save for cluster files

    for (var ctr = 0; ctr < pts.length; ctr++) {

        if (pts[ctr].components.position.data.x <= pos_x_check && pts[ctr].components.position.data.x >= neg_x_check &&
            pts[ctr].components.position.data.y <= pos_y_check && pts[ctr].components.position.data.y >= neg_y_check &&
            pts[ctr].components.position.data.z <= pos_z_check && pts[ctr].components.position.data.z >= neg_z_check) {
            exists = true;
            points_in_cluster++;
            pts[ctr].setAttribute('material', 'color', colors[clr_selected]);
        }
    }
    if (exists) {
        var cluster = new Cluster("box", position_var, scale_var, colors[clr_selected]);
        CS.addCluster(cluster);
        if(cam_toggle){
            // console.log(statText.components);
            // statText.setAttribute('graph-stats', 'num_points', points_in_cluster);
            statText.components["graph-stats"].num_points = points_in_cluster;
            var new_val = statText.components["graph-stats"].data.action;
            new_val++;
            console.log(new_val);
            statText.setAttribute('graph-stats', 'action', new_val);
            // statText.setAttribute('graph-stats', 'action', 'add');
            // statText.update(points_in_cluster);
        }
    }

    exists = false;
}

function sphere_select() {

    var position_var = tool.getAttribute('position');

    if(_.isEqual(old_position, position_var)){
        return;
    }else{
        old_position = position_var;
    }

    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    clr_selected = 0;

    var radius = tool.getAttribute('geometry');
    var position_var = tool.getAttribute('position');
    var scale_var = sphere_scale_tool;

    var exists = false;
    var points_in_cluster = 0;

    var distance;
    var x_component;
    var y_component;
    var z_component;

    for (var ctr = 0; ctr < pts.length; ctr++) {

        x_component = pts[ctr].components.position.data.x - position_var.x;
        y_component = pts[ctr].components.position.data.y - position_var.y;
        z_component = pts[ctr].components.position.data.z - position_var.z;
        distance = Math.sqrt(x_component * x_component + y_component * y_component + z_component * z_component);

        if (distance <= radius.radius * scale_var.x) {
            exists = true;
            points_in_cluster++;
            pts[ctr].setAttribute('material', 'color', colors[clr_selected]);
        }
    }

    if (exists) {
        var cluster = new Cluster("sphere", position_var, scale_var, colors[clr_selected]);
        CS.addCluster(cluster);
        // console.log(statText.components);
        if(cam_toggle){
            // statText.setAttribute('graph-stats', 'num_points', points_in_cluster);
            statText.components["graph-stats"].num_points = points_in_cluster;
            var new_val = statText.components["graph-stats"].data.action;
            new_val++;
            console.log(new_val);
            statText.setAttribute('graph-stats', 'action', new_val);
            // statText.setAttribute('graph-stats', 'action', 'add');
            // statText.update(points_in_cluster);
       }
    }

    exists = false;
}

function changeClstrClr() {

    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    if (clr_selected < colors.length - 1)
        clr_selected++;
    else
        clr_selected = 0;


    var length = CS.clusters.length;

    console.log(CS.clusters[length - 1].shape_type);

    if (CS.clusters.length > 0 && CS.clusters[length - 1].shape_type === "box") {

        var position_var = CS.clusters[length - 1].position;
        var scale_var = CS.clusters[length - 1].scale;

        var pos_x_check = position_var.x + (scale_var.x / 2);
        var neg_x_check = position_var.x - (scale_var.x / 2);

        var pos_y_check = position_var.y + (scale_var.y / 2);
        var neg_y_check = position_var.y - (scale_var.y / 2);

        var pos_z_check = position_var.z + (scale_var.z / 2);
        var neg_z_check = position_var.z - (scale_var.z / 2);

        for (var ctr = 0; ctr < pts.length; ctr++) {
            if (pts[ctr].components.position.data.x <= pos_x_check && pts[ctr].components.position.data.x >= neg_x_check &&
                pts[ctr].components.position.data.y <= pos_y_check && pts[ctr].components.position.data.y >= neg_y_check &&
                pts[ctr].components.position.data.z <= pos_z_check && pts[ctr].components.position.data.z >= neg_z_check) {
                pts[ctr].setAttribute('material', 'color', colors[clr_selected]);
            }
        }
        CS.clusters[CS.clusters.length - 1].color = colors[clr_selected];
    } else if (CS.clusters.length > 0 && CS.clusters[length - 1].shape_type === "sphere") {
        var radius = 1;
        var position_var = CS.clusters[length - 1].position;
        var sphere_scale_var = CS.clusters[length - 1].scale;
        console.log(sphere_scale_var);
        var distance;
        var x_component;
        var y_component;
        var z_component;

        for (var ctr = 0; ctr < pts.length; ctr++) {

            x_component = pts[ctr].components.position.data.x - position_var.x;
            y_component = pts[ctr].components.position.data.y - position_var.y;
            z_component = pts[ctr].components.position.data.z - position_var.z;
            distance = Math.sqrt((x_component * x_component) + (y_component * y_component) + (z_component * z_component));

            if (distance <= radius * sphere_scale_var.x) {
                pts[ctr].setAttribute('material', 'color', colors[clr_selected]);
            }
        }
        CS.clusters[CS.clusters.length - 1].color = colors[clr_selected];
    }
    
    if (CS.clusters.length > 0 ) {
        var cluster_id_string = "#cluster" + (CS.clusters.length - 1);
        var cluster_stat_label = document.querySelector(cluster_id_string);
        cluster_stat_label.setAttribute('text', 'color', colors[clr_selected]);
    }
}

function undoCluster() {

    old_position = {X: 0, y: 0, z: 0};

    if (CS.clusters.length == 0)
        return;

    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    var length = CS.clusters.length - 1;

    if (CS.clusters[length].shape_type == "box") {

        var position_var = CS.clusters[length].position;
        var scale_var = CS.clusters[length].scale;

        var pos_x_check = position_var.x + (scale_var.x / 2);
        var neg_x_check = position_var.x - (scale_var.x / 2);

        var pos_y_check = position_var.y + (scale_var.y / 2);
        var neg_y_check = position_var.y - (scale_var.y / 2);

        var pos_z_check = position_var.z + (scale_var.z / 2);
        var neg_z_check = position_var.z - (scale_var.z / 2);

        for (var ctr = 0; ctr < pts.length; ctr++) {
            if (pts[ctr].components.position.data.x <= pos_x_check && pts[ctr].components.position.data.x >= neg_x_check &&
                pts[ctr].components.position.data.y <= pos_y_check && pts[ctr].components.position.data.y >= neg_y_check &&
                pts[ctr].components.position.data.z <= pos_z_check && pts[ctr].components.position.data.z >= neg_z_check) {
                pts[ctr].setAttribute('material', 'color', "#90a4ae");
            }
        }
    } else if (CS.clusters[length].shape_type == "sphere") {
        var radius = 1;
        var position_var = CS.clusters[length].position;
        var scale_var = CS.clusters[length].scale;

        var distance;
        var x_component;
        var y_component;
        var z_component;

        for (var ctr = 0; ctr < pts.length; ctr++) {

            x_component = pts[ctr].components.position.data.x - position_var.x;
            y_component = pts[ctr].components.position.data.y - position_var.y;
            z_component = pts[ctr].components.position.data.z - position_var.z;
            distance = Math.sqrt(x_component * x_component + y_component * y_component + z_component * z_component);

            if (distance <= radius * scale_var.x) {
                pts[ctr].setAttribute('material', 'color', "#90a4ae");
            }
        }

    }

    if (CS.clusters.length > 0 && cam_toggle) {
        var cluster_id_string = "#cluster" + (CS.clusters.length-1);
        console.log(cluster_id_string, document.querySelector(cluster_id_string));
        
    
        while(document.querySelector(cluster_id_string) === null && CS.clusters.length !== 0){
            CS.clusters.pop();
            cluster_id_string = "#cluster" + (CS.clusters.length-1);
        }

         var cluster_stat_label = document.querySelector(cluster_id_string);
        
        console.log(cluster_stat_label);
        statText.removeChild(cluster_stat_label);
        statText.components["graph-stats"].new_clusters--;
        var new_val = statText.components["graph-stats"].data.action - 1;
        statText.setAttribute('graph-stats', 'action', new_val);
        console.log(statText, statText.components["graph-stats"], statText.components);
    }


    CS.clusters.pop();
}

function save() {

    if (help_toggle)
        toggle_help();

    if (CS.clusters.length) {
        const set = {
            name: CS.name,
            date_created: CS.date_created,
            shapes: CS.clusters
        };
        const metadataId = fcsState.selectedMetadata._id;

        axios.post(apiRoot + "metadata/" + metadataId + "/addcluster", set)
            .then(function(response) {
                console.log(response);
            })
            .catch(function(error) {
                console.log(error);
            });

        if (cam_toggle)
            save_message.emit('fade');
        else
            vr_save_message.emit('vr_fade');

    }
}

function reset() {
    if (save_toggle)
        return;

    if (help_toggle)
        toggle_help();

    while (CS.clusters.length > 0) {
        undoCluster();
    }
}

function toggle_help() {

    if (save_toggle)
        return;


    if (help_toggle) {
        if (cam_toggle)
            label1El.setAttribute('visible', 'false');
        else
            label2El.setAttribute('visible', 'false');

        help_toggle = !help_toggle;
    } else {
        if (cam_toggle)
            label1El.setAttribute('visible', 'true');
        else
            label2El.setAttribute('visible', 'true');

        help_toggle = !help_toggle;
    }

    console.log(help_toggle, cam_toggle);

}

// handle VR enter/exit events
scene.addEventListener('enter-vr', () => {
    console.log('enter vr');

    if (help_toggle)
        toggle_help();

    scale = false;

    statText.setAttribute('visible', 'false');
    std_help_label.setAttribute('visible', 'false');
    camera1El.setAttribute("camera", "active: false");
    camera2El.setAttribute("camera", "active: true");
    camera2El.setAttribute('look-controls', 'enabled', 'true');

    for (var i = 0; i < pts.length; i++) {
        pts[i].setAttribute('look-at', '#standard-cam');
    }

    camera2El.setAttribute('gamepad-controls', 'enabled', 'true');
    camera2El.setAttribute('gamepad-controls', 'movementEnabled', 'true');
    camera2El.setAttribute('gamepad-controls', 'flyEnabled', 'true');
    // camera2El.setAttribute('look-controls', 'enabled', 'true');
    cam_toggle = false;

    add_vr_controls();

    console.log("we made it");
});

scene.addEventListener('exit-vr', () => {
    console.log("exit-vr");

    if (help_toggle)
        toggle_help();

    save_toggle = false;
    // vr_help_label.setAttribute('visible', 'false');
    camera2El.setAttribute('gamepad-controls', 'enabled', 'false');
    camera2El.setAttribute('gamepad-controls', 'movementEnabled', 'false');
    camera2El.setAttribute('gamepad-controls', 'flyEnabled', 'false');
    camera2El.setAttribute('look-controls', 'enabled', 'false');

    camera2El.setAttribute("camera", "active: false");
    camera1El.setAttribute("camera", "active: true");
    std_help_label.setAttribute('visible', 'true');
    statText.setAttribute('visible', 'true');

    for (var i = 0; i < pts.length; i++) {
        pts[i].setAttribute('look-at', '#orbital-cam');
    }


    cam_toggle = true;

    add_standard_controls();
});
