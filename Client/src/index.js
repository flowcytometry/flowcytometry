import 'babel-polyfill'
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { configureStore } from './redux/configureStore';
import App from './App';
import {persistStore} from 'redux-persist'
import injectTapEventPlugin from 'react-tap-event-plugin';
import { browserHistory } from 'react-router'

// Needed for onTouchTap 
// http://stackoverflow.com/a/34015469/988941 
injectTapEventPlugin();

export const store = configureStore();
store.dispatch({
  type:"SET_API", 
  apiRoot: process.env.REACT_APP_API_URI || "localhost:3001/api/"
});
persistStore(store, {}, (err, result) => {
  console.log(err, result);
  const { user } = result;
  if (user && user.loggedIn) {
      const location = user.roles.includes('admin') ? "Users" : "Patients";
      browserHistory.push(location);
  }
});

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
