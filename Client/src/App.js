import React, { Component } from 'react';
import { Router, Route, IndexRoute, browserHistory} from 'react-router';
import { Nav } from './components/Nav';
import { LoginContainer } from './components/auth/Login';
import { UserList } from './components/users/UserList';
import { PatientList } from './components/patients/PatientList';
import Auth from './components/auth/Auth';
import { Graph } from './components/graph/pointplotlist';
import { FCSTestList } from './components/fcs/FCSTestList';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { ErrorMessage } from './components/ErrorMessage';
import NotFound from './components/NotFound';
import Forgot from './components/auth/Forgot';

const Container = (props) => (
    <div>
        <header>
            <Nav />
            <ErrorMessage/>
        </header>
        <main>
            {props.children}
        </main>
        <footer>
        </footer>
    </div>
)

class App extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <Router history={browserHistory}>
          <Route path='/' component={Container}>
            <IndexRoute component={LoginContainer}/>
            <Route path='Login' component={LoginContainer} />
            <Route path='Forgot' component={Forgot} />
            <Route component={Auth}>
              <Route path='Users' component={UserList} />
              <Route path='Patients' component={PatientList} />
              <Route path='FCSTests' component={FCSTestList} />
            </Route>
            <Route path="*" component={NotFound}/>
          </Route>
        </Router>
      </MuiThemeProvider>
    );
  }
}

export default App;
