import React from 'react';
import { connect } from 'react-redux';
import { deleteClusterSet } from '../../redux/actions';
import { Row, Col, Button, Icon } from 'react-materialize';
import Dialog from 'material-ui/Dialog';
import Popover, {PopoverAnimationVertical} from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';

const mapDispatchToProps = dispatch => {
    return {
        onDelete: (metadata, clusterSet) => () => dispatch(deleteClusterSet(metadata, clusterSet))
    };
}

const mapStateToProps = state => {
    return {
        clusterSet: state.fcs.selectedClusterSet
    };
}

class MoreOptionsMenu extends React.Component {
  state = {
    detailsOpen: false,
    editOpen: false,
  };

  handleDelete = () => {
    this.setState({detailsOpen: true});

  };

  handleClose = () => {
    this.setState({detailsOpen: false, editOpen: false});
  };

  render() {
    const {metadata, clusterSet, onDelete} = this.props;
    return (
        <IconMenu
            anchorOrigin={{horizontal: 'left', vertical: 'top'}}
            targetOrigin={{horizontal: 'left', vertical: 'top'}}
            iconButtonElement={
                <IconButton style={{padding:"0", width:"inherit", height:"inherit"}}>
                    <FontIcon 
                        className="material-icons"
                        color="grey"
                        hoverColor="black"
                        >
                        more_vert
                    </FontIcon>
                </IconButton>}>
            <MenuItem 
                primaryText="Remove Selected Cluster Set"
                leftIcon={
                    <FontIcon 
                    className="material-icons">
                        delete
                    </FontIcon>
                }
                onTouchTap={onDelete(metadata, clusterSet)}/>
        </IconMenu>
    );
  }
}

export default connect(null, mapDispatchToProps)(MoreOptionsMenu);