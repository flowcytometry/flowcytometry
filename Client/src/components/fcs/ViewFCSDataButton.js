import  React, {Component} from 'react';
import { connect } from 'react-redux';
import { Button } from 'react-materialize';
import { getFCSPoints } from '../../redux/actions';

const mapDispatchToProps = (dispatch, props) => {
    var { metadata, clusterSet} = props;
    return {
        viewPatientFCSData: () => {
            dispatch({type: "SELECT_CLUSTER_SET", clusterSet});
            dispatch({type:"SELECT_METADATA", metadata});
            dispatch(getFCSPoints(metadata.processed_file));
        }
    };
}

class ViewFCSDataButton extends Component {
    render() {
        const {disabled, metadata, viewPatientFCSData} = this.props;
        return (
            <Button disabled={disabled}
                waves='light' className="blue-grey" 
                onClick={() => viewPatientFCSData(metadata)}>
                   View
            </Button>
        )
    }
}

export default connect(null, mapDispatchToProps)(ViewFCSDataButton);