import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getFCSMetaData } from '../../redux/actions';
import { ListTitle } from '../common/ListTitle';
import { InfoCardList } from '../common/InfoCardList';
import FCSTableRow from './FCSTableRow';
import ViewFCSDataButton from './ViewFCSDataButton';
import AddFCSData from './AddFCSData';
import { Col } from 'react-materialize';
import Subheader from 'material-ui/Subheader';

import { Button, Input } from 'react-materialize';
import { ListOptions, ListOptionsBar } from '../common/ListOptions';
import { ConditionalElemnet } from '../common/ConditionalElement';
import { Loader } from '../common/Loader';
import sortBy from 'lodash/sortBy';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import { ReverseButton } from '../common/ReverseButton';
import TableRowColumn from 'material-ui/Table/TableRowColumn';
import TableHeaderColumn from 'material-ui/Table/TableHeaderColumn';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RefreshButton from '../common/RefreshButton';

const filterList = (metadataList, filter) => metadataList = metadataList.filter( metadata => {
    const uploadDate = metadata.upload_date;

    return uploadDate.startsWith(filter);
});

const mapStateToProps = state => {
    var {selectedPatient} = state.patients;
    var {metadataList, sort, filter, descendingOrder} = state.fcs;

    if ( filter !== '' ) {
        metadataList = filterList(metadataList, filter);
    }

    metadataList = sortBy(metadataList, [sort]);

    if( descendingOrder ) {
        metadataList.reverse();
    }

    return {
        patientId: selectedPatient._id,
        title: selectedPatient.firstname + "'s FCS Data",
        metadataList,
        sort,
        currentFilter: filter,
        sortOptions: [
            {value: 'upload_date', label: 'Upload Date'},
            {value: 'numPoints', label: '# Points'},
            {value: 'status', label: 'Processing Status'}
        ]
    };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getFCSMetaData: (patientId) => () => dispatch(getFCSMetaData(patientId)),
    reverse: () => dispatch({type: "TOGGLE_FCS_METADATA_ORDER"}),
    sortBy: field => dispatch({type: "SORT_METADATA", field}),
    filter: keyword => dispatch({type: "FILTER_METADATA", keyword}),
  };
}

const cardMap = fcsMetaData => ({
    key: fcsMetaData.id,
    title: 
        <div className="center-align">
            <Subheader style={{paddingLeft: "0px"}} >Date Uploaded</Subheader>
            <span className="flow-text" style={{fontWeight: '300', fontSize: '1.3rem'}}>
                {fcsMetaData.proc_start}
            </span>
        </div>,
    detail: 
        <div className="center-align">
            <Subheader style={{paddingLeft: "0px"}} >Number of Points</Subheader>
            <span className="flow-text" style={{fontWeight: '300', fontSize: '1.3rem'}}>
                {fcsMetaData.numPoints}
            </span>
        </div>,
    content: 
        <div className="center-align">
            <Subheader style={{paddingLeft: "0px"}} >Processing Status</Subheader>
            <span className="flow-text" style={{fontWeight: '300', fontSize: '1.3rem'}}>
                {fcsMetaData.status}
            </span>
        </div>,
    action: <div className="center-align"><ViewFCSDataButton id={fcsMetaData.id}/></div>
});

class FCSTestListContainer extends Component {
    componentWillMount() {
        this.props.getFCSMetaData(this.props.patientId)();
    } 

    render() {
        const { metadataList, reverse, sort, filter, currentFilter, sortOptions, sortBy, getFCSMetaData, patientId} = this.props;
        return (
            <div className="container">
                <ListTitle title={this.props.title}/>
                {/*<ListOptions 
                    sort={this.props.sort}
                    sortBy={this.props.sortBy}
                    currentFilter={this.props.currentFilter}
                    filterBy={this.props.filter}
                    addItemModal={<AddFCSData/>}
                    propList={[
                        {value: 'firstname', label: 'First Name'},
                        {value: 'lastname', label: 'Last Name'}
                    ]}
                /> */}
                <InfoCardList
                    rows={!metadataList ? [] :
                        metadataList.map(metadata => <FCSTableRow key={metadata._id} fcsMetaData={metadata}/>)
                    }
                    showCheckboxes={false}
                    headers={[
                        <TableHeaderColumn>Date Uploaded</TableHeaderColumn>,
                        <TableHeaderColumn>Processing Status</TableHeaderColumn>,
                        <TableHeaderColumn>Select Cluster Set</TableHeaderColumn>,
                        <TableHeaderColumn style={{width:12}}></TableHeaderColumn>,
                        <TableHeaderColumn><Loader style={{marginLeft: "40%"}} size={"small"}/></TableHeaderColumn>
                    ]}
                    cardItems={!metadataList ? [] : metadataList.map(
                        (metadataList, i) => cardMap(metadataList))}
                >
                    <ListOptionsBar 
                        firstGroup={[
                            <AddFCSData/>,
                            <RefreshButton refresh={getFCSMetaData(patientId)}/>
                        ]}
                        secondGroup={[<ReverseButton onClick={reverse}/>]}
                        thirdGroup={[
                            <IconButton
                                tooltip="Search" 
                                iconClassName="material-icons"
                                iconStyle={{color: "grey"}}>
                                search
                            </IconButton>,
                            <Input
                                s={12}
                                l={6}
                                label="Search"
                                value={currentFilter}
                                onChange={event => filter(event.target.value.toLowerCase())}/>,
                            <IconButton 
                                tooltip="Sort"
                                iconClassName="material-icons"
                                iconStyle={{color: "grey"}}>
                                sort
                            </IconButton>,...
                            sortOptions.map((prop, i) => <Input 
                                key={i}
                                name='group1'
                                type='radio'
                                className='with-gap'
                                value={prop.value}
                                checked={sort === prop.value}
                                label={prop.label}
                                onChange={() => sortBy(prop.value)}/>
                            )
                        ]} />
                </InfoCardList>
            </div>
        );
    }
}

export const FCSTestList = connect(mapStateToProps, mapDispatchToProps)(FCSTestListContainer);