import Moment from 'moment';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getFCSMetaData } from '../../redux/actions';
import { ListTitle } from '../common/ListTitle';
import { InfoCardList } from '../common/InfoCardList';
import ViewFCSDataButton from './ViewFCSDataButton';
import AddFCSData from './AddFCSData';
import { Col } from 'react-materialize';
import Subheader from 'material-ui/Subheader';
import MoreOptionsMenu from './MoreOptionsMenu';
import { Button, Input } from 'react-materialize';
import { ListOptions, ListOptionsBar } from '../common/ListOptions';
import { ConditionalElemnet } from '../common/ConditionalElement';
import { Loader } from '../common/Loader';
import sortBy from 'lodash/sortBy';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import { ReverseButton } from '../common/ReverseButton';
import TableRowColumn from 'material-ui/Table/TableRowColumn';
import TableHeaderColumn from 'material-ui/Table/TableHeaderColumn';
import TableRow from 'material-ui/Table/TableRow';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

const mapStateToProps = state => {
    var {selectedPatient} = state.patients;
    var {metadataList, sort, filter, descendingOrder} = state.fcs;

    return {
        patientId: selectedPatient._id,
        selectedClusterSet: state.fcs.selectedClusterSet
    };
}

const mapDispatchToProps = (dispatch) => {
  return {
    selectClusterSet: clusterSet => dispatch({type: "SELECT_CLUSTER_SET", clusterSet})
  };
}

class FCSTableRow extends Component {
    state = {
        value:null,
        clusterSets:[],
        items: ["herp","derp"]
    };

    handleChange = (event, index, value) => {
        this.setState({value});
        this.props.selectClusterSet(value);
    }

    render() {
        const { fcsMetaData, selectedClusterSet } = this.props;
        var statusText = "";

        var items = fcsMetaData.clusters.map(
            clusterSet => <MenuItem 
                key={clusterSet._id} 
                value={clusterSet} 
                primaryText={Moment(clusterSet.date_created).local().format("MM/DD/YYYY HH:mm") + (clusterSet.name === "kmeans" ? "(generated)" : "")}/>
        );
        items.unshift(<MenuItem value={null} primaryText="Default" />);

        if(fcsMetaData.status === 0){
            statusText=<span 
                className="flow-text grey-text" 
                style={{fontWeight: '300', fontSize: '1.3rem'}}>
                UNPROCESSED
            </span>
        } else if (fcsMetaData.status === 1) {
            statusText=<span 
                className="flow-text cyan-text text-darken-2" 
                style={{fontWeight: '300', fontSize: '1.3rem'}}>
                IN PROGRESS...
            </span>
        } else {
            statusText=<span 
                className="flow-text light-green-text text-darken-3" 
                style={{fontWeight: '300', fontSize: '1.3rem'}}>
                COMPLETED
            </span>
        }

        return (
            <TableRow hoverable={false}>
                <TableRowColumn>
                    <span className="flow-text" style={{fontWeight: '300', fontSize: '1.3rem'}}>
                        {Moment(fcsMetaData.upload_date).local().format("MM/DD/YYYY HH:mm")}
                    </span>
                </TableRowColumn>
                <TableRowColumn>
                        {statusText}
                </TableRowColumn>
                <TableRowColumn>
                        <SelectField
                            value={this.state.value}
                            onChange={this.handleChange}
                            maxHeight={200}
                        >
                            {items}
                        </SelectField>
                </TableRowColumn>
                <TableRowColumn style={{width:12}}>
                    <div onClick={e => {
                            e.preventDefault();
                            e.stopPropagation();
                        }}>
                        <MoreOptionsMenu metadata={fcsMetaData} clusterSet={selectedClusterSet}/>
                    </div>
                </TableRowColumn>
                <TableRowColumn>
                    <div className="center-align">
                        <ViewFCSDataButton 
                            metadata={fcsMetaData}
                            clusterSet={this.state.value}
                            disabled={fcsMetaData.status === 2 ? false : true}/>
                    </div>
                </TableRowColumn>
            </TableRow>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FCSTableRow);