import  React from 'react';
import { connect } from 'react-redux';
import { Row, Icon, Button, Modal } from 'react-materialize';
import { addFCSFile } from '../../redux/actions';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';

const mapStateToProps = state => {
    const { error } = state;
    const { _id, id } = state.patients.selectedPatient;
    const patientId = _id || id;

    return {
        errorMessage: error.message || null,
        patientId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSubmit: (files, patientId) => {
            dispatch(addFCSFile(files[0], patientId));
        }
    };
};

export const AddFCSData = ({onSubmit, errorMessage, patientId}) => {
    let files;
    return (
        <Modal
            header='Upload FCS Data'
            actions={[
                <Button waves='red' modal="close" className="btn-flat">Cancel</Button>, 
                <Button waves='light' modal='close' className="teal darken-4" type="submit" onClick={e => onSubmit(files,patientId)}>
                    Submit<Icon className="right">send</Icon>
                </Button>
            ]}
            trigger={
                <IconButton
                    tooltip={"Add"}>
                    <FontIcon 
                        className="material-icons"
                        color="grey"
                        hoverColor="green">
                        add_circle
                    </FontIcon>
                </IconButton>
            }>
            <Row>
                <form action="#">
                    <div className="file-field input-field">
                        <div className="btn blue-grey">
                            <span>File</span>
                            <input type="file" multiple onChange={e => files=e.target.files}/>
                        </div>
                        <div className="file-path-wrapper">
                            <input className="file-path validate" type="text" placeholder="Upload one or more files"/>
                        </div>
                    </div>
                </form>
            </Row>
        </Modal>
    )
};

export default connect(mapStateToProps, mapDispatchToProps)(AddFCSData);