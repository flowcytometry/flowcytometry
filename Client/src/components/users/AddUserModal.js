import  React from 'react';
import { connect } from 'react-redux';
import { Row, Col, Input, Icon, Button, Modal } from 'react-materialize';
import { ErrorMessage } from '../ErrorMessage';
import { addUser } from '../../redux/actions';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';
import Toggle from 'material-ui/Toggle';
import InputPassword from 'react-ux-password-field';
import { parse, format, asYouType } from 'libphonenumber-js'

const mapDispatchToProps = dispatch => {
    return {
        onSubmit: (user, adminRole, docRole) => {
            user.roles = [];
            if ( adminRole ) 
                user.roles.push(adminRole);

            if (docRole) 
                user.roles.push(docRole);

            dispatch(addUser(user));
        }
    };
};

export const AddUser = ({onSubmit}) => {
    let user = {}, adminRole, docRole = true, passwordOne, passwordTwo;
    var passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/;
    return (
        <Modal fixedfooter
            header='Add User'
            modal={true}
            trigger={
                <IconButton
                    tooltip={"Add"}>
                    <FontIcon 
                        className="material-icons"
                        color="grey"
                        hoverColor="green">
                        add_circle
                    </FontIcon>
                </IconButton>
            }>
            <form onSubmit={(e) => {
                    onSubmit(user, adminRole, docRole); 
                    e.preventDefault();
                }}>
                <h5>Login Info</h5>
                <Row>
                    <Input required s={6} validate label="Email" 
                    onChange={event => user.email=event.target.value}>
                        <Icon>email</Icon>
                    </Input>
                    {/*<Col s={1} style={{marginTop: "2.5%"}}><Icon>lock</Icon></Col>
                    <Col s={5} style={{marginTop: "2%"}}>
                        <InputPassword required minLength={6}
                        style={{display:"inherit"}}
                        placeholder="Password"
                        pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$" 
                        title="Letter, number, and underscore only. 6 or more characters" 
                        onChange={text => user.password = text}/>
                    </Col>*/}
                </Row>
                <h5>Contact Info</h5>
                <Row>
                    {/*<Col s={12} m={6} className="center-align">
                        <Icon large>account_circle</Icon>
                        <form action="#">
                            <div className="file-field input-field">
                                <Button waves='light' className="blue-grey">
                                    <span>Picture</span>
                                    <input type="file"/>
                                </Button>
                                <div className="file-path-wrapper">
                                    <input className="file-path validate" type="text"/>
                                </div>
                            </div>
                        </form>
                    </Col>*/}
                    <Input required s={6} validate label="First Name" onChange={event => user.firstname=event.target.value}>
                        <Icon className="grey">perm_identity</Icon>
                    </Input>
                    <Input required s={6} validate label="Last Name" onChange={event => user.lastname=event.target.value}>
                        <Icon >perm_identity</Icon>
                    </Input>
                    <Input required s={6} type='tel' label="Phone" onChange={event => {
                        const number = event.target.value;
                        console.log(number, parse(number,'US'));
                        if (parse(number, 'US').country) {
                            user.phone=event.target.value;
                            console.log(user);
                            event.target.setCustomValidity("");
                        } else {
                            event.target.setCustomValidity("Incorrectly Formatted");
                        }
                    }}>
                        <Icon>phone</Icon>
                    </Input>
                    <Input required required s={6} label="Organization" onChange={event => user.organization=event.target.value}>
                        <Icon>business</Icon>
                    </Input>
                </Row>
                <h5>Roles</h5>
                <Row> 
                    <Col s={6}>
                        <Input name='group1' type='checkbox' value='admin' label='Admin' onChange={event => adminRole=event.target.value}/>
                        <Input name='group1' type='checkbox' value='doctor' label='Doctor' onChange={event => docRole=event.target.value}/>
                    </Col>
                    <Col s={6}>
                        <Toggle
                            defaultToggled={false}
                            label={
                                <span style={{fontWeight: '300', fontSize: '1.2rem', float:'right'}}>
                                    Active
                                </span>
                            }
                            onToggle={(e, isInputChecked) => user.activated = isInputChecked}
                        />
                    </Col>
                </Row>
                <Button waves='light' className="teal darken-4" type="submit" 
                    onClick={e => console.log(e)}>
                    Submit<Icon className="right">send</Icon>
                </Button>
            </form>
       </Modal>
    )
};

export default connect(null, mapDispatchToProps)(AddUser);