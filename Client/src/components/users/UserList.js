import Moment from 'moment';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Icon, Input } from 'react-materialize';
import Subheader from 'material-ui/Subheader';
import sortBy from 'lodash/sortBy';
import Toolbar, {ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import TableRowColumn from 'material-ui/Table/TableRowColumn';
import TableHeaderColumn from 'material-ui/Table/TableHeaderColumn';
import Popover, {PopoverAnimationVertical} from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';

import { getUsers, sortUsers, filterUsers, selectUserRows } from '../../redux/actions';
import { ListTitle } from '../common/ListTitle';
import { ListOptions, ListOptionsBar } from '../common/ListOptions';
import { InfoCardList } from '../common/InfoCardList';
import { UserDetailsModal } from './UserDetailsModal';
import { EditUserDetailsModal } from './EditUserDetailsModal';
import AddUser from './AddUserModal';
import { DeactivateUsersButton } from './DeactivateUsersButton';
import { ActivateUsersButton } from './ActivateUsersButton';
import { SearchField } from '../common/SearchField';
import { Loader } from '../common/Loader';
import { InactiveVisibilityToggle } from '../common/InactiveVisibilityToggle';
import { ReverseButton } from '../common/ReverseButton';
import MoreOptionsMenu from './MoreOptionsMenu';
import RefreshButton from '../common/RefreshButton';

const getFilteredUsers = (users, filter) => users = users.filter( user => {
    var result = false;

    for (var property in user ) {
        if (property === "activated") {
            property = user[property] ? 'active' : 'inactive';
        }

        if (typeof user[property] === 'string') {
            if(user[property].toLowerCase().startsWith(filter))
                return true;
        } 
    }
    
    const fullname = ( user.firstname + " " + user.lastname).toLocaleLowerCase().startsWith(filter);
    return fullname || user.roles.reduce((acc, val) => acc || val.startsWith(filter), false) ; 
});

const mapStateToProps = (state) => {
    var { isLoading } = state.loading;
    var { sort, descendingOrder, filter, inactiveUsersVisibile, list: users} = state.users;

    if ( !inactiveUsersVisibile && users) {
        users = users.filter(user => user.activated);
    }

    if ( filter !== '' ) {
        users = getFilteredUsers(users, filter);
    }

    users = sortBy(users, [sort]);

    if( descendingOrder ) {
        users.reverse();
    }

    return {
        sort,
        isLoading,
        users,
        currentFilter: filter,
        currentVisibility: inactiveUsersVisibile,
        sortOptions: [
            {value: 'firstname', label: 'First Name'},
            {value: 'lastname', label: 'Last Name'},
            {value: 'role', label: 'Role'},
            {value: 'last_active', label: 'Activity'}
        ]
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        getUsers: () => dispatch(getUsers()),
        sortBy: field => dispatch(sortUsers(field)),
        filter: keyword => dispatch(filterUsers(keyword)),
        select: users => rows => dispatch(selectUserRows(users, rows)),
        onVisibility: () => dispatch({type: "SELECT_USER_VISIBILITY_FILTER"}),
        reverse: () => dispatch({type: "TOGGLE_USER_ORDER"}),
    };
}

const cardMap = user => ({
    key: user.id,               
    title: 
        <div>
            <Subheader style={{paddingLeft: "0px"}} >Name</Subheader>
            <span className="center-align" style={{fontWeight: '500', fontSize: '1.6rem'}}>
                {user.firstname + " " + user.lastname}
            </span>
        </div>,
    detail: 
        <div>
            <Subheader style={{paddingLeft: "0px"}}>Roles</Subheader>
            <span className="center-align" style={{fontWeight: '300', fontSize: '1.3rem'}}>{user.roles.reduce((acc, val) => acc += val+" ", "")}</span>
        </div>,
    content: 
        <div>
            <Subheader style={{paddingLeft: "0px"}}>Last Active</Subheader>
            <span className="center-align" style={{fontWeight: '300', fontSize: '1.3rem'}}>{user.last_active}</span>
        </div>,
    action: 
        <p><UserDetailsModal 
            selectedUser={user}
            firstName={user.firstname}
            lastName={user.lastname}
            roles={user.roles}
            organization={user.organization}
            recentActivity={user.last_login}
            phone={user.phone}
            email={user.email}
            status={user.activated ? 'ACTIVE' : 'INACTIVE'}
        /></p>
});

const tableMap = user => ({
    key:user.id,
    title:
        <TableRowColumn>
            <span className="flow-text" style={{fontWeight: '400', fontSize: '1.6rem'}}>
                    {user.firstname + " " + user.lastname}
            </span>
        </TableRowColumn>,
    detail:
        <TableRowColumn>
            <span className="flow-text" style={{fontWeight: '300', fontSize: '1.3rem'}}>
                {user.roles.reduce((acc, val) => acc += val+" ", "")}
            </span>
        </TableRowColumn>,
    content:
        <TableRowColumn>
            <span className="flow-text" style={{fontWeight: '300', fontSize: '1.3rem'}}>
                {
                    user.last_login ?
                    Moment(user.last_login).local().fromNow() : "n/a"
                    }
            </span>
        </TableRowColumn>,
    action: 
        <TableRowColumn style={{width:12}}>
            <div onClick={e => {
                    e.preventDefault();
                    e.stopPropagation();
                }}>
                <MoreOptionsMenu user={user}/>
            </div>
        </TableRowColumn>
});

class UserListContainer extends Component {
    componentWillMount() {
        this.props.getUsers();
    }

    render() {
        const {users, select, sort, sortBy, filter, currentFilter, onVisibility, currentVisibility, reverse, sortOptions, getUsers} = this.props;
        return (
            <div className="container">
                <ListTitle title="Users"/>
                {/*<ListOptions 
                    sort={sort}
                    sortBy={sortBy}
                    filterBy={filter}
                    currentFilter={currentFilter}
                    addItemModal={<AddUser/>}
                    propList={[
                        {value: 'firstname', label: 'First Name'},
                        {value: 'lastname', label: 'Last Name'},
                        {value: 'role', label: 'Role'},
                        {value: 'last_active', label: 'Active'}
                    ]}
                />*/}
                {users.length === 0 &&
                <div className="center-align"><Loader size={"big"}/></div>}
                <InfoCardList
                    selectRows={select(users)}
                    showCheckboxes={false}
                    headers={[
                        <TableHeaderColumn>Name</TableHeaderColumn>,
                        <TableHeaderColumn>Roles</TableHeaderColumn>,
                        <TableHeaderColumn>Last Active</TableHeaderColumn>,
                        <TableHeaderColumn style={{width:12}}></TableHeaderColumn>
                    ]}
                    tableItems={!users ? [] : users.map(user => tableMap(user))}
                    cardItems={!users ? [] : users.map(user => cardMap(user))}>
                    <ListOptionsBar 
                        firstGroup={[
                            <AddUser/>,
                            <RefreshButton refresh={getUsers}/>
                        ]}
                        secondGroup={[
                            <InactiveVisibilityToggle 
                                currentVisibility={currentVisibility}
                                onVisibility={onVisibility}/>,
                            <ReverseButton onClick={reverse}/>,
                        ]}
                        thirdGroup={[
                            <IconButton
                                tooltip="Search" 
                                iconClassName="material-icons"
                                iconStyle={{color: "grey"}}>
                                search
                            </IconButton>,
                            <SearchField 
                                currentFilter={currentFilter} filterBy={filter}/>,
                            <IconButton 
                                tooltip="Sort"
                                iconClassName="material-icons"
                                iconStyle={{color: "grey"}}>
                                sort
                            </IconButton>,...
                            sortOptions.map((prop, i) => <Input 
                                key={i}
                                name='group1'
                                type='radio'
                                className='with-gap'
                                value={prop.value}
                                checked={sort === prop.value}
                                label={prop.label}
                                onChange={() => sortBy(prop.value)}/>
                            )]}/>
                </InfoCardList>
            </div>
        );
    }
}

export const UserList = connect(mapStateToProps, mapDispatchToProps)(UserListContainer);