import  React, {Component} from 'react';
import { connect } from 'react-redux';
import { Row, Col, Input, Icon, Preloader } from 'react-materialize';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import {ConditionalElement} from '../common/ConditionalElement';

export const ActivateUsersButton = connect(
    //map state to props
    (state, props) => ({
        show: state.users.selectedTableRows.length !== 0
    }), 
    //map dispatch to props
    dispatch => ({
        onDeactivate: () => dispatch({type: "SET_SELECTED_USERS_ACTIVATION", active: true})
    }))(({onDeactivate, show}) => 
    //stateless component
    <ConditionalElement condition={show}>
        <IconButton 
        tooltip={"Activate"} 
        onClick={onDeactivate}>
            <FontIcon 
                className="material-icons"
                color="grey"
                hoverColor="blue">
                power_settings_new
            </FontIcon>
        </IconButton>
    </ConditionalElement>
);

        // tooltipStyles={{top: "20px"}}
        // style={{padding:"0", width:"0", height:"0"}} 
        // iconStyle={{iconHoverColor:"red"}}
        // tooltip={"Disable"} 
        // onClick={onDeactivate} 
        // tooltipPosition={"bottom-right"} 