import React from 'react';
import { Row, Input } from 'react-materialize';
import { connect } from 'react-redux';
import includes from 'lodash/includes';

const mapStateToProps = (state, props) => {
    var { user } = props;
    var { error } = state;
    var rolesList = [];

    if ( user ) {
        const roles = user.roles;
        const isAdmin = includes(roles, "admin");
        const isDoc = includes(roles, "doctor");
        rolesList = [
            {
                value: "admin",
                label: "Admin",
                checked: isAdmin,
            },
            {
                value: "doctor",
                label: "Doctor",
                checked: isDoc,
            } 
        ];
    }

    return {
        error: error  ? error.message : "",
        rolesList
    };
}

const RolesCheckboxGroup = ({rolesList, onChange, user}) => {
    return (
        <div>
            <h5 className="left-align">Roles</h5>
            <Row>
                {rolesList.map((role, i) => <Input
                    key={i}
                    name="group1" 
                    type="checkbox" 
                    value={role.value} 
                    label={role.label} 
                    checked={role.checked} 
                    onChange={(event) => {
                        if (role.checked) {
                            const index = user.roles.indexOf(role.value);
                            user.roles.splice(index, 1);
                        }
                        else {
                            user.roles.push(role.value);
                        }
                        
                        role.checked = !role.checked;
                        this.forceUpdate();
                    }} 
                />)}
            </Row>
        </div>
    )
}

export default connect(mapStateToProps, null)(RolesCheckboxGroup);