import  React from 'react';
import { connect } from 'react-redux';
import { Row, Col, Input, Icon } from 'react-materialize';
import RolesCheckboxGroup from './RolesCheckboxGroup';
import Toggle from 'material-ui/Toggle';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import Dialog from 'material-ui/Dialog';

export const EditUserDetailsModal = ({ user, actions, open, update, handleClose }) => {
    return <Dialog
        title={"Edit Details"}
        titleStyle={{margin: 0,
            padding: "24px 24px 20px",
            color: "rgba(0, 0, 0, 0.870588)",
            fontSize: 32,
            fontWeight: 400,
            borderBottom: "none"}}
        actions={actions}
        modal={true}
        open={open}
        onRequestClose={handleClose}>
            <Row>
                <Input s={6} validate label="First Name" defaultValue={user.firstname} onChange={event => user.firstname=event.target.value}>
                    <Icon prefix>perm_identity</Icon>
                </Input>
                <Input s={6} validate label="Last Name" defaultValue={user.lastname} onChange={event => user.lastname=event.target.value}>
                    <Icon prefix>perm_identity</Icon>
                </Input>
                <Input s={6} validate label="Email" defaultValue={user.email} onChange={event => user.email=event.target.value}>
                    <Icon>email</Icon>
                </Input>
                <Input s={6} validate type='tel' label="Phone" defaultValue={user.phone} onChange={event => user.phone=event.target.value}>
                    <Icon>phone</Icon>
                </Input>
                <Input s={12} label="Organization" defaultValue={user.organization} onChange={event => user.organization=event.target.value}>
                    <Icon>supervisor_account</Icon>
                </Input>
                <Col s={6}>
                    <RolesCheckboxGroup user={user}/>
                </Col>
                <Col s={6}>
                    <h5 className="left-align">Activation Status</h5>
                    <Toggle
                        defaultToggled={user.activated}
                        label={
                            <span style={{fontWeight: '300', fontSize: '1.2rem', float:'right'}}>
                                Active
                            </span>
                        }
                        onToggle={(e, isInputChecked) => user.activated = isInputChecked}
                    />
                </Col>
            </Row>
    </Dialog>
};