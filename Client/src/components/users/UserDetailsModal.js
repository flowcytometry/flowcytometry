import  React from 'react';
import { Row, Col } from 'react-materialize';
import { EditUserDetailsModal } from './EditUserDetailsModal';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import Dialog from 'material-ui/Dialog';

export const UserDetailsModal = ({selectedUser, firstName, lastName, roles, organization="n/a", recentActivity="n/a", phone="n/a", email="n/a", status, actions, open, handleClose }) =>
<Dialog
title={"Details"}
titleStyle={{margin: 0,
    padding: "24px 24px 20px",
    color: "rgba(0, 0, 0, 0.870588)",
    fontSize: 32,
    fontWeight: 400,
    borderBottom: "none"}}
actions={actions}
modal={false}
open={open}
onRequestClose={handleClose}>
    <Row>
        <Col s={12} m={6}>
            <h6>Name</h6>
            <h5>{ firstName + " " + lastName }</h5>
            <h6>Phone</h6>
            <h5>{ phone }</h5>
            <h6>Email</h6>
            <h5>{ email }</h5>
        </Col> 
        <Col s={12} m={6}>
            <h6>Organization</h6>
            <h5>{ organization }</h5>
            <h6>Recent Activity</h6>
            <h5>{ recentActivity }</h5>
            <h6>Roles</h6>
            <h5>{roles}</h5>
            <h6>Activation Status</h6>
            <h5 className={ status === 'ACTIVE' ? "light-green-text text-darken-3" : "red-text text-darken-3"}>
                { status }
            </h5>
        </Col>
    </Row>
</Dialog>;