import React from 'react';
import { connect } from 'react-redux';
import { Row, Col, Button, Icon } from 'react-materialize';
import Dialog from 'material-ui/Dialog';
import Popover, {PopoverAnimationVertical} from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import { UserDetailsModal } from './UserDetailsModal';
import { EditUserDetailsModal } from './EditUserDetailsModal';
import { updateUser } from '../../redux/actions';

const mapDispatchToProps = dispatch => {
    return {
        update: user => dispatch(updateUser(user))
    };
}

class MoreOptionsMenu extends React.Component {
  state = {
    detailsOpen: false,
    editOpen: false,
  };

  handleDetailsOpen = () => {
    this.setState({detailsOpen: true});
  };

    handleEditOpen = () => {
        this.setState({editOpen: true});
    };

  handleClose = () => {
    this.setState({detailsOpen: false, editOpen: false});
  };

  render() {
    const {user, update} = this.props;

    return (
      <div>
        <IconMenu
            anchorOrigin={{horizontal: 'left', vertical: 'top'}}
            targetOrigin={{horizontal: 'left', vertical: 'top'}}
            iconButtonElement={
                <IconButton style={{padding:"0", width:"inherit", height:"inherit"}}>
                    <FontIcon 
                        className="material-icons"
                        color="grey"
                        hoverColor="black"
                        >
                        more_vert
                    </FontIcon>
                </IconButton>}>
            <MenuItem primaryText="Details" onTouchTap={this.handleDetailsOpen}/>
            <MenuItem primaryText="Edit" onTouchTap={this.handleEditOpen}/>
        </IconMenu>
        <UserDetailsModal 
            selectedUser={user}
            firstName={user.firstname}
            lastName={user.lastname}
            roles={user.roles}
            organization={user.organization}
            recentActivity={user.last_active}
            phone={user.phone}
            email={user.email}
            status={user.activated ? 'ACTIVE' : 'INACTIVE'}
            actions={[]}
            open={this.state.detailsOpen}
            handleClose={this.handleClose}
        />
        <EditUserDetailsModal 
            user={user} 
            open={this.state.editOpen} 
            handleClose={this.handleClose} 
            actions={[
                <Button waves='red' modal="close" className="btn-flat" 
                    onClick={this.handleClose}>Cancel</Button>, 
                <Button waves='light' modal='close' className="teal darken-4" type="submit" 
                    onClick={() => {update(user); this.handleClose()}}>
                    Submit<Icon className="right">send</Icon>
                </Button>
            ]}/>
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(MoreOptionsMenu);