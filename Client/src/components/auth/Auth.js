import React, { Component } from 'react';
import { browserHistory } from 'react-router'
import { connect } from 'react-redux';
import { Row, Icon } from 'react-materialize';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';

class AuthContainer extends Component {
  render() {
    const { isLoggedIn } = this.props;
    if (isLoggedIn) {
      return this.props.children
    } else {
      return (
        <div>
          <p className="flow-text red-text text-lighten-2 center-align">
              <Icon className="red-text text-darken-1">warning</Icon> You are not logged in.
          </p>
          <p className="center-align">
            <FlatButton
            onClick={() => browserHistory.replace('/login')}
            label="Login"
            icon={<FontIcon color="grey" className="material-icons">send</FontIcon>}
            />
          </p>
        </div>
      );
    }
  }
}

function mapStateToProps(state, ownProps) {
  return {
    isLoggedIn: state.user.loggedIn,
    currentURL: ownProps.location.pathname
  }
}

export default connect(mapStateToProps)(AuthContainer)