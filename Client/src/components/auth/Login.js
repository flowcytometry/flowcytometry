//Login.js
import React from 'react';
import { connect } from 'react-redux';
import { Row, Col, Button, Preloader } from 'react-materialize';
import { loginRequest } from '../../redux/actions.js';
import { ErrorMessage } from '../ErrorMessage';
import { Loader } from '../common/Loader';
import { Link } from 'react-router';

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = (dispatch) => ({
    onLoginClick: (email, password) => dispatch(loginRequest(email, password))
});

const Login=({ onLoginClick, }) => {
    let emailInput, passwordInput;
    return (
        <div className="row">
            <div className="col s12 m6 offset-m3">
                <div className="card">
                    <form>
                        <div className="card-content">
                            <span className="card-title">Login</span>
                            <div className="row">
                                <div className="input-field col s12">
                                    <input ref={ node => {emailInput = node;} } id="email" type="email" className="validate"/>
                                    <label >Email</label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col s12">
                                    <input ref={ node => {passwordInput = node;} } id="password" type="password" className="validate"/>
                                    <label >Password</label>
                                </div>
                            </div>
                        </div>
                        <div className="card-action center-align">
                            <button 
                                className="btn waves-effect waves-light light-green darken-2" 
                                type="submit" 
                                name="action"
                                onClick={e => { 
                                    e.preventDefault();
                                    onLoginClick(emailInput.value, passwordInput.value);
                                }}> 
                                    Submit <i className="material-icons right">send</i>
                            </button>
                        </div>
                        <div className="center-align">
                            <Link to='/Forgot'>
                            Forgot your password?
                            </Link>
                        </div>
                        <div className="center-align"><Loader/></div>
                    </form>
                </div>
            </div>
        </div>        
     );
};

export const LoginContainer = connect(mapStateToProps, mapDispatchToProps)(Login)