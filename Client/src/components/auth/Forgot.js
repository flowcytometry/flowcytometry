import React from 'react';
import { connect } from 'react-redux';
import { Row, Col, Button, Input, Icon } from 'react-materialize';
import { resetPassword } from '../../redux/actions.js';
import { Loader } from '../common/Loader';
import Snackbar from 'material-ui/Snackbar';

const mapStateToProps = state => {
    return {resetEmailAddress: state.users.resetEmailAddress};
};

const mapDispatchToProps = (dispatch) => ({
    onReset: (emailAddress) => dispatch(resetPassword(emailAddress))
});

const Forgot=({ onReset, resetEmailAddress }) => {
    let emailAddress;
    return (
        <Row>
            <Col s={12} m={6} offset="m3">
                <div className="card">
                    <form onSubmit={e => {
                        e.preventDefault();
                        onReset(emailAddress);
                    }}>
                        <div className="card-content">
                            <span className="card-title">Reset Password</span>
                            <Row>
                                <Input required s={12} validate label="Email" 
                                onChange={event => emailAddress=event.target.value}>
                                <Icon>email</Icon>
                                </Input>
                            </Row>
                        </div>
                        <div className="card-action center-align">
                            <button 
                                className="btn waves-effect waves-light light-green darken-2" 
                                type="submit" >
                                    Reset Password <i className="material-icons right">send</i>
                            </button>
                        </div>
                    </form>
                </div>
                <div className="center-align"><Loader/></div>
            </Col>
            <Snackbar
            open={resetEmailAddress}
            message={"Password reset email sent to " + resetEmailAddress}
            autoHideDuration={4000}
            onRequestClose={e => resetEmailAddress=false}
            />
        </Row>
     );
};

export default connect(mapStateToProps, mapDispatchToProps)(Forgot)