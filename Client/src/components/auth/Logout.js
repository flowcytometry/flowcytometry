import React from 'react';
import { connect } from 'react-redux';
import { Button } from 'react-materialize';
import { logout } from '../../redux/actions.js';

const mapDispatchToProps = (dispatch) => {
  return {
    onLogoutClick: () => dispatch(logout())
  };
};

const PresentationalLogout = ( { onLogoutClick }) => (
    <Button onClick={() => { onLogoutClick(); }}
        waves='light' className="red lighten-2">
        Logout
    </Button>
)

export const Logout = connect(null, mapDispatchToProps)(PresentationalLogout)