import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { Icon } from 'react-materialize';
import { Settings } from './Settings';
import { Logout } from './auth/Logout';
import Divider from 'material-ui/Divider';

const mapStateToProps = (state) => {
    var user = state.user;
    var {loggedIn, roles} = state.user;
    var linkElements = [];
    
    if (loggedIn) {
        linkElements = [
            <Settings triggerElement={<Icon>settings</Icon>}/>,
            <Link to='/Login'>
                <Logout/>
            </Link>
        ];
        
        if (roles.includes("admin")) {
            linkElements.unshift(<Link to='/Users'>Users</Link>);
        }
        
        if (roles.includes("doctor")) {
            linkElements.unshift(<Link to='/Patients'>Patients</Link>)
        }

        // linkElements.unshift(<div style={{fontFamily: "Ubuntu", fontSize: "100%"}}>{user.firstname.charAt(0).toUpperCase() + user.firstname.slice(1)}</div>);
    }

    return { links: linkElements }
};

const MobileNav = (links) => {
    if(links.length > 0) {
        links.splice(links.length - 2, 1, <Settings triggerElement={"Settings"}/>);
        links.splice(links.length - 1, 0, <Divider/>);
    }
    return links
}

const NavBar = ({ links }) => (
    <nav className="blue-grey darken-4">
        <div className="nav-wrapper">
            {/*<a href="#" className="brand-logo">FCS.VR</a>*/}
            <Link to="/login">
                <img src="./img/navbarlogo.png" className="brand-logo" width="230px" style={{marginLeft: "0.5%"}}/>
            </Link>
            <a href="#" data-activates="mobile-demo" className="button-collapse"><i className="material-icons">menu</i></a>
            <ul className="right hide-on-med-and-down">
                {links.map((link, i) => <li key={i}>{link}</li>)}
            </ul>
            <ul className="side-nav" id="mobile-demo">
                {MobileNav(links).map((link, i) => <li key={i}>{link}</li>)}
            </ul>
        </div>
    </nav>
);

export const Nav = connect(mapStateToProps, null)(NavBar)
