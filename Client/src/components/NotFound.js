import  React, {Component} from 'react';
import {Row} from 'react-materialize';

export default () => <Row><p className="center-aligned">Error: 404 Page Not Found. Maybe you entered the wrong url?</p></Row>;