import React from 'react';
import { Row, Col } from 'react-materialize';

export const InfoCard = ({cardContent, cardAction}) => (
    <Row className='card-panel z-depth-4'>
        {cardContent.map((content, i) => <Col key={i} s={12} className="center-align">{content}</Col>)}
        <Col s={12} className="center-align">
            { cardAction }
        </Col>
    </Row>
);
