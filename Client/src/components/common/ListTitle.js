import React from 'react';
import { Row, Col } from 'react-materialize';

export const ListTitle = ({title}) => (
    <Row>
        <Col s={12} className="center-align">
            <h2 className="header">{ title }</h2>
        </Col>
    </Row>
);
