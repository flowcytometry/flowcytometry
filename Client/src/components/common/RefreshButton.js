import  React, {Component} from 'react';
import { connect } from 'react-redux';
import { Row, Col, Input, Icon, Preloader } from 'react-materialize';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import {ConditionalElement} from '../common/ConditionalElement';

export default connect(
    //map state to props
    (state, props) => ({
        show: state.users.selectedTableRows.length !== 0
    }), 
    //map dispatch to props
    (dispatch, props) => ({
        onRefresh: props.refresh
    }))(({onRefresh, show}) => 
    //stateless component
    <IconButton 
    tooltip={"Refresh"} 
    onClick={onRefresh}>
        <FontIcon 
            className="material-icons"
            color="grey"
            hoverColor="black">
            refresh
        </FontIcon>
    </IconButton>
);