import  React, {Component} from 'react';
import { connect } from 'react-redux';
import { Row, Col, Input, Icon, Preloader } from 'react-materialize';
import { InfoCard } from './InfoCard';
import { AppBar, Table, TableRow, TableRowColumn, TableHeader, TableHeaderColumn, TableBody, TableFooter, Paper, IconButton, Card, CardHeader, CardText} from 'material-ui';
import MediaQuery from 'react-responsive';
import Subheader from 'material-ui/Subheader';

const mapStateToProps = (state) => {
    var { isLoading } = state.loading;

    return {
        isLoading,
    };
}

const CardList = ({items}) => <ul>
    {items.map((item, i) =>
        <InfoCard
        key={item.key}
        cardContent={[item.title, item.detail, item.content]}
        cardAction={item.action}
        />
    )}
</ul>;

const TableView = ({showCheckboxes, items, headers, selectRows, onDelete, rows}) => 
<Paper zDepth={1}>
    <Table 
    fixedHeader={true} 
    fixedFooter={true}
    selectable={showCheckboxes} 
    multiSelectable={showCheckboxes} 
    onRowSelection={rows => selectRows(rows)}>
        <TableHeader displaySelectAll={showCheckboxes} enableSelectAll={showCheckboxes} adjustForCheckbox={showCheckboxes}>
            <TableRow>
                {headers}
            </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={showCheckboxes} showRowHover={false} deselectOnClickaway={false}>
            {rows ? 
            rows :
            items.map(item =>
                <TableRow key={item.key} hoverable={true}>
                    {item.title}
                    {item.detail}
                    {item.content} 
                    {item.action}
                </TableRow>
            )}
        </TableBody>
    </Table>
</Paper>

export class InfoCardListContainer extends Component {
    render() {
        var { showCheckboxes, tableItems, cardItems, headers, isLoading, selectRows, onDelete, children, rows } = this.props;

        return (
            <div>
                {!isLoading && cardItems.length === 0 && <h5 className="grey-text text-darken-1 center-align">No Results Found</h5>}
                <MediaQuery minDeviceWidth={768}>
                    {children}
                    <TableView 
                    onDelete={onDelete}
                    selectRows={selectRows}
                    showCheckboxes={showCheckboxes} 
                    items={tableItems} 
                    headers={headers}
                    rows={rows}/>
                </MediaQuery>
                <MediaQuery maxDeviceWidth={767}>
                     <CardList items={cardItems}/>
                </MediaQuery>
            </div>);
    }
}

export const InfoCardList = connect(mapStateToProps, null)(InfoCardListContainer);
