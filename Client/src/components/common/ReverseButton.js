import React, { Component } from 'react';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';

export const ReverseButton = ({...props}) => <IconButton
    tooltip="Reverse"
    {...props}>
    <FontIcon 
        className="material-icons"
        color="grey"
        hoverColor="black">
        swap_vert
    </FontIcon>
</IconButton>;