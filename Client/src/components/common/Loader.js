import React from 'react';
import {connect} from 'react-redux';
import { Preloader } from 'react-materialize';

export const Loader = connect(
    state => ({isLoading: state.loading.isLoading}), null)(
        ({isLoading, size}) => {
            if (isLoading)
                return <Preloader size={size}/>;
            else
                return null;
});