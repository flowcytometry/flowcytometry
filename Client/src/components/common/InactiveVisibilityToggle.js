import  React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';

export const InactiveVisibilityToggle = ({currentVisibility, onVisibility}) => (
<IconButton 
    tooltipPosition={"bottom-center"}
    tooltip={currentVisibility ? "Hide Inactive" : "Show Inactive"}
    onClick={onVisibility}>
        <FontIcon 
            className="material-icons"
            color={currentVisibility ? "teal" : "grey"}
            hoverColor="teal">
            visibility_off
        </FontIcon>
</IconButton>
);