import  React, {Component} from 'react';

export const ConditionalElement = ({condition, children}) => {
    console.log(condition);
    if ( condition ) {
        return children;
    } else {
        return null;
    }
};