import React from 'react';
import { Row, Col, Input, Icon } from 'react-materialize';
import Toolbar, {ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import Paper from 'material-ui/Paper';
import { InactiveVisibilityToggle } from '../common/InactiveVisibilityToggle';

export const ListOptions = ({sort, sortBy, addItemModal, filterBy, currentFilter, propList}) => (
    <Row>
        <Col s={12} m={2} className="center-align">
            {addItemModal}
        </Col>
        <Input s={12} m={3} label="Search" value={currentFilter} onChange={event => filterBy(event.target.value.toLowerCase())}><Icon>search</Icon></Input>
        <Col className="center-align">
            <h5>Sort</h5>
            {propList.map((prop, i) => <Input 
                key={i}
                name='group1'
                type='radio'
                className='with-gap'
                value={prop.value}
                checked={sort === prop.value}
                label={prop.label}
                onChange={() => sortBy(prop.value)}/>
            )}
        </Col>
    </Row>
);

export const ListOptionsBar = ({firstGroup, secondGroup, thirdGroup}) => (
    <Paper zDepth={1}>
        <Toolbar className="white">
            <ToolbarGroup firstChild={true}>
                {firstGroup.map((item, i) => <div key={i}>{item}</div>)}
            </ToolbarGroup>
            <ToolbarGroup>
                {secondGroup.map((item, i) => <div key={i}>{item}</div>)}
                <ToolbarSeparator/>
                {thirdGroup.map((item, i) => <div key={i}>{item}</div>)}
            </ToolbarGroup>
        </Toolbar>
    </Paper>
);