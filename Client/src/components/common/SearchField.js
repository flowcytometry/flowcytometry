import  React, {Component} from 'react';
import { connect } from 'react-redux';
import { Row, Col, Input, Icon, Preloader } from 'react-materialize';
import { IconButton} from 'material-ui';

export const SearchField = ({currentFilter, filterBy, show}) => {
    return <Input s={12} l={6} label="Search" value={currentFilter} onChange={event => filterBy(event.target.value.toLowerCase())}></Input>;
;
};