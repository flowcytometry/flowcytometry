import  React from 'react';
import { connect } from 'react-redux';
import { Row, Col, Input, Icon, Button, Modal } from 'react-materialize';
import { addPatient } from '../../redux/actions';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';

const mapDispatchToProps = dispatch => {

    return {
        onSubmit: (firstname, lastname, email, phone) => {
            dispatch(addPatient(
                {firstname, lastname, email, phone}
            ));
        }
    };
};

const AddPatientModal = ({onSubmit, errorMessage}) => {
    let firstname, lastname, email, phone;
    return (
        <Modal
            header='Add Patient'
            actions={[
                <Button waves='red' modal="close" className="btn-flat">Cancel</Button>, 
                <Button waves='light' modal='close' className="teal darken-4" type="submit" onClick={e => onSubmit(firstname, lastname, email, phone)}>Submit<Icon className="right">send</Icon></Button>
            ]}
            trigger={
                <IconButton
                    tooltip={"Add"}>
                    <FontIcon 
                        className="material-icons"
                        color="grey"
                        hoverColor="green">
                        add_circle
                    </FontIcon>
                </IconButton>
            }>
            <Row>
                <Input s={6} validate label="First Name" onChange={event => firstname=event.target.value}><Icon prefix>perm_identity</Icon></Input>
                <Input s={6} validate label="Last Name" onChange={event => lastname=event.target.value}><Icon prefix>perm_identity</Icon></Input>
                <Input s={6} validate label="Email" onChange={event => email=event.target.value}><Icon>email</Icon></Input>
                <Input s={6} validate type='tel' label="Phone" onChange={event => phone=event.target.value}><Icon>phone</Icon></Input>
            </Row>
        </Modal>
    );
}

export default connect(null, mapDispatchToProps)(AddPatientModal);