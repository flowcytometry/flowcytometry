import  React from 'react';
import { connect } from 'react-redux';
import { Row, Col, Input, Icon, Button, Modal } from 'react-materialize';
import { updatePatient } from '../../redux/actions';
import Dialog from 'material-ui/Dialog';

const mapStateToProps = (state, props) => {
    return {
        doctors: state.users.list.filter(user => user.roles.includes("doctor"))
    };
}

export const EditPatientDetails = ({ patient, doctors, update, actions, open, handleClose }) => {
    return (
        <Dialog
        title={"Edit Details"}
        titleStyle={{margin: 0,
            padding: "24px 24px 20px",
            color: "rgba(0, 0, 0, 0.870588)",
            fontSize: 32,
            fontWeight: 400,
            borderBottom: "none"}}
        actions={actions}
        modal={true}
        open={open}
        onRequestClose={handleClose}
        autoScrollBodyContent={true}>
        <Row>
            <Input s={6} validate label="First Name" defaultValue={patient.firstname} onChange={event => patient.firstname=event.target.value}>
                <Icon prefix>perm_identity</Icon>
            </Input>
            <Input s={6} validate label="Last Name" defaultValue={patient.lastname} onChange={event => patient.lastname=event.target.value}>
                <Icon prefix>perm_identity</Icon>
            </Input>
            <Input s={6} validate label="Email" defaultValue={patient.email} onChange={event => patient.email=event.target.value}>
                <Icon>email</Icon>
            </Input>
            <Input s={6} validate type='tel' label="Phone" defaultValue={patient.phone} onChange={event => patient.phone=event.target.value}>
                <Icon>phone</Icon>
            </Input>
            <Input s={6} type='select' name="doctor_ids" label="Migrate Patient" defaultValue={patient.doctor} onChange={event => {
                patient.doctor=event.target.value;
            }}>
                {doctors.map(doc => {
                    return <option key={doc._id} value={doc._id} >
                        {"Dr. " + doc.firstname + " " + doc.lastname}
                        {doc.id === patient.doctor ? " ( Current )" : ""}
                    </option>
                })}
            </Input>
        </Row>
    </Dialog>
)
};

export const EditPatientDetailsModal = connect(mapStateToProps, null)(EditPatientDetails);