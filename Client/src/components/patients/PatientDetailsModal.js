import  React, {Component} from 'react';
import { connect } from 'react-redux';
import { Row, Col, Icon, Button, Modal } from 'react-materialize';
import { EditPatientDetailsModal } from './EditPatientDetailsModal';
import Dialog from 'material-ui/Dialog';

const mapStateToProps = (currenState, props) => {
    var {err} = currenState;

    var {firstname, 
        lastname, 
        phone, 
        email, 
        address, 
        zipcode, 
        state, 
        doctor, 
    } = props.patient;

    const {patient} = props;

    return {
        error: err  ? err.message : "",
        firstname, 
        lastname, 
        phone, 
        email, 
        address, 
        zipcode, 
        state,  
        doctor,
        patient
    };
}

class PatientDetailsContainer extends Component {
    render() {
        var {
            patient,
            firstname, 
            lastname, 
            phone, 
            email, 
            address, 
            doctor,
            patient,
            actions,
            handleClose,
            open
        } = this.props;
            
        return (
            <Dialog
            title={"Details"}
            titleStyle={{margin: 0,
                padding: "24px 24px 20px",
                color: "rgba(0, 0, 0, 0.870588)",
                fontSize: 32,
                fontWeight: 400,
                borderBottom: "none"}}
            actions={actions}
            modal={false}
            open={open}
            onRequestClose={handleClose}>
                <Row>
                    <Col s={12} m={5}>
                        <h6>Name</h6>
                        <h5>{ firstname + " " + lastname }</h5>
                        <h6>Phone</h6>
                        <h5>{ phone }</h5>
                        <h6>Email</h6>
                        <h5>{ email }</h5>
                    </Col>
                    <Col s={12} m={5}>
                        <h6>Doctor</h6>
                        <h5>{doctor}</h5>
                        <h6>Number of Uploaded FCS Data</h6>
                        <h5>{ patient.num_tests }</h5>
                    </Col> 
                </Row>
            </Dialog>
        );
    }
}

export const PatientDetails = connect(mapStateToProps)(PatientDetailsContainer);