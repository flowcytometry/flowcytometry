import React, { Component } from 'react';
import { Link } from 'react-router';
import { Button, Input } from 'react-materialize';
import { connect } from 'react-redux';
import { ListTitle } from '../common/ListTitle';
import { ListOptions, ListOptionsBar } from '../common/ListOptions';
import { InfoCardList } from '../common/InfoCardList';
import { getPatients, selectPatient, sortPatients, filterPatients } from '../../redux/actions';
import { PatientDetails } from './PatientDetailsModal';
import AddPatientModal from './AddPatientModal';
import Subheader from 'material-ui/Subheader';
import TableRowColumn from 'material-ui/Table/TableRowColumn';
import TableHeaderColumn from 'material-ui/Table/TableHeaderColumn';
import { ConditionalElemnet } from '../common/ConditionalElement';
import { Loader } from '../common/Loader';
import { InactiveVisibilityToggle } from '../common/InactiveVisibilityToggle';
import sortBy from 'lodash/sortBy';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import { ReverseButton } from '../common/ReverseButton';
import MoreOptionsMenu from './MoreOptionsMenu';
import RefreshButton from '../common/RefreshButton';

const getFilteredPatietns = (patients, filter) => patients.filter( patient => {
    const firstname = patient.firstname.toLowerCase(),
            lastname = patient.lastname.toLowerCase(),
            email = patient.email.toLowerCase(),
            phone = patient.phone.toLowerCase();

    return firstname.startsWith(filter) ||
            lastname.startsWith(filter) ||
            email.startsWith(filter) ||
            phone.startsWith(filter);
});

const mapStateToProps = (state, props) => {
    var { sort, descendingOrder, filter, inactivePatientsVisible, list: patients} = state.patients;

    if ( filter !== 'all' && patients) {
        patients = getFilteredPatietns(patients, filter);
    }

    patients = sortBy(patients, [sort]);

    if( descendingOrder ) {
        patients.reverse();
    }

    return {
        patients,
        sort,
        currentFilter: filter,
        sortOptions: [
            {value: 'firstname', label: 'First Name'},
            {value: 'lastname', label: 'Last Name'}
        ]
    };
}

const mapDispatchToProps = dispatch => {
  return {
    getPatients: () => dispatch(getPatients()),
    selectPatient: (patient) => dispatch(selectPatient(patient)),
    sortBy: field => dispatch(sortPatients(field)),
    filter: keyword => dispatch(filterPatients(keyword)),
    selectRows: patients => rows => dispatch({type: "SELECT_PATIENT_ROWS", patients, rows}),
    reverse: () => dispatch({type: "TOGGLE_PATIENT_ORDER"})
  };
}

const cardMap = (patient, selectPatient) => ({
    key: patient.id,
    title:<div>
            <Subheader style={{paddingLeft: "0px"}} >Name</Subheader>
            <span style={{fontWeight: '500', fontSize: '1.6rem'}}>
                {patient.firstname + " " + patient.lastname}
            </span>
        </div>,
    detail:<div>
            <Subheader style={{paddingLeft: "0px"}}>Email</Subheader>
            <span style={{fontWeight: '300', fontSize: '1.3rem'}}>{patient.email}</span>
        </div>,
    content:
        <Link to='/FCSTests'>
                <p><Button flat waves='light' modal="close" 
                onClick={() => selectPatient(patient)}>
                    View FCS Data
                </Button></p>
        </Link>,
    action: <PatientDetails patient={patient}/>
});

const tableMap = (patient, selectPatient) => ({
    key: patient.id,
    title:
        <TableRowColumn>
            <span style={{fontWeight: '400', fontSize: '1.6rem'}}>
                {patient.firstname + " " + patient.lastname}
            </span>
        </TableRowColumn>,
    detail:
        <TableRowColumn>
            <span style={{fontWeight: '400', fontSize: '1rem'}}>
                {patient.email}
            </span>
        </TableRowColumn>,
    content:
        <TableRowColumn>
            <Link to='/FCSTests'>
                <Button  waves='light' className="blue-grey" modal="close"
                onClick={() => selectPatient(patient)}>
                    View FCS Data
                </Button>
            </Link>
        </TableRowColumn>,
    action: 
        <TableRowColumn style={{width:12}}>
            <MoreOptionsMenu patient={patient}/>
        </TableRowColumn>
});

class PatientListContainer extends Component {
    componentWillMount() {
        this.props.getPatients();
    }

    render() {
        const {patients, selectRows, sort, sortBy, filter, currentFilter, onVisibility, currentVisibility, reverse, sortOptions, selectPatient, getPatients} = this.props;
        return (
            <div className="container">
                <ListTitle title="Patients"/>
                <div className="center-align"><Loader size="big"/></div>
                {/*<ListOptions 
                    sort={this.props.sort}
                    sortBy={this.props.sortBy}
                    currentFilter={this.props.currentFilter}
                    filterBy={this.props.filter}
                    addItemModal={<AddPatientModal/>}
                    propList={[
                        {value: 'firstname', label: 'First Name'},
                        {value: 'lastname', label: 'Last Name'}
                    ]}
                />*/}
                <InfoCardList
                    showCheckboxes={false}
                    headers={[
                        <TableHeaderColumn>Name</TableHeaderColumn>,
                        <TableHeaderColumn>Email</TableHeaderColumn>, 
                        <TableHeaderColumn></TableHeaderColumn>,
                        <TableHeaderColumn style={{width:12}}></TableHeaderColumn>
                    ]}
                    tableItems={this.props.patients ? 
                        this.props.patients.map((patient, i) => tableMap(patient, selectPatient)) : []}
                    cardItems={this.props.patients ? 
                        this.props.patients.map((patient, i) => cardMap(patient, selectPatient)) : []} 
                >
                    <ListOptionsBar 
                        firstGroup={[
                            <AddPatientModal/>,
                            <RefreshButton refresh={getPatients}/>
                        ]}
                        secondGroup={[<ReverseButton onClick={reverse}/>]}
                        thirdGroup={[
                            <IconButton
                                tooltip="Search" 
                                iconClassName="material-icons"
                                iconStyle={{color: "grey"}}>
                                search
                            </IconButton>,
                            <Input
                                s={12}
                                l={6}
                                label="Search"
                                value={currentFilter}
                                onChange={event => filter(event.target.value.toLowerCase())}/>,
                            <IconButton 
                                tooltip="Sort"
                                iconClassName="material-icons"
                                iconStyle={{color: "grey"}}>
                                sort
                            </IconButton>,...
                            sortOptions.map((prop, i) => <Input 
                                key={i}
                                name='group1'
                                type='radio'
                                className='with-gap'
                                value={prop.value}
                                checked={sort === prop.value}
                                label={prop.label}
                                onChange={() => sortBy(prop.value)}/>
                            )]}/>
                </InfoCardList>
            </div>
        );
    }
}

export const PatientList = connect(mapStateToProps, mapDispatchToProps)(PatientListContainer);