import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Icon } from 'react-materialize';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';

export const ErrorMessage = connect(
    state => ({message: state.error.message}),
    dispatch => ({
        clearError: () => dispatch({type:"CLEAR_ERROR"})
    }))(({message, clearError}) => {
        if (message)
            return (
                <Row>
                    <Col s={12}>
                        <p className="flow-text red-text text-lighten-2 center-align">
                            <Icon className="red-text text-darken-1">warning</Icon> { message }
                            <IconButton
                                tooltip="Close"
                                onClick={clearError}>
                                <FontIcon 
                                    className="material-icons"
                                    color="grey"
                                    hoverColor="black">
                                    close
                                </FontIcon>
                            </IconButton>
                        </p>
                    </Col>
                </Row>
            );
        else
            return null;
});