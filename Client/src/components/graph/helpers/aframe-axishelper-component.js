! function(e) {
  function r(o) {
    if (t[o]) return t[o].exports;
    var i = t[o] = {
      exports: {},
      id: o,
      loaded: !1
    };
    return e[o].call(i.exports, i, i.exports, r), i.loaded = !0, i.exports
  }
  var t = {};
  return r.m = e, r.c = t, r.p = "", r(0)
}([function(e, r) {
  if ("undefined" == typeof AFRAME) throw new Error("Component attempted to register before AFRAME was available.");

  AFRAME.registerComponent('axis-helper', {
    schema: {
      colorX: {default: '#f44242'},
      colorY: {default: '#eaef5d'},
      colorZ: {default: '#a85eed'},
      length: {type: 'int', default: 7},
      origin:  {type: 'vec3', default: {x: 0, y: 0, z: 0}},
      dirX:    {type: 'vec3', default: {x: 5, y: 0, z: 0}},
      negDirX: {type: 'vec3', default: {x: -5, y: 0, z: 0}},
      dirY:    {type: 'vec3', default: {x: 0, y: 5, z: 0}},
      negDirY: {type: 'vec3', default: {x: 0, y: -5, z: 0}},
      dirZ:    {type: 'vec3', default: {x: 0, y: 0, z: 5}},
      negDirZ: {type: 'vec3', default: {x: 0, y: 0, z: -5}},
      isNegX: {type: 'boolean', default: false},
      isNegY: {type: 'boolean', default: false},
      isNegZ: {type: 'boolean', default: false}
    },
    init: function() {
      var e = this.el.object3D,
          r = this.data,
          l = r.length,
          o = r.origin,
          cx = r.colorX,
          cy = r.colorY,
          cz = r.colorZ,
          px = r.dirX,
          nx = r.negDirX,
          py = r.dirY,
          ny = r.negDirY,
          pz = r.dirZ,
          nz = r.negDirZ,
          ix = r.isNegX,
          iy = r.isNegY,
          iz = r.isNegZ,
          posXArrow = new THREE.ArrowHelper( px, o, l, cx ),
          negXArrow = new THREE.ArrowHelper( nx, o, l, cx ),
          posYArrow = new THREE.ArrowHelper( py, o, l, cy ),
          negYArrow = new THREE.ArrowHelper( ny, o, l, cy ),
          posZArrow = new THREE.ArrowHelper( pz, o, l, cz ),
          negZArrow = new THREE.ArrowHelper( nz, o, l, cz );

      if(ix)
        e.add(negXArrow);

      if(iy)
        e.add(negYArrow);

      if(iz)
        e.add(negZArrow);

      e.add(posXArrow);
      e.add(posYArrow);
      e.add(posZArrow);

    }
  })
}]);
