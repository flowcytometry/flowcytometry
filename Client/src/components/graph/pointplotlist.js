import React, {
    Component
} from 'react';
import {
    connect
} from 'react-redux';
import * as actions from '../../redux/actions';

const mapStatetoProps = (state) => {
    var {
        points
    } = state.fcs;
    var {
        setOrbitControls
    } = state.leap;

    return {
        points: points ? points : [],
        setOrbitControls
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        handFound: () => dispatch(actions.handFound()),
        handLost: () => dispatch(actions.handLost())
    };
}

class PointPlotList extends Component {
    render() {
        // var title = this.props.title;
        // var points = this.props.points;
        // var autoRotate = "autoRotate: false;",
        //     target = "target: #target;",
        //     damping = "enableDamping: true;",
        //     dampingFactor = "dampingFactor: 0.125;",
        //     rotationSpeed = "rotateSpeed:0.25;",
        //     minDistance = "minDistance:3;",
        //     maxDistance = "maxDistance:100;";

        // var orbitSettings = autoRotate + target + damping + dampingFactor + rotationSpeed + minDistance + maxDistance;

        return <a href="graph.html" >my link</a>
    }
}

export const Graph = connect(mapStatetoProps, mapDispatchToProps)(PointPlotList);