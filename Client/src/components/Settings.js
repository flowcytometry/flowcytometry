import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Input, Icon, Button, Modal } from 'react-materialize';
import RolesCheckboxGroup from './users/RolesCheckboxGroup';
import { updateUser } from '../redux/actions';

const DoctorNotificationSettingsView = ({user}) => {
    if (user.settings) {
        return <Row>
            {/*<Row>        
                <Col s={6}>
                    <span>FCS Process Start</span>
                </Col>
                <Col s={6}>
                    <Input name='DocNotificaitonGroup' type='checkbox' value='Email' label='Email' defaultChecked={user.settings.notifications.processing_start ? 'Checked' : ''}  onChange={event => {
                        user.settings.notifications.processing_start = !user.settings.notifications.processing_start;
                    }}/>
                    <Input name='DocNotificaitonGroup' type='checkbox' value='Text' label='Text' />
                </Col>
            </Row>*/}
            <Row>
                <Col s={6}>
                    <span>FCS Process Completion</span>
                </Col>
                <Col s={6}>
                    <Input name='DocNotificaitonGroup' type='checkbox' value='Email' label='Email' defaultChecked={user.settings.notifications.processing_end ? 'Checked' : ''}  onChange={event => {
                        user.settings.notifications.processing_end = !user.settings.notifications.processing_end;
                    }}/>
                    <Input name='DocNotificaitonGroup' type='checkbox' value='Text' label='Text' />
                </Col>
            </Row>
        </Row>;
    } else 
        return null;
};

const SettingsView = ({ triggerElement, user, updateUser }) => { 
    return (
        <Modal
            header='Settings'
            fixedFooter
            actions={[
                <Button waves='red' modal="close" className="btn-flat">Cancel</Button>, 
                <Button waves='light' modal='close' className="teal darken-4" type="submit"onClick={() => updateUser(user)}>Submit<Icon className="right">send</Icon></Button>
            ]}
            trigger={<a>{triggerElement}</a>}>
            <h5>Personal Info</h5>
            <Row>
                {/*<Col s={12} m={6} className="center-align">
                    <Icon large>account_circle</Icon>
                    <form action="#">
                        <div className="file-field input-field">
                            <Button waves='light' className="blue-grey">
                                <span>Picture</span>
                                <input type="file"/>
                            </Button>
                            <div className="file-path-wrapper">
                                <input className="file-path validate" type="text"/>
                            </div>
                        </div>
                    </form>
                </Col>*/}
                <Input s={6} validate label="First Name" defaultValue={user.firstname} onChange={event => user.firstname=event.target.value}><Icon prefix>perm_identity</Icon></Input>
                <Input s={6} validate label="Last Name" defaultValue={user.lastname} onChange={event => user.lastname=event.target.value}><Icon prefix>perm_identity</Icon></Input>
                <Input s={6} validate label="Email" defaultValue={user.email} onChange={event => user.email=event.target.value}><Icon>email</Icon></Input>
                <Input s={6} validate type='tel' defaultValue={user.phone} label="Phone" onChange={event => user.phone=event.target.value}><Icon>phone</Icon></Input>
                <Input s={12} label="Organization" defaultValue={user.organization} onChange={event => user.organization=event.target.value}><Icon>supervisor_account</Icon></Input>
                <Col s={12} m={4}>
                    <RolesCheckboxGroup 
                        user={user}
                        onChange={updateUser}
                    />
                </Col>
                <Col s={12} m={8}>
                    <h5>Notification Settings</h5>
                    <DoctorNotificationSettingsView
                        user={user}
                        onChange={updateUser}/>
                </Col>
            </Row>
        </Modal>
    )
}

const mapStateToProps = state => {
    var { user } = state;

    return {
        user
    };
}

const mapDispatchToProps = (dispatch,{ownProps}) => {
  return {
    updateUser: user => dispatch(updateUser(user))
        .then(() => dispatch({type: "UPDATE_LOGGED_IN_USER", updateUser: user}))
  };
}

class SettingsContainer extends Component {
    render() {
        return (
            <SettingsView 
                triggerElement={this.props.triggerElement} 
                user={this.props.user} 
                updateUser={this.props.updateUser}/>
        );
    }
}

export const Settings = connect(mapStateToProps, mapDispatchToProps)(SettingsContainer);
