import axios from 'axios';
import * as api from './api.js';
import { browserHistory } from 'react-router'
import { persistStore } from 'redux-persist';
import { store } from '../index';
import * as _ from 'lodash';

console.log(store);
const apiRoot = process.env.REACT_APP_API_URI || "localhost:3001/api/";

// Action Types
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const LOGOUT = 'LOGOUT';
export const API_REQUEST = 'API_REQUEST';
export const API_SUCCESS = 'API_SUCCESS';
export const API_FAILED = 'API_FAILED';
export const SORT_LIST = 'SORT_LIST';
export const VIEW_DATA = 'VIEW_DATA';
export const ADD_USER = 'ADD_USER',
    ADD_PATIENT = 'ADD_PATIENT',
    HAND_FOUND = 'HAND_FOUND',
    HAND_LOST = 'HAND_LOST',
    SORT_USERS = 'SORT_USERS',
    SORT_PATIENTS = 'SORT_PATIENTS',
    SORT_METADATA = 'SORT_METADATA',
    RECEIVED_USERS = 'RECEIVED_USERS',
    RECEIVED_USER = 'RECEIVED_USER',
    RECEIVED_PATIENTS = 'RECEIVED_PATIENTS',
    RECEIVED_METADATA = 'RECEIVED_METADATA',
    RECEIVED_FCSPOINTS = 'RECEIVED_FCSPOINTS',
    UPLOAD_FCS = 'UPLOAD_FCS',
    UPLOAD_SUCCESSFUL = 'UPLOAD_SUCCESSFUL',
    SELECT_PATIENT = 'SELECT_PATIENT',
    FILTER_USERS = 'FILTER_USERS',
    FILTER_PATIENTS = 'FILTER_PATIENTS',
    FILTER_METADATA = 'FILTER_METADATA';

// Common Actions
const sortAction = (type, field) => ({ type, field });
const filterAction = (type, keyword) => ({ type, keyword })

// API Actions
export const apiRequest = (method, url, data) => ({
    type: API_REQUEST,
    method,
    url,
    data
});

export const apiSuccess = response => ({
    type: API_SUCCESS,
    response
});

export const apiFailed = error => ({
    type: API_FAILED,
    error
});

export const apiFetch = (method = "get", url, data = null) => dispatch => {
    dispatch(apiRequest(method, url, data));
    return axios({ method, url, data }).then(
        response => {
            if (response.status === 401) {
                dispatch({type: LOGOUT});
                return null;
            }

            dispatch(apiSuccess(response));
            return response.data.data;
        },
        error => dispatch(apiFailed(error))
    );
};

// Login Actions
export const loginSuccess = user => ({
    type: LOGIN_SUCCESS,
    user
});

export const loginFailed = error => ({
    type: LOGIN_FAILED,
    error
});

export const loginRequest = (email, password) => dispatch => {
    dispatch({
        type: LOGIN_REQUEST,
        user: { email, password }
    });

    if (apiRoot === 'local') {
        var loginPromise = api.authenticate(email, password).then(
            user => dispatch(loginSuccess(user)),
            error => dispatch(loginFailed(error))
        ).then(action => action.user) 
    }
    else {
        var loginPromise = axios({
            method: "post",
            url: apiRoot + "login",
            data: { email, password }
        }).then(response => {
            return axios({
                method: "get",
                url: apiRoot + "doctors/" + response.data.data.id,
                data: null
            }).then(response => {
                var user = response.data.data;
                dispatch(apiSuccess(response));
                dispatch(loginSuccess(user));
                return user;
            });
        },
            error => dispatch(loginFailed(error))
        ).catch(error => console.error(error));
    }

    return loginPromise.then(user => {
        const location = user.roles.includes('admin') ? "Users" : "Patients";
        browserHistory.push(location);
    });
}

export const logout = () => dispatch => {
    const logoutPromise = apiRoot === 'local' ?
        Promise.resolve() :
        axios({ method: "delete", url: apiRoot + "login" });

    return logoutPromise.then(response => {
        persistStore(store).purge();
        localStorage.clear();
        dispatch({ type: LOGOUT });
    });
};

// Users Actions
export const getUsers = filter => dispatch => {
    var users = [];

    if ( apiRoot === 'local') {
        users = api.fetchUsers();
    } else {
        users = dispatch(apiFetch("get", apiRoot + "users", null))
            .then(users => dispatch({ type: RECEIVED_USERS, users }));
    }

    return users;
}

export const getUser = userId => dispatch => {
    var user = {};

    if ( apiRoot === 'local' ) {
        user = api.fetchUser(userId);
    } else {
        user = dispatch(apiFetch("get", apiRoot + "doctors/" + userId, null))
            .then(user => dispatch({
                type: RECEIVED_USER,
                user
            }));
    }

    return user;
}

export const addUser = user => dispatch => {
    // var passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/;
    // console.log(passwordRegex.test(user.password), user.password);
    // if (!passwordRegex.test(user.password)) {
    //     return dispatch({type: "SET_ERROR", error: {message: "New user password does not meet requirements."}});
    // }

    const addUserPromise = apiRoot === 'local' ?
        Promise.resolve() :
        dispatch(apiFetch("post", apiRoot + "doctors", user));

    return addUserPromise.then(() => dispatch({ type: ADD_USER, user }));
};

export const updateUser = updatedUser => dispatch => {
    dispatch({ type: "UPDATE_USER", updatedUser });

    if (apiRoot === 'local') {
        const index = api.mockDB.users.findIndex(user => user.id === updatedUser.id);
        if (index !== -1) {
            api.mockDB.users[index] = updatedUser;
        }
        return dispatch(getUsers());
    } else {
        return dispatch(apiFetch("put", apiRoot + "doctors/" + updatedUser._id, updatedUser))
            .then(() => dispatch(getUsers()));
    }
};

export const sortUsers = field => sortAction(SORT_USERS, field);

export const filterUsers = keyword => filterAction(FILTER_USERS, keyword)

export const selectUserRows = (users, rows) => ({ type: "SELECT_USER_ROWS", users, rows });

export const deactiveUsers = () => {
    console.log("Deactivate called!");
};

export const resetPassword = email => dispatch => {
    if(apiRoot !== 'local') {
        return dispatch(apiFetch("post", apiRoot + "forgot", {email}))
            .then(() => dispatch({type: "PASSWORD_RESET", email}));
    }
};

// Patient Actions
export const getPatients = doctorId => dispatch => {
    dispatch({ type: API_REQUEST });
    var patients = apiRoot === 'local' ?
        api.fetchPatients(doctorId) :
        dispatch(apiFetch("get", apiRoot + "patients", null));

    return patients.then(
        patients => {
            dispatch({type: RECEIVED_PATIENTS,patients});
            dispatch({ type: API_SUCCESS });
        });
}

export const addPatient = patient => dispatch => {
    const addPatientPromise = apiRoot === 'local' ?
        Promise.resolve() :
        dispatch(apiFetch("post", apiRoot + "patients", patient));

    return addPatientPromise.then(() => dispatch({ type: ADD_PATIENT, patient }));
};

export const updatePatient = updatedPatient => dispatch => {
    dispatch({ type: "UPDATE_PATIENT", updatedPatient });
    if (apiRoot === 'local') {
        const index = api.mockDB.patients.findIndex(patient => patient.id === updatedPatient.id);
        if (index !== -1) {
            api.mockDB.patients[index] = updatedPatient;
        }
        return dispatch(getPatients());
    } else {
        return dispatch(apiFetch("put", apiRoot + "patients/" + updatedPatient._id, updatedPatient))
            .then(() => dispatch(getPatients()));
    }
};

export const selectPatient = patient => ({
    type: SELECT_PATIENT,
    patient
})

export const sortPatients = field => sortAction(SORT_PATIENTS, field);

export const filterPatients = keyword => filterAction(FILTER_PATIENTS, keyword);

// FCS Data Actions
export const getFCSMetaData = patientId => dispatch => {
    var fcsMetaDataListPromise = apiRoot === 'local' ?
        Promise.resolve(api.mockDB.testData) :
        dispatch(apiFetch("get", apiRoot + "metadata/patient/" + patientId, null));

    return fcsMetaDataListPromise.then(
        metaData => dispatch({
            type: RECEIVED_METADATA,
            metaData
        })
    );
};

export const updateFCSMetadata = updatedMetadata => dispatch => {
    dispatch({ type: "UPDATE_METADATA", updatedMetadata });
    if (apiRoot === 'local') {
        const index = api.mockDB.patients.findIndex(patient => patient.id === updatedMetadata.id);
        if (index !== -1) {
            api.mockDB.patients[index] = updatedMetadata;
        }
        return dispatch(getPatients());
    } else {
        return dispatch(apiFetch("put", apiRoot + "metadata/" + updatedMetadata._id, updatedMetadata));
    }
};

export const deleteClusterSet = (metadata, clusterSet) => dispatch => {
    const index = _.findIndex(metadata.clusters, cluster => cluster._id === clusterSet._id);
    metadata.clusters.splice(index, 1);
    dispatch(updateFCSMetadata(metadata));
};

export const getFCSPoints = fcsId => dispatch => {
    if( apiRoot === 'local') {
        var fcsPointsPromise = Promise.resolve(api.mockDB.points);
    } else {
        var fcsPointsPromise = dispatch(apiFetch("get", apiRoot + "processed/" + fcsId + "/download", null));
    }

    return fcsPointsPromise
        .then(points => Promise.resolve(dispatch({
            type: RECEIVED_FCSPOINTS,
            points
        })).then(() => window.open("graph.html", "_blank")));
};

export const addFCSFile = (file, patientId) => dispatch => {
    dispatch({
        type: UPLOAD_FCS,
        file
    });
    if (apiRoot !== 'local') {
        var form = new FormData();
        form.append("file", file);
        var xhr = new XMLHttpRequest();
        xhr.onload = () => {
            dispatch({ type: UPLOAD_SUCCESSFUL });
            dispatch(getFCSMetaData(patientId));
        };
        xhr.open("post", apiRoot + "preprocessed/patient/" + patientId, true);
        xhr.send(form);
    }
};