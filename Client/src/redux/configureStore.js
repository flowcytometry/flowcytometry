import { createStore, compose, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './reducers';
import {autoRehydrate} from 'redux-persist'

const logger = store => next => action => {
  console.groupCollapsed(action.type);
  console.log('previous state', store.getState());
  console.log('dispatching', action);
  let result = next(action);
  console.log('next state', store.getState());
  console.groupEnd(action.type);
  return result;
}

export const configureStore = () => 
  createStore(rootReducer,
    compose(
      applyMiddleware(thunkMiddleware, logger), 
      autoRehydrate()
    )
  );