import axios from 'axios';
export const API_REQUEST = 'API_REQUEST';
export const API_SUCCESS = 'API_SUCCESS';
export const API_FAILED = 'API_FAILED';

export const apiRequest = (method, url, data) => ({
    type: API_REQUEST,
    method,
    url,
    data
});

export const apiSuccess = response => ({
    type: API_SUCCESS,
    response
});

export const apiFailed = error => ({
    type: API_FAILED,
    error
});

export const apiFetch = (method = "get", url, data = null) => dispatch => {
    dispatch(apiRequest(method, url, data));
    return axios({
        method,
        url,
        data
    }).then(
        response => {
            dispatch(apiSuccess(response));
            return response.data.data;
        },
        error => dispatch(apiFailed(error))
    );
};
