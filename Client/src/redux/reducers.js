import { combineReducers } from 'redux';
import * as _ from 'lodash';
import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILED,
    API_REQUEST, API_SUCCESS, API_FAILED, LOGOUT, SELECT_PATIENT, ADD_USER, ADD_PATIENT, SELECT_USER, SORT_USERS, SORT_PATIENTS, SORT_METADATA, RECEIVED_USERS, RECEIVED_PATIENTS, RECEIVED_METADATA, RECEIVED_FCSPOINTS, FILTER_USERS, FILTER_PATIENTS, FILTER_METADATA} from './actions';

function apiReducer(state = "", action) {
    switch(action.type) {
        case "SET_API":
            return action.apiRoot;
        default:
            return state;
    }
}

function loginReducer(state = {loggedIn: false, roles:[]}, action) {
    switch(action.type) {
        case LOGIN_SUCCESS:
            return Object.assign({}, state, 
            {loggedIn: true}, action.user)
        case LOGIN_FAILED:
            return Object.assign({}, state, {loggedIn: false})
        case LOGOUT:
            return Object.assign({}, {}, {loggedIn: false, roles:[]}); 
        case "UPDATE_LOGGED_IN_USER":
            return Object.assign({}, state, action.updatedUser)
        case LOGIN_REQUEST:
        default:
            return state;
    }
}

function loadingReducer(state = {isLoading: false}, action) {
    switch(action.type) {
        case LOGIN_REQUEST:
        case API_REQUEST:
            return Object.assign({}, state, {isLoading: true, url: action.url})
        case API_SUCCESS:
        case API_FAILED:
        case LOGIN_FAILED:
        case LOGOUT:
        case LOGIN_SUCCESS:
            return Object.assign({}, state, {isLoading: false, url:''})
        default:
            return state;
    }
}

function errorReducer(state = {}, action) {
    switch(action.type) {
        case API_FAILED:
            return Object.assign({}, state, {message: action.error.message})
        case LOGIN_FAILED:
            return Object.assign({}, state, {message: action.error.response.data.message})
        case API_SUCCESS:
        case LOGIN_SUCCESS:
            return Object.assign({}, state, {message: null});
        case "SET_ERROR":
            return {...state, message: action.error.message};
        case LOGOUT:
        case "CLEAR_ERROR":
            return {};
        default:
            return state;
    }
}

const initialUsersState = {
    sort: 'firstname', 
    descendingOrder: false, 
    filter: '', 
    list: [], 
    selectedUser: {}, 
    selectedTableRows:[],
    inactiveUsersVisibile: false
};

function usersReducer(state = initialUsersState, action) {
    switch(action.type) {
        case ADD_USER:
            const newList = [...state.list, action.user];
            return Object.assign({}, state, {list:newList});
        case RECEIVED_USERS:
            return Object.assign({}, state, {list: action.users})
        case SORT_USERS:
            return Object.assign({}, state, {sort: action.field});
        case FILTER_USERS:
            return Object.assign({}, state, {filter: action.keyword});
        case SELECT_USER:
            return Object.assign({}, state, {selectedUser: action.user});
        case "SELECT_USER_ROWS":
                switch(action.rows) {
                    case "all": 
                        return {...state, selectedTableRows: action.users}; 
                    case "None":
                        return {...state, selectedTableRows: []};
                    default:
                        const result = action.rows.map(rowIndex => action.users[rowIndex]); 
                        return {...state, selectedTableRows: result};
                }
        case "SET_SELECTED_USERS_ACTIVATION":
            const users = [...state.list];
            
            const deactivatedUsersIndices = state.selectedTableRows.map(
                user => state.list.findIndex(el => el.id === user.id)
            );

            for (var index of deactivatedUsersIndices) {
                users[index].activated = action.active;
            }

            return {...state, list: users, selectedTableRows:[]};
        case "SELECT_USER_VISIBILITY_FILTER":
            return {...state, inactiveUsersVisibile: !state.inactiveUsersVisibile};
        case "TOGGLE_USER_ORDER":
            return {...state, descendingOrder: !state.descendingOrder};
        case "PASSWORD_RESET":
            return {...state, resetEmailAddress: action.email};
        case LOGOUT:
            return initialUsersState;
        default:
            return state;
    }
}

const initialPatientsState = {
    sort: 'id', 
    descendingOrder: false, 
    filter: '',
    list: [], 
    selectedPatient: {}
};

function patientsReducer(state = initialPatientsState, action) {
    switch(action.type) {
        case ADD_PATIENT:
            return Object.assign({}, state, {list:[...state.list, action.patient]});
        case RECEIVED_PATIENTS:
            return Object.assign({}, state, {list: action.patients})
        case SORT_PATIENTS:
            var {list} = state;
            list = _.sortBy(list, [action.field]);
            return Object.assign({}, state, {list, sort: action.field});
        case FILTER_PATIENTS:
            return Object.assign({}, state, {filter: action.keyword}); 
        case SELECT_PATIENT:
            var patient = action.patient;
            return Object.assign({}, state, {selectedPatient: patient})
        case "TOGGLE_PATIENT_ORDER":
            return {...state, descendingOrder: !state.descendingOrder};
        default:
            return state;
    }
}

const initialFcsState = {
    sort: 'upload_date',
    filter: '',
    descendingOrder: false,
    metadataList: [],
    selectedMetadata: {},
    points: [],
    selectedTableRows:[],
    selectedClusterSet:{}
};

function fcsReducer(state = initialFcsState, action) {
    switch(action.type) {
        case RECEIVED_METADATA:
            return Object.assign({}, state, {metadataList: action.metaData})
        case RECEIVED_FCSPOINTS:
            return Object.assign({}, state, {points: action.points})
        case SORT_METADATA:
            return Object.assign({}, state, {sort: action.field});
        case FILTER_METADATA:
            return Object.assign({}, state, {filter: action.keyword});
        case "TOGGLE_FCS_METADATA_ORDER":
            return {...state, descendingOrder: !state.descendingOrder};
        case "SELECT_CLUSTER_SET":
            return {...state, selectedClusterSet: action.clusterSet};
        case "SELECT_METADATA":
            return {...state, selectedMetadata: action.metadata};
        case "UPDATE_METADATA":
            var index = _.findIndex(state.metadataList, metadata => metadata._id === action.updatedMetadata._id);
            state.metadataList[index] = action.updatedMetadata;
            return {...state, metadataList: state.metadataList};
        default:
            return state;
    }
}

const rootReducer = combineReducers({
    api: apiReducer,
    user: loginReducer,
    error: errorReducer,
    loading: loadingReducer,
    users: usersReducer,
    patients: patientsReducer,
    fcs: fcsReducer
})

export default rootReducer
